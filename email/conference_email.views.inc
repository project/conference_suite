<?php
/**
 * @file
 * Views support.
 */
/**
 * Implements hook_views_data_alter().
 */
function conference_email_views_data_alter(&$data) {
  $data['views_entity_node']['email_chairs'] = [
    'field' => [
      'title' => t('Email area chairs'),
      'help' => t('Provide a link to the area-chair email form. Appears in node:area.'),
      'handler' => 'conference_email_handler_field_node_link_email_chairs',
    ],
  ];
}
