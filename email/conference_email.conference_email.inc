<?php
/**
 * @file
 * Implementations of Conference Email hooks on behalf of system modules.
 */

/**
 * Implements hook_conference_email_email_keys().
 */
function conference_email_conference_email_email_keys() {
  return [
    'user' => '*',
  ];
}