<?php
/**
 * @file
 * Pages and forms for Conference Email.
 */

/**
 * Menu callback: displays an email form.
 */
function conference_email_form_page($type) {
  $args = func_get_args();
  array_shift($args);
  return drupal_get_form("conference_email_{$type}_form", $args);
}

/**
 * Form constructor for conference_email forms.
 */
function conference_email_form($form, &$form_state, $type, $properties = [], $arguments = []) {
  drupal_set_title(conference_email_form_title($type));

  $form_state['conference_email']['mail_key'] = $type;
  $form_state['conference_email']['properties'] = $properties;

  if (!empty($properties['flood'])) {
    // Piggyback on the Contact module, if enabled
    $limit = variable_get('contact_threshold_limit', 5);
    $window = variable_get('contact_threshold_window', 3600);
    if (!flood_is_allowed("conference_email_{$type}", $limit, $window) && !user_access('override sending limits')) {
      drupal_set_message(format_plural($limit, "You cannot send more than 1 message in @interval. Try again later.", "You cannot send more than @count messages in @interval. Try again later.", array('%limit' => $limit, '@interval' => format_interval($window))), 'error');
      drupal_access_denied();
      drupal_exit();
    }
  }

  if (!empty($properties['from'])) {
    global $user;

    $form['from'] = ['#tree' => TRUE];
    $form['from']['name'] = [
      '#type' => 'textfield',
      '#title' => t('Your name'),
      '#default_value' => $user->uid ? format_username($user) : '',
      '#maxlength' => 255,
      '#required' => TRUE,
      '#weight' => -150,
    ];

    $form['from']['email'] = [
      '#type' => !empty(element_info('emailfield')) ? 'emailfield' : 'textfield',
      '#title' => t('Your email address'),
      '#default_value' => $user->uid ? $user->mail : '',
      '#maxlength' => 255,
      '#required' => TRUE,
      '#weight' => -125,
    ];
  }

  if (!empty($properties['conference'])) {
    $form['conference'] = [
      '#type' => 'select',
      '#title' => t('Conference'),
      '#options' => Conference::optionsList(),
      '#default_value' => Conference::current(),
      '#required' => TRUE,
      '#weight' => -100,
    ];
  }

  if (isset($properties['areas'])) {
    if (!empty($properties['areas'])) {
      $areas = node_load_multiple(array_keys($properties['areas']));

      foreach ($areas as $nid => $area) {
        $options[$nid] = $area->title;
      }
    }
    else {
      $options[] = t('- No areas available -');
    }

    $form['area'] = [
      '#type' => 'select',
      '#title' => t('Area'),
      '#options' => $options,
      '#default_value' => count($options) == 1 ? reset($options) : NULL,
      '#required' => TRUE,
      '#weight' => -50,
    ];
  }

  $form['subject'] = [
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => 1,
  ];


  $form['message'] = [
    '#type' => 'text_format',
    '#title' => t('Message'),
    '#rows' => 20,
    '#required' => TRUE,
    '#weight' => 10,
  ];

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Send email'),
  ];
  
  if (module_exists('honeypot')) {
    honeypot_add_form_protection($form, $form_state, ['honeypot', 'time_restriction']);
  }

  return $form;
}

/**
 * Validation handler for conference_email_form().
 */
function conference_email_form_validate($form, &$form_state) {
  $recipients = [];
  drupal_alter('conference_email_form_recipients', $recipients, $form, $form_state);

  if (empty($recipients)) {
    form_set_error('', t('There are no valid recipients for your message.'));
  }

  $form_state['recipients'] = $recipients;
}

/**
 * Submission handler for conference_email_form().
 *
 * @todo Handle messages in different languages.
 */
function conference_email_form_submit($form, &$form_state) {
  if (!empty($form_state['recipients'])) {
    global $language;
    // Send first 25
    $recipients = array_splice($form_state['recipients'], 0, 25);
    $arguments = reset($form_state['build_info']['args']);

    // Default parameters
    $params = [
      'key' => $form_state['conference_email']['mail_key'],
      'message' => [
        'subject' => $form_state['values']['subject'],
        'body' => $form_state['values']['message'],
        'language' => $language,
      ],
      'from' => !empty($form_state['values']['from']) ? $form_state['values']['from'] : [
        'name' => format_username($GLOBALS['user']),
        'email' => $GLOBALS['user']->mail,
      ],
      'prefix_list' => $form_state['conference_email']['prefix_list'],
      'form_values' => $form_state['values'],
      'form_arguments' => $arguments,
    ];

    foreach ($recipients as $recipient) {
      // Add recipient
      $params['recipients'] = [$recipient];
      conference_email_send($params);
    }

    // Queue remaining
    if (count($form_state['recipients']) <> 0) {
      $queue = DrupalQueue::get('conference_email_send_queued_form_mail');

      while (count($form_state['recipients']) <> 0) {
        $recipients = array_splice($form_state['recipients'], 0, 25);

        foreach ($recipients as $recipient) {
          // Add recipient
          $params['recipients'] = [$recipient];
          $queue->createItem($params);
        }
      }
    }

    // Sender email confirmation
    $params['key'] .= '_sender_confirmation';

    // If sender is current user
    if ($params['from']['email'] == $GLOBALS['user']->mail) {
      $params['recipients'] = [$GLOBALS['user']];
    }
    else {
      // Create a dummy user
      $params['recipients'] = [(object) [
        'name' => $params['from']['name'],
        'mail' => $params['from']['email'],
      ]];
    }

    // Reset to default
    $params['from'] = [];
    $params['prefix_list'] = !empty($form_state['conference_email']['prefix_list']) ? $form_state['conference_email']['prefix_list'] : [];
    conference_email_send($params);

    drupal_set_message(t('Your message has been sent.'));

    if (!empty($form_state['conference_email']['properties']['flood'])) {
      flood_register_event("conference_email_{$form_state['conference_email']['mail_key']}", variable_get('contact_threshold_window', 3600));
    }

    $form_state['redirect'] = conference_email_form_redirect($form_state['conference_email']['mail_key'], $arguments);
  }
}

/**
 * Builds title for email form.
 */
function conference_email_form_title($type) {
  switch ($type) {
    case 'area_people' :
      return t('Email people in your area');

    case 'area_chairs' :
      return t('Email all area chairs');

    case 'area_chair' :
      return t('Email the area chairs'); // @todo make dynamic
  }
}

/**
 * Implements hook_conference_email_form_recipients_alter().
 *
 * @return An array of user objects.
 */
function conference_email_conference_email_form_recipients_alter(&$recipient_list, $form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];
  $recipient_list = [];
  $prefix_list = [];
  $views_map = cvget('conference_email_views_map');

  switch ($form['#form_id']) {
    case 'conference_email_area_people_form' :
      extract($values);
      $view_arguments = [$area, $conference, 'any'];
      $view_result = [];

      foreach ($recipients as $recipient_type) {
        if ( !empty($recipient_type) ) {
          $view = explode(':', $views_map["area_people_{$recipient_type}"]);
          $view_arguments[] = $recipient_type;
          $view_result = array_merge($view_result, call_user_func_array('views_get_view_result', array_merge($view, $view_arguments)));

          switch ($recipient_type) {
            case 'approved' :
              $prefix_list[] = t('approved speakers');
            break;

            case 'pending' :
              $prefix_list[] = t('speakers pending approval');
            break;

            case 'rejected' :
              $prefix_list[] = t('rejected speakers');
            break;

            case 'chairs' :
              $prefix_list[] = t('session chairs');
            break;
          }
        }
      }

      if (!empty($view_result)) {
        array_walk($view_result, function(&$result) {
          $result = (array) $result;
        });

        $view_result = user_load_multiple(array_column($view_result, 'uid'));
      }

      $recipient_list = array_merge($recipient_list, $view_result);
    break;

    case 'conference_email_area_chairs_form' :
      $view_result = call_user_func_array('views_get_view_result', explode(':', $views_map['area_chair_email']));

      if (!empty($view_result)) {
        array_walk($view_result, function(&$result) {
          $result = (array) $result;
        });

        $view_result = array_column($view_result, 'users_field_data_conference_area_chair_mail');

        $accounts = (new EntityFieldQuery)
          ->entityCondition('entity_type', 'user')
          ->propertyCondition('mail', $view_result)
          ->execute();

        if (!empty($accounts['user'])) {
          $recipient_list = array_merge($recipient_list, user_load_multiple(array_keys($accounts['user'])));
        }
      }
    break;

    case 'conference_email_area_chair_form' :
      $arguments = reset($form_state['build_info']['args']);
      $wnode = entity_metadata_wrapper('node', reset($arguments));

      $recipient_list = $wnode->conference_area_chair->value();
    break;
  }

  $form_state['conference_email']['prefix_list'] = $prefix_list;

  return $recipient_list;
}

/**
 * Returns the proper redirect URL for each form.
 */
function conference_email_form_redirect($key, $arguments) {
  switch ($key) {
    case 'area_people' :
    case 'area_chairs' :
      return 'admin/conference/dashboard';

    case 'area_chair' :
      $node = reset($arguments);
      return "node/{$node->nid}";
  }
}
