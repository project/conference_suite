<?php
/**
 * @file
 * Hooks for Conference Email.
 */

/**
 * Allows modules to declare a list of email keys.
 */
function hook_conference_email_email_keys() {

}

/**
 * Allows modules to alter the list of email keys.
 */
function hook_conference_email_email_keys(&$keys) {

}
