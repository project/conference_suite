<?php
/**
 * @file
 * Definition of conference_email_handler_field_node_link_email_chairs.
 */

/**
 * Field handler to present a link to the area chairs' email form.
 *
 * @ingroup views_field_handlers
 */
class conference_email_handler_field_node_link_email_chairs extends views_handler_field_node_link {
  /**
   * Renders the link.
   */
  function render_link($node, $values) {
    // Ensure user has access to the email form
    if (!conference_email_access('area_chair', $node)) {
      return;
    }

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = "node/$node->nid/email";
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('email area chairs');
    return $text;
  }
}
