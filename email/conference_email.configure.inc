<?php
/**
 * @file
 * Variable declarations for Conference E-mail.
 */
function conference_email_configure_variable_info() {
  $variables = [
    'conference_email_footer' => [
      'group' => 'email_text',
      'type' => 'group',
      'title' => t('Conference e-mail footer'),
      'items' => [
        'general' => [
          'title' => t('Footer text'),
          'description' => t('This text will be appended to all e-mails sent from the website.'),
        ],
        'unmonitored' => [
          'title' => t('Unmonitored account text'),
          'description' => t('This text will be appended to e-mails sent from the website’s main e-mail account.'),
          'default' => "*This message was sent from an unmonitored account. If you need a prompt response, please do not reply directly to it.*",
        ],
      ],
      'element' => [
        '#type' => 'textarea',
      ],
    ],
    'conference_email_salutations' => [
      'group' => 'email_text',
      'element' => [
        '#type' => 'textarea',
        '#description' => t('Enter one per line, including any desired punctuation. A random salutation will be used to add a more personal feeling in closing outgoing messages.'),
        '#title' => t('E-mail salutations'),
        '#rows' => 10,
      ],
    ],
    'conference_email_views_map' => [
      'group' => 'views_map',
      'type' => 'group',
      'title' => t('Conference e-mail views map'),
      'options callback' => '_conference_get_views',
      'element' => [
        '#type' => 'select',
        '#options' => [t('- Select -')],
      ],
      'items' => [
        'area_chair_email' => [
          'title' => t('Area chairs’ e-mail addresses'),
        ],
        'area_people_approved' => [
          'title' => t('Area’s approved speakers'),
        ],
        'area_people_rejected' => [
          'title' => t('Area’s rejected speakers'),
        ],
        'area_people_pending' => [
          'title' => t('Area’s pending speakers'),
        ],
        'area_people_chairs' => [
          'title' => t('Area’s session chairs'),
        ],
      ],
    ],
    'conference_email_from_addresses' => [
      'group' => 'email_from',
      'type' => 'matrix',
      'title' => t('Custom “from” addresses'),
      'edit link' => 'admin/conference/settings/email/senders/%s/edit',
      'properties' => [
        'key' => [
          'title' => t('Mail key'),
          'self' => TRUE,
        ],
        'name' => [
          'title' => t('From name'),
        ],
        'email' => [
          'title' => t('From email'),
        ],
      ],
      'items callback' => 'conference_email_email_keys',
    ],
    'conference_email_delay:module_name' => [
      'type' => 'meta',
      'title' => t('Conference Email delay by module'),
    ],
  ];

  return $variables;
}

/**
 * Implements hook_configure_variable_defaults().
 */
function conference_email_configure_variable_defaults() {
  return [
    'conference_email_views_map' => [
      'area_chair_email' => 'areas:chair_emails',
      'area_people_approved' => 'area_emails:approved',
      'area_people_rejected' => 'area_emails:rejected',
      'area_people_pending' => 'area_emails:pending',
      'area_people_chairs' => 'area_emails:chairs',
    ],
  ];
}
