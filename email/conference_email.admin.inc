<?php
/**
 * @file
 * Administrative pages for Conference Email.
 */

/**
 * Form constructor: admin page for delayed emails.
 */
function conference_email_admin_delayed_page($form, &$form_state) {
  if (!empty($form_state['conference_email_confirm'])) {
    return conference_email_admin_delayed_action_confirm($form, $form_state, $form_state['values']['delayed'], $form_state['conference_email_confirm']);
  }

  return conference_email_admin_delayed_form();
}

/**
 * Form builder: lists delayed emails.
 */
function conference_email_admin_delayed_form() {
  drupal_set_title('Delayed emails');
  $delayed = cvget('conference_email_delay', ['module_name' => '%']);
  $delayed = !empty($delayed['multiple']) ? $delayed['multiple'] : [];

  $header = [
    'module' => t('Module'),
    'key' => t('Email key'),
    'delayed' => t('Delayed until'),
  ];

  if (module_exists('devel') && user_access('access devel information')) {
    $header['content'] = t('Content');
  }

  $rows = [];
  foreach ($delayed as $module => $emails) {
    foreach ($emails as $hash => $data) {
      $rows["$module:$hash"] = [
        'module' => $module,
        'key' => $data['message_key'],
        'delayed' => format_date($data['delay'], 'custom', 'c'),
      ];

      if (module_exists('devel') && user_access('access devel information')) {
        $rows["$module:$hash"]['content'] = kprint_r($data, TRUE);
      }
    }
  }

  $form['delayed'] = [
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $rows,
    '#empty' => t('There are no delayed emails.'),
  ];

  if (!empty($delayed)) {
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete selected emails'),
      '#validate' => ['conference_email_admin_delayed_form_validate'],
      '#submit' => ['conference_email_admin_delayed_form_submit'],
      '#name' => 'delete',
    ];

    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => t('Send selected emails'),
      '#validate' => ['conference_email_admin_delayed_form_validate'],
      '#submit' => ['conference_email_admin_delayed_form_submit'],
      '#name' => 'send',
    ];
  }

  return $form;
}

/**
 * Validation handler for conference_email_admin_delayed_form().
 */
function conference_email_admin_delayed_form_validate($form, &$form_state) {
  $emails = array_filter($form_state['values']['delayed']);
  if (empty($emails)) {
    form_set_error('delayed', t('You must select at least one email.'));
  }
  else {
    $form_state['values']['delayed'] = $emails;
  }
}

/**
 * Submit handler for conference_email_admin_delayed_form().
 */
function conference_email_admin_delayed_form_submit($form, &$form_state) {
  $form_state['conference_email_confirm'] = $form_state['triggering_element']['#name'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Confirmation form: delete delayed emails.
 */
function conference_email_admin_delayed_action_confirm($form, &$form_state, $emails, $action) {
  $form['delayed'] = [
    '#type' => 'value',
    '#value' => $emails,
  ];

  $form['action'] = [
    '#type' => 'value',
    '#value' => $action,
  ];

  $form['#submit'][] = 'conference_email_admin_delayed_action_confirm_submit';
  $confirm_question = format_plural(count($emails),
  'Are you sure you want to !action the selected email?',
  'Are you sure you want to !action the selected emails?',
  ['!action' => $action]);

  $action_verb = $action == 'delete' ? t('Delete') : t('Send');

  return confirm_form($form,
  $confirm_question,
  'admin/content', t('This action cannot be undone.'),
  $action_verb, t('Cancel'));
}

/**
 * Submit handler for conference_email_admin_delayed_action_confirm().
 */
function conference_email_admin_delayed_action_confirm_submit($form, &$form_state) {
  foreach ($form_state['values']['delayed'] as $hash) {
    list($module, $hash) = explode(':', $hash);
    $delete[$module][$hash] = $hash;
  }

  foreach ($delete as $module => $hashes) {
    $emails = cvget('conference_email_delay', ['module_name' => $module]);

    foreach ($hashes as $hash) {
      if (!empty($emails[$hash])) {
        switch ($form_state['values']['action']) {
          case 'delete' :
            unset($emails[$hash]);
          break;

          case 'send' :
            $emails[$hash]['delay'] = REQUEST_TIME;
          break;
        }
      }
    }

    if (!empty($emails)) {
      cvset('conference_email_delay', $emails, ['module_name' => $module]);
    }
    else {
      cvdel('conference_email_delay', ['module_name' => $module]);
    }
  }

  $action_participle = $form_state['values']['action'] == 'delete' ? t('deleted') : t('queued for delivery');

  drupal_set_message(format_plural(count($form_state['values']['delayed']), 'The selected email has been !action.', 'The selected emails have been !action.', ['!action' => $action_participle]));
}
