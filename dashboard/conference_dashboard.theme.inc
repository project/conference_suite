<?php
/**
 * @file
 * Theme functions and templates for Conference Dashboard.
 */

/**
 * Returns HTML for a single dashboard block.
 */
function theme_conference_dashboard_block($variables) {
  $class = implode(' ', array_merge(['view-count'], $variables['class']));
  $output = '<strong>' . $variables['title'] . '</strong>';
  $output .= "<div class=\"$class\">" . $variables['content'];
  if ( isset($variables['note']) ) {
    $output .= "<small>" . $variables['note'] . "</small>";
  }
  $output .= '</div>';

  return $output;
}
