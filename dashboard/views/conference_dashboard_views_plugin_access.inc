<?php
/**
 * @file
 * conference_dashboard_plugin_access_callback.inc
 */

/**
 * Access plugin that provides callback-function-based access control.
 */
class conference_dashboard_views_plugin_access extends views_plugin_access {
  function access($account) {
    return conference_dashboard_views_access($this->options['conference_dashboard_block'], $account);
  }

  function get_access_callback() {
    return ['conference_dashboard_views_access', [$this->options['conference_dashboard_block']]];
  }

  function summary_title() {
    return $this->options['conference_dashboard_block'];
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['conference_dashboard_block'] = array('default' => 'TRUE');

    return $options;
  }


  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    static $setup;

    if (empty($setup)) {
      $setup = cvget('conference_dashboard_setup');

      foreach ($setup as $name => $block) {
        $blocks[$name] = "{$block['context']}: {$block['name']}";
      }
    }

    $form['conference_dashboard_block'] = array(
      '#type' => 'select',
      '#options' => $blocks,
      '#title' => t('Dashboard blocks'),
      '#default_value' => $this->options['conference_dashboard_block'],
      '#description' => t('Only users who can access the selected block will be able to access this display.'),
    );
  }
}