<?php
/**
 * @file
 * Customizes views.
 */

/**
 * Extends template_preprocess_views_view_field().
 *
 * @todo Ticket link should appear only after a set date, or after setting a variable.
 * @todo remove reference to field_city_value
 */
function conference_dashboard_preprocess_views_view_field(&$vars) {
  $field = $vars['field'];
  $view = $vars['view'];
  $row = $vars['row'];
  $output = $vars['output'];

  switch ( $view->name ) {
    case 'conference' :
      if ( $view->current_display == 'past' ) {
        switch ( $field->real_field ) {
          case 'conference_year_value' :
          case 'field_city_value' :
            if ( !empty($row->field_body) ) {
              $output = l($output, 'node/' . $row->nid);
            }
          break;

          case 'nothing' :
            if ( !empty($row->field_body) ) {
              $year = $row->field_data_conference_year_conference_year_value;
              $output = l(t('schedule'), 'conference/schedule/' . $year, ['attributes' => ['title' => t('!year conference schedule', ['!year' => $year])]]);
            }
          break;
        }
      }
    break;

    case 'user_content' :
      if ( $view->current_display == 'content_tab' ) {
        $options = &$field->options;

        switch ( $field->field ) {
          case 'type' :
            $options['alter']['alter_text'] = TRUE;
            $options['alter']['text'] = t($field->original_value . 's');
          break;
        }

        $output = $field->advanced_render($row);
      }
    break;
  }

  $vars['output'] = $output;
}

/**
 * Implements hook_views_query_alter().
 */
function conference_dashboard_views_query_alter(&$view, &$query) {
  $delta = "{$view->name}_{$view->current_display}";

  switch ( $delta ) {
    case 'user_content_content_tab' :
      // The display should (contextually) filter presenters OR session chairs
      $query->where[0]['type'] = 'OR';
    break;
  }
}

/**
 * Implements hook_views_plugins().
 */
function conference_dashboard_views_plugins() {
  return array(
    'access' => array(
      'parent' => array(
        'no ui' => TRUE,
        'handler' => 'views_plugin_access',
        'parent' => '',
      ),
      'conference_dashboard_views_plugin_access' => array(
        'title' => t('Conference dashboard access'),
        'help' => t('Access granted based on permission to view corresponding dashboard block.'),
        'handler' => 'conference_dashboard_views_plugin_access',
        'uses options' => TRUE,
        'path' => drupal_get_path('module', 'conference_dashboard') . '/views',
      ),
    ),
  );
}
