<?php
/**
 * @file
 * conference_dashboard.features.inc
 */

/**
 * Implements hook_views_api().
 */
function conference_dashboard_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
