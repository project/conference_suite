<?php
/**
 * @file
 * Functions that produce the conference dashboard and its elements.
 *
 * @todo add proper link to dashboard to user (?) menu
 */

/**
 * Displays the conference dashboard.
 *
 * @todo Add conference select menu (maybe only for admins?)
 * @todo Change counter blocks to properly accept conference as argument
 */
function conference_dashboard_page() {
  drupal_get_title();

  /**
   * @todo Make this useful
   * $conference = arg(3);
   * $conference = empty($conference) ? Conference::current() : $conference;
   */

  $output = '';

  $dashboard_setup = cvget('conference_dashboard_setup');
  $sections = configure_variable_get_info('conference_dashboard_setup')['properties']['section']['element']['#options'];

  $dashboard_blocks = [];

  // Get user areas. Using NULL as account will load the current user
  $user_areas = Conference::getUserAreas(NULL, TRUE);

  foreach ( $dashboard_setup as $block_name => $block ) {
    $block['key'] = $block_name;

    if (conference_dashboard_block_access($block)) {
      $block_view = explode(':', $block['view']);
      $block_arguments = conference_dashboard_build_block_arguments($block);
      $area_argument_key = array_search('%area', $block_arguments);

      $block_link_options = [];

      if ( !empty($block['total']) ) {
        $block_link_options['attributes']['class'][] = 'total';
      }

      conference_dashboard_build_block_query($block, $block_link_options);

      $block['name'] = !empty($block['display']) ? check_plain($block['display']) : check_plain($block['name']);

      switch ( $block['context'] ) {
        case 'admin' :
          $count = conference_dashboard_block_count($block_name, $block_view, $block_arguments);
          $dashboard_blocks[$block['context']][$block['section']][$block_name] = [
            'title' => t($block['name']),
            'content' => l($count, $block['path'], $block_link_options),
          ];
        break;

        case 'areas' :
          if ( !empty($user_areas) ) {
            foreach ( $user_areas as $area_nid => $area ) {
              // Not using $view->access() because it's more important to check permission over the specific area
              if ( Conference::isAreaChair($area_nid) ) {

                if ( $area_argument_key !== FALSE ) {
                  $block_arguments[$area_argument_key] = $area_nid;
                }

                $count = conference_dashboard_block_count($block_name, $block_view, $block_arguments);

                $dashboard_blocks[$block['context']][$area_nid][$block['section']][$block_name] = [
                  'title' => t($block['name']),
                  'content' => l($count, $block['path'], $block_link_options),
                ];
              }
            }
          }
        break;
      }
    }
  }

  // Admin blocks
  if ( !empty($dashboard_blocks['admin']) ) {
      // Conference blocks
    $output .= '<h3>' . t('Overview') . '</h3>';
    $output .= '<dl>';
    foreach ( $dashboard_blocks['admin'] as $section => $blocks ) {
      $output .= "<dt class=\"$section\">" . $sections[$section] . '</dt><dd>';

      $list = [
        'type' => 'ul',
        'attributes' => [
          'class' => [
            $section,
          ],
        ],
        'items' => [],
      ];

      foreach ( $blocks as $name => $block ) {
        $theme_variables = ['title' => $block['title'], 'content' => $block['content'], 'class' => [$name]];
        if ( isset($block['note']) ) {
          $theme_variables['note'] = t($block['note']);
        }

        $list['items'][] = theme('conference_dashboard_block', $theme_variables);
      }

      $output .= theme('item_list', $list);
      $output .= '</dd>';
    }

    $output .= '</dl>';
  }
  else {
    if ( user_access('administer conference configuration') ) {
      $output .= '<p>' . t('No administrative views are available. !config_link', ['!config_link' => l(t('Configure the dashboard settings.'), 'admin/conference/settings/dashboard')]) . '</p>';
    }
  }

  // Area blocks
  if ( !empty($dashboard_blocks['areas']) ) {
    $output .= '<h3>' . t('Your areas') . '</h3>';

    foreach ( $dashboard_blocks['areas'] as $area => $area_blocks ) {
      $area = $user_areas[$area];

      $output .= '<h4>' . l(t($area->title), 'node/' . $area->nid) . '</h4>';
      $output .= '<dl>';

      foreach ( $area_blocks as $section => $blocks ) {
        $output .= "<dt class=\"$section\">" . $sections[$section] . '</dt><dd>';

        $list = [
          'type' => 'ul',
          'attributes' => [
            'class' => [
              $section,
            ],
          ],
          'items' => [],
        ];

        foreach ( $blocks as $name => $block ) {
          $block['content'] = str_replace(urlencode("%area"), $area->nid, $block['content']);
          $theme_variables = ['title' => $block['title'], 'content' => $block['content'], 'class' => [$name]];
          if ( isset($block['note']) ) {
            $theme_variables['note'] = t($block['note']);
          }
          $list['items'][] = theme('conference_dashboard_block', $theme_variables);
        }

        $output .= theme('item_list', $list);
        $output .= '</dd>';
      }

      $output .= '</dl>';
    }
  }

  return $output;
}

/**
 * Displays a sortable table with recap information for each area, with a row of totals.
 */
function conference_dashboard_areas() {
  drupal_set_title(t('Overview of areas'));

  $dashboard_setup = cvget('conference_dashboard_setup');

  $table = [
    'header' => [
      'area' => [
        'data' => t('Area'),
        'field' => 'area',
        'sort' => 'asc',
      ],
    ],
  ];

  $areas_query = new EntityFieldQuery();
  $areas_query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'area')
    ->propertyOrderBy('title');
  $areas = $areas_query->execute();

  $areas = reset($areas);

  $totals = [];

  foreach ( $dashboard_setup as $block_name => $block_info ) {
    if ( $block_info['context'] == 'areas' && !empty($block_info['enabled']) ) {
      $block_view = explode(':', $block_info['view']);
      $block_arguments = conference_dashboard_build_block_arguments($block_info);

      $table['header'][$block_name]['data'] = t($block_info['name']);
      $table['header'][$block_name]['class'] = ['number'];
      $table['header'][$block_name]['field'] = $block_name;

      $block_link_options = [];
      conference_dashboard_build_block_query($block_info, $block_link_options);

      $area_argument_key = array_search('%area', $block_arguments);
      if ( is_numeric($area_argument_key) ) {
        $block_arguments[$area_argument_key] = 'all';
      }

      $result = conference_dashboard_block_count($block_name, $block_view, $block_arguments, TRUE);

      $area_rows = [];
      // Get area key
      $keys = !empty($result) ? array_keys(reset($result)) : [];
      $keys = preg_grep("/field_data_conference_area/", $keys);
      $area_key = !empty($keys) ? reset($keys) : NULL;

      if ( !empty($area_key) ) {
        foreach ( $result as $row ) {
          $area_rows[$row[$area_key]][] = $row;
        }
      }

      foreach ( $areas as $area ) {
        if ( empty($totals['area']) ) {
          $totals['area'] = t('Total');
        }

        $count = !empty($area_rows[$area->nid]) ? count($area_rows[$area->nid]) : 0;

        $area_node = node_load($area->nid);
        $table['rows'][$area->nid]['area'] = l($area_node->title, 'node/' . $area->nid);
        $table['rows'][$area->nid][$block_name]['data'] = $count;

        // The 'content' key isn't in the official spec, but used here to replace the raw number after sorting
        $table['rows'][$area->nid][$block_name]['content'] = l($count, $block_info['path'], $block_link_options);
        // Link to general views, not chair views
        // It's a dirty solution, but it works
        $table['rows'][$area->nid][$block_name]['content'] = str_replace('/areas', '', $table['rows'][$area->nid][$block_name]['content']);
        $table['rows'][$area->nid][$block_name]['content'] = str_replace(urlencode("%area"), $area->nid, $table['rows'][$area->nid][$block_name]['content']);
        $table['rows'][$area->nid][$block_name]['class'] = ['number'];

        // Add to totals
        $totals[$block_name]['data'] = empty($totals[$block_name]['data']) ? $count : $totals[$block_name]['data'] + $count;
        $totals[$block_name]['class'] = ['number'];
      }
    }
  }

  if ( module_exists('tsort') ) {
    // Replace row keys with progressive deltas
    $table['rows'] = array_values($table['rows']);
    $order = tablesort_get_order($table['header']);
    $sort = tablesort_get_sort($table['header']);
    $table['rows'] = tsort_nonsql_sort($table['rows'], $sort, $order['sql']);
  }

  // Replace count data with link to view
  foreach ( $table['rows'] as &$row ) {
    foreach ( $row as $cell_name => &$cell ) {
      if ( is_array($cell) ) {
        $cell['data'] = $cell['content'];
      }
    }
  }

  // Add totals only after table has been sorted
  $table['rows']['totals']['class'] = ['total'];
  $table['rows']['totals']['data'] = $totals;
  $table['rows']['totals']['no_striping'] = TRUE;

  $output = '<p>' . t('Click on the number inside a cell to go to the relevant table, filtered by area.') . '</p>';
  $output .= theme('table', $table);

  return $output;
}

/**
 * Processes the arguments for a dashboard block.
 */
function conference_dashboard_build_block_arguments($block) {
  $block_arguments = !empty($block['arguments']) ? explode(',', $block['arguments']) : [];

  foreach ( $block_arguments as $delta => $argument ) {
    $argument = trim($argument);

    if ( !empty($argument) ) {
      // Check if argument is declared as a function
      if ( stristr($argument, 'function:') ) {
        $argument = explode(':', $argument);
        $argument = call_user_func_array($argument[1], []);
      }

      $block_arguments[$delta] = $argument;
    }
    else {
      unset($block_arguments[$delta]);
    }
  }

  return $block_arguments;
}

/**
 * Produces the URL query for a dashboard block link.
 */
function conference_dashboard_build_block_query($block, &$link_options) {
  $block_query = !empty($block['query']) ? explode(',', $block['query']) : [];

  foreach ( $block_query as $delta => $query ) {
    $query = explode(':', $query);

    $link_options['query'][trim($query[0])] = trim($query[1]);
  }
}

/**
 * Returns the row count of a dashboard block's view. Queries for each block
 * are cached for performance, then executed before the count is returned.
 */
function conference_dashboard_block_count($block_name, $block_view, $arguments, $return_result = FALSE, $reset = FALSE) {
  $queries = &drupal_static(__FUNCTION__);
  $hash = sha1(serialize(func_get_args()));

  if ( empty($queries) ) {
    $cache = cache_get(__FUNCTION__);
    $queries = !empty($cache->data) ? $cache->data : [];
  }

  if ( empty($queries[$hash]) || $reset ) {
    list($view_name, $display_id) = $block_view;
    $view = views_get_view($view_name);

    if ( is_object($view) ) {
      if ( !empty($arguments) ) {
        $view->set_arguments($arguments);
      }

      if ( $view->build($display_id) ) {
        $queries[$hash] = $view->query->query();
        cache_set(__FUNCTION__, $queries, 'cache', CACHE_TEMPORARY);
      }
    }
  }

  if ( $return_result ) {
    return !empty($queries[$hash]) ? $queries[$hash]->execute()->fetchAll(PDO::FETCH_ASSOC) : [];
  }

  return !empty($queries[$hash]) ? $queries[$hash]->execute()->rowCount() : NULL;
}
