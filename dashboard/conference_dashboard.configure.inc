<?php
/**
 * @file
 * Defines conference-dashboard variables.
 */

/**
 * Implements hook_configure_variable_info().
 */
function conference_dashboard_configure_variable_info() {
  return [
    // Conference dashboard
    'conference_dashboard_setup' => [
      'group' => 'dashboard',
      'type' => 'matrix',
      'title' => t('Dashboard blocks'),
      'edit link' => 'admin/conference/settings/dashboard/%s/edit',
      'properties' => [
        'name' => [
          'title' => t('Name'),
        ],
        'display' => [
          'title' => t('Display name'),
        ],
        'view' => [
          'title' => t('View and display'),
          'options callback' => '_conference_get_views',
          'element' => [
            '#type' => 'select',
            '#options' => [t('- Select -')],
            '#required' => TRUE,
          ],
        ],
        'arguments' => [
          'title' => t('Arguments'),
          'element' => [
            '#description' => t('Separate arguments with commas.'),
          ],
        ],
        'path' => [
          'title' => t('Path'),
        ],
        'query' => [
          'title' => t('Query string'),
          'element' => [
            '#description' => t('Separate query elements with commas, in the form <code>key:value</code>.'),
          ],
        ],
        'note' => [
          'title' => t('Note'),
        ],
        'context' => [
          'title' => t('Context'),
          'element' => [
            '#type' => 'select',
            '#options' => [
              'admin' => t('Administrator'),
              'areas' => t('Area chair')
            ],
          ],
        ],
        'section' => [
          'title' => t('Section'),
          'element' => [
            '#type' => 'select',
            '#options' => [
              'conference' => t('Conference'),
              'presentations' => t('Presentations'),
              'sessions' => t('Sessions'),
              'speakers' => t('Speakers'),
            ],
          ],
        ],
        'total' => [
          'title' => t('Total'),
          'element' => [
            '#type' => 'checkbox',
            '#description' => t('If checked, the element will be shown in the row header as the total.'),
          ],
        ],
        'enabled' => [
          'title' => t('Enabled'),
          'element' => [
            '#type' => 'checkbox'
          ],
        ],
        'test' => [
          'title' => t('Test'),
          'element' => [
            '#type' => 'checkbox'
          ],
        ],
      ],
      'items' => [
        // Admin blocks
        'presentations',
        'presentations_pending',
        'presentations_approved',
        'presentations_rejected',
        'presentations_withdrawn',
        'presentations_pending_notification',
        'presentations_unassigned',
        'sessions',
        'sessions_orphan',
        'speakers_all',
        'speakers_approved',
        // Area blocks
        'presentations_area',
        'presentations_area_approved',
        'presentations_area_pending',
        'presentations_area_pending_notification',
        'sessions_area',
        'speakers_area_all',
        'speakers_area_approved',
      ],
    ],
  ];
}

/**
 * Implements hook_configure_variable_defaults().
 */
function conference_dashboard_configure_variable_defaults() {
  return [
    'conference_dashboard_setup' => [
      // Admin blocks
      'presentations' => [
        'name' => 'Presentations',
        'display' => 'Total',
        'view' => 'conference_presentations:presentations_all',
        'path' => 'admin/conference/presentations',
        'context' => 'admin',
        'section' => 'presentations',
        'total' => 1,
        'enabled' => 1
      ],
      'presentations_pending' => [
        'name' => 'Pending presentations',
        'display' => 'Pending',
        'view' => 'conference_presentations:presentations_pending',
        'arguments' => 'all, function:conference_current',
        'path' => 'admin/conference/presentations',
        'query' => 'rejected:0, approved:0',
        'context' => 'admin',
        'section' => 'presentations',
        'total' => 0,
        'enabled' => 1
      ],
      'presentations_approved' => [
        'name' => 'Approved presentations',
        'display' => 'Approved',
        'view' => 'conference_presentations:presentations_approved',
        'arguments' => 'all, function:conference_current',
        'path' => 'admin/conference/presentations',
        'query' => 'rejected:0, approved:1, withdrawn:0',
        'context' => 'admin',
        'section' => 'presentations',
        'total' => 0,
        'enabled' => 1
      ],
      'presentations_rejected' => [
        'name' => 'Rejected presentations',
        'display' => 'Rejected',
        'view' => 'conference_presentations:presentations_rejected',
        'arguments' => 'all, function:conference_current',
        'path' => 'admin/conference/presentations',
        'query' => 'rejected:1, approved:0',
        'context' => 'admin',
        'section' => 'presentations',
        'total' => 0,
        'enabled' => 1
      ],
      'presentations_withdrawn' => [
        'name' => 'Withdrawn presentations',
        'display' => 'Withdrawn',
        'view' => 'conference_presentations:presentations_withdrawn',
        'arguments' => 'all, function:conference_current',
        'path' => 'admin/conference/presentations',
        'query' => 'withdrawn:1',
        'context' => 'admin',
        'section' => 'presentations',
        'total' => 0,
        'enabled' => 1
      ],
      'presentations_pending_notification' => [
        'name' => 'Pending notifications',
        'view' => 'conference_presentations:notifications',
        'arguments' => 'all, function:conference_current',
        'path' => 'admin/conference/notifications',
        'context' => 'admin',
        'section' => 'presentations',
        'total' => 0,
        'enabled' => 1
      ],
      'presentations_unassigned' => [
        'name' => 'Unassigned presentations',
        'display' => 'Unassigned',
        'view' => 'conference_presentations:unassigned_count',
        'path' => 'admin/conference/presentations/unassigned',
        'note' => 'Missing session or presenter',
        'context' => 'admin',
        'section' => 'presentations',
        'total' => 0,
        'enabled' => 1
      ],
      'sessions' => [
        'name' => 'Sessions',
        'display' => 'Total',
        'view' => 'conference_sessions:sessions_all',
        'path' => 'admin/conference/sessions',
        'context' => 'admin',
        'section' => 'sessions',
        'total' => 1,
        'enabled' => 1
      ],
      'sessions_orphan' => [
        'name' => 'Orphan sessions',
        'display' => 'Orphans',
        'view' => 'conference_sessions:orphan_counter',
        'path' => 'admin/conference/sessions/orphan',
        'context' => 'admin',
        'section' => 'sessions',
        'total' => 0,
        'enabled' => 1
      ],
      'speakers_all' => [
        'name' => 'All speakers',
        'display' => 'Total',
        'view' => 'conference_speakers:counter',
        'arguments' => 'all, function:conference_current',
        'path' => 'admin/conference/speakers',
        'context' => 'admin',
        'section' => 'speakers',
        'total' => 1,
        'enabled' => 1,
      ],
      'speakers_approved' => [
        'name' => 'Approved speakers',
        'display' => 'Approved',
        'view' => 'conference_speakers:counter_approved',
        'arguments' => 'all, function:conference_current',
        'path' => 'admin/conference/speakers',
        'query' => 'approval:1',
        'context' => 'admin',
        'section' => 'speakers',
        'total' => 0,
        'enabled' => 1,
      ],
      // Area blocks
      'presentations_area' => [
        'name' => 'Presentations',
        'display' => 'Total',
        'view' => 'conference_presentations:presentations_area',
        'arguments' => '%area, function:conference_current',
        'path' => 'admin/conference/areas/presentations',
        'query' => 'area:%area',
        'context' => 'areas',
        'section' => 'presentations',
        'total' => 1,
        'enabled' => 1
      ],
      'presentations_area_approved' => [
        'name' => 'Approved presentations',
        'display' => 'Approved',
        'view' => 'conference_presentations:presentations_approved',
        'arguments' => '%area, function:conference_current',
        'path' => 'admin/conference/areas/presentations',
        'query' => 'approved:1, rejected:0, area:%area',
        'context' => 'areas',
        'section' => 'presentations',
        'total' => 0,
        'enabled' => 1
      ],
      'presentations_area_pending' => [
        'name' => 'Pending presentations',
        'display' => 'Pending',
        'view' => 'conference_presentations:presentations_pending',
        'arguments' => '%area, function:conference_current',
        'path' => 'admin/conference/areas/presentations',
        'query' => 'approved:0, rejected:0, area:%area',
        'context' => 'areas',
        'section' => 'presentations',
        'total' => 0,
        'enabled' => 1
      ],
      'presentations_area_pending_notification' => [
        'name' => 'Pending notifications',
        'view' => 'conference_presentations:notifications',
        'arguments' => '%area, function:conference_current',
        'path' => 'admin/conference/areas/notifications',
        'query' => 'area:%area',
        'context' => 'areas',
        'section' => 'presentations',
        'total' => 0,
        'enabled' => 1
      ],
      'sessions_area' => [
        'name' => 'Sessions',
        'display' => 'Total',
        'view' => 'conference_sessions:sessions_area',
        'arguments' => '%area, function:conference_current',
        'path' => 'admin/conference/areas/sessions',
        'query' => 'area:%area',
        'context' => 'areas',
        'section' => 'sessions',
        'total' => 1,
        'enabled' => 1
      ],
      'speakers_area_all' => [
        'name' => 'All speakers',
        'display' => 'Total',
        'view' => 'conference_speakers:counter',
        'arguments' => '%area, function:conference_current',
        'path' => 'admin/conference/areas/speakers',
        'query' => 'area:%area',
        'context' => 'areas',
        'section' => 'speakers',
        'total' => 1,
        'enabled' => 1,
      ],
      'speakers_area_approved' => [
        'name' => 'Approved speakers',
        'display' => 'Approved',
        'view' => 'conference_speakers:counter_approved',
        'arguments' => '%area, function:conference_current',
        'path' => 'admin/conference/areas/speakers',
        'query' => 'area:%area, approval:approved',
        'context' => 'areas',
        'section' => 'speakers',
        'total' => 0,
        'enabled' => 1,
      ],
    ],
  ];
}