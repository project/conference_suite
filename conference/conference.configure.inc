<?php
/**
 * @file
 * Defines conference-related variables.
 */

/**
 * Implements hook_configure_variable_info().
 */
function conference_configure_variable_info() {
  $variables = [
    'conference_current' => [
      'options callback' => 'conference_get_conferences_options_list',
      'element' => [
        '#type' => 'select',
        '#title' => t('Current conference'),
        '#options' => [],
        '#required' => TRUE,
      ],
    ],
    'conference_day_date_pattern' => [
      'group' => 'misc',
      'title' => t('Full-day date pattern'),
      'options callback' => 'date_format_type_options',
      'default' => 'long',
      'element' => [
        '#type' => 'select',
        '#description' => t('Human-readable date format as returned by <code>Conference::days()</code>.'),
        '#options' => [],
      ],
    ],
  ];

  return $variables;
}

/**
 * Implements hook_configure_variable_set().
 */
function conference_configure_variable_set($variables) {
  if ($variables['name'] == 'conference_current') {
    // Reset Conference::current()
    drupal_static_reset('conference_current');
  }
}
