<?php

/**
 * @file
 * Definition of conference_views_plugin_argument_default_conference_current_id.
 */

/**
 * Default argument plugin to get the NID of the current conference.
 */
class conference_views_plugin_argument_default_conference_current_id extends views_plugin_argument_default {

  /**
   * {@inheritdoc}
   */
  public function get_argument() {
    return Conference::current();
  }
}