<?php
/**
 * @file
 * Hooks for Conference.
 */

/**
 * Allows modules to alter the list of available area filters.
 */
function hook_conference_area_filter_names_alter(&$area_filters) {

}

/**
 * Allows modules to alter the array of conference days.
 */
function hook_conference_get_days_alter(&$conference_days, $conference) {

}

/**
 * Allows modules to alter the list of possible operation links
 * used as an entity's extra fields.
 *
 * @see conference_entity_operations()
 * @see conference_field_extra_fields()
 * @see conference_node_view()
 */
function hook_conference_entity_operations_alter(&$operations, $entity, $type, $view_mode, $langcode) {

}
