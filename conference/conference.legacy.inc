<?php
/**
 * @file
 * Legacy/deprecated functions for the conference module.
 */

/**
 * Returns the nid of the current conference.
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::current() instead.
 */
function conference_current() {
  return Conference::current();
}

/**
 * Returns an array of areas as full node objects keyed by nid.
 *
 * @return array
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::getAreas() instead.
 */
function conference_get_areas() {
  return Conference::getAreas();
}

/**
 * Returns the conference node associated with a certain entity,
 * or the current conference.
 *
 * @param null $entity
 * @param string $entity_type
 *
 * @return bool|mixed|null
 * @throws \EntityMetadataWrapperException
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::getEntityConference() instead.
 */
function conference_get_conference($entity = NULL, $entity_type = 'node') {
  return Conference::getEntityConference($entity, $entity_type);
}

/**
 * Returns a conference node for a given year.
 *
 * @param $year
 *
 * @return mixed|null
 * @throws \EntityMetadataWrapperException
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::getByYear() instead.
 */
function conference_get_conference_by_year($year) {
  return Conference::getByYear($year);
}

/**
 * Returns the year for a given conference.
 *
 * @param null $conference
 *
 * @return mixed
 * @throws \EntityMetadataWrapperException
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::year() instead.
 */
function conference_get_conference_year($conference = NULL) {
  return Conference::year($conference);
}

/**
 * Returns an array of conferences, used to populate select lists.
 *
 * @param array $nids
 * @param null $length
 * @param int $start
 *
 * @return array
 * @throws \EntityFieldQueryException
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::optionsList() instead.
 */
function conference_get_conferences_options_list($nids = [], $length = NULL, $start = 0) {
  return Conference::optionsList($nids, $length, $start);
}

/**
 * Returns an array of days for a specific conference.
 *
 * @param null $conference
 *
 * @return array
 * @throws \EntityMetadataWrapperException
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::days() instead.
 */
function conference_get_days($conference = NULL) {
  return Conference::days($conference);
}

/**
 * Returns an array of views, used to populate select lists.
 *
 * @return array
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::getViews() instead.
 */
function _conference_get_views() {
  return Conference::getViews();
}

/**
 * Returns area nodes of which a certain user is chair.
 *
 * @param null $account
 * @param bool $full
 *
 * @return array
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::getUserAreas() instead.
 */
function _conference_get_user_areas($account = NULL, $full = FALSE) {
  return Conference::getUserAreas($account, $full);
}

/**
 * Checks whether user is area chair.
 *
 * @param $node
 * @param null $account
 *
 * @return bool
 *
 * @deprecated in 7.x-4.x. Will be removed in 7.x-5.x.
 *   Use Conference::isAreaChair() instead.
 */
function _is_area_chair($node, $account = NULL) {
  return Conference::isAreaChair($node, $account);
}
