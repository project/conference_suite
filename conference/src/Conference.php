<?php

/**
 * Class Conference
 */
class Conference {

  /**
   * Returns the nid of the current conference.
   *
   * @return int
   */
  public static function current() {
    $variable = 'conference_current';
    $conference = &drupal_static($variable);

    if (empty($conference)) {
      $conference = cvget($variable);
    }

    return (int) $conference;
  }

  /**
   * Returns the nid of the previous conference.
   *
   * @return int|null
   */
  public static function previous() {
    try {
      $previous_node = static::getByYear(static::year() - 1);
      $previous = $previous_node->nid;

      return (int) $previous;
    } catch (EntityMetadataWrapperException $e) {
      return NULL;
    }
  }

  /**
   * Returns an array of areas as full node objects keyed by nid.
   *
   * @return array
   */
  public static function getAreas() {
    $areas = (new EntityFieldQuery)
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'area')
      ->propertyOrderBy('title')
      ->execute();

    return !empty($areas) ? node_load_multiple(array_keys($areas['node'])) : [];
  }

  /**
   * Returns area nodes of which a certain user is chair.
   *
   * @param null $account
   * @param bool $full
   *
   * @return array
   */
  public static function getUserAreas($account = NULL, $full = FALSE) {
    $account = empty($account) ? $GLOBALS['user'] : $account;
    $area_chair_uid = is_object($account) ? $account->uid : $account;

    $user_areas = &drupal_static('conference_user_areas');

    if (empty($user_areas[$area_chair_uid])) {
      $areas = (new EntityFieldQuery)
        ->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'area')
        ->fieldCondition('conference_area_chair', 'target_id', $area_chair_uid)
        ->execute();
      $user_areas[$area_chair_uid] = reset($areas);
    }

    if (!empty($user_areas[$area_chair_uid])) {
      if ($full) {
        return entity_load('node', array_keys($user_areas[$area_chair_uid]));
      }

      return $user_areas[$area_chair_uid];
    }

    return [];
  }

  /**
   * Checks whether user is area chair.
   *
   * @param object $node
   *   The node.
   * @param mixed $account
   *   If NULL, the active account will be used.
   *
   * @return bool
   *   If TRUE, the user is an area chair related to the node.
   */
  public static function isAreaChair($node, $account = NULL) {
    /** @var \EntityDrupalWrapper $w */
    $w = entity_metadata_wrapper('node', $node);

    try {
      $type = $w->get('type')->value();
    }
    catch (EntityMetadataWrapperException $e) {
      return FALSE;
    }

    $account = $account ?: $GLOBALS['user'];

    switch ($type) {
      case 'area':
        $conference_area_chair = $w->conference_area_chair->raw();
        if (!empty($conference_area_chair)) {
          if (in_array($account->uid, $conference_area_chair)) {
            return TRUE;
          }
        }
        break;

      case 'presentation':
        $area = $w->conference_area->raw();
        return !empty($area) ? static::isAreaChair($area, $account) : FALSE;

      case 'session':
        if ($areas = $w->conference_areas->raw()) {
          foreach ($areas as $nid) {
            // Return TRUE as soon as a match is found.
            return static::isAreaChair($nid, $account);
          }
        }
        break;
    }
  }

  /**
   * Returns the conference node associated with a certain entity,
   * or the current conference.
   *
   * @param null $entity
   * @param string $entity_type
   *
   * @return bool|mixed|null
   * @throws \EntityMetadataWrapperException
   */
  public static function getEntityConference($entity = NULL, $entity_type = 'node') {
    if (!empty($entity)) {
      /**
       * @var \EntityDrupalWrapper $w
       */
      $w = entity_metadata_wrapper($entity_type, $entity);

      // If the entity is a conference node
      if ($entity_type == 'node' && $w->get('type')->value() == 'conference') {
        return $w->value();
      }

      try {
        return $w->conference_reference->value();
      }
      catch (EntityMetadataWrapperException $e) {
        // If the entity has no conference_reference field
        return NULL;
      }
    }

    return node_load(static::current());
  }

  /**
   * Returns a conference node for a given year.
   *
   * @param $year
   *
   * @return mixed|null
   * @throws \EntityMetadataWrapperException
   */
  public static function getByYear($year) {
    static $conferences;
    $conferences = empty($conferences) ? [] : $conferences;

    if (!isset($conferences[$year])) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'conference')
        ->fieldCondition('conference_year', 'value', $year);
      $query = $query->execute();
      $query = reset($query);

      if (!empty($query)) {
        foreach ($query as $nid => $row) {
          $node = node_load($row->nid);
          /**
           * @var \EntityDrupalWrapper $w
           */
          $w = entity_metadata_wrapper('node', $nid);
          $date = $w->get('conference_dates')->value();
          if (!empty($date)) {
            $conferences[$year] = $node;
          }
        }
      }
    }

    if (!empty($conferences[$year])) {
      return $conferences[$year];
    }

    return NULL;
  }

  /**
   * Returns the year for a given conference.
   *
   * @param null $conference
   *
   * @return mixed
   * @throws \EntityMetadataWrapperException
   */
  public static function year($conference = NULL) {
    static $conferences = [];
    $conference = empty($conference) ? static::current() : $conference;
    /**
     * @var \EntityDrupalWrapper $wrapper
     */
    $wrapper = entity_metadata_wrapper('node', $conference);
    $nid = $wrapper->nid->value();

    if (!isset($conferences[$nid])) {
      $conferences[$nid] = $wrapper->get('conference_year')->value();
    }

    return $conferences[$nid];
  }

  /**
   * Returns an array of days for a specific conference.
   *
   * @param null $conference
   *
   * @return array
   * @throws \EntityMetadataWrapperException
   */
  public static function days($conference = NULL) {
    $conference = empty($conference) ? static::current() : $conference;
    /**
     * @var \EntityDrupalWrapper $w
     */
    $w = entity_metadata_wrapper('node', $conference);
    $date_format = cvget('conference_day_date_pattern');
    $nid = $w->get('nid')->value();
    $id = "conference_days:" . $nid;
    $conference_days = &drupal_static($id);

    if (empty($conference_days) && $cache = cache_get($id)) {
      $conference_days = $cache->data;
    }

    if (empty($conference_days[$nid])) {
      $dates = $w->get('conference_dates')->value();

      // Duration of the conference in days
      // Start and end dates don't take into account full days, so
      // the duration is actually one day short, but that's okay
      $duration = ($dates['value2'] - $dates['value']) / 86400;

      // Used to sanitize day names for URL generation
      module_load_include('inc', 'pathauto');

      for ($i = 0; $i <= $duration; $i++) {
        $conference_day = $dates['value'] + $i * 86400;
        $conference_date = format_date($conference_day, $date_format);
        $conference_weekday = drupal_strtolower(pathauto_cleanstring(format_date($conference_day, 'custom', 'l')));

        $conference_days[$nid][$conference_weekday] = [
          'day' => $conference_day,
          'date' => $conference_date,
        ];
      }

      $conference = $w->value();
      drupal_alter('conference_get_days', $conference_days, $conference);
      cache_set($id, $conference_days, 'cache', CACHE_TEMPORARY);
    }

    return empty($conference_days[$nid]) ? [] : $conference_days[$nid];
  }

  /**
   * Returns an array of conferences, used to populate select lists.
   *
   * @param array $nids
   * @param null $length
   * @param int $start
   *
   * @return array
   * @throws \EntityFieldQueryException
   */
  public static function optionsList($nids = [], $length = NULL, $start = 0) {
    $conferences = (new EntityFieldQuery)
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'conference')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldOrderBy('conference_year', 'value', 'DESC');

    if (!empty($nids)) {
      $conferences->propertyCondition('nid', $nids);
    }

    if (!empty($length)) {
      $conferences->range($start, $length);
    }

    $conferences = $conferences->execute();

    if (!empty($conferences['node'])) {
      foreach (array_keys($conferences['node']) as $nid) {
        $w = entity_metadata_wrapper('node', $nid);
        $return[$nid] = $w->get('conference_year')->value() . ' – ' . $w->get('title')->value();
      }
    }

    return empty($return) ? [] : $return;
  }

  /**
   * Returns an array of views, used to populate select lists.
   *
   * @return array
   */
  public static function getViews() {
    $views_list = views_get_all_views();
    ksort($views_list);

    $views = [];

    foreach ($views_list as $name => $view) {
      ksort($view->display);
      foreach ($view->display as $display_name => $display) {
        $views[$view->human_name . ' (' . $name . ')'][$name . ':' . $display_name] = $display->display_title . ' (' . $display_name . ')';
      }
    }

    return $views;
  }
}
