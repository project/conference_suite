<?php
/**
 * @file
 *
 * Mailer support for Conference module.
 */

/**
 * Implements hook_default_mailer_type().
 */
function conference_default_mailer_type() {
  watchdog('conference', print_r(debug_backtrace(), TRUE));
  $types = [
    // Contact chair(s) of single area
    [
      'type' => 'conference_area_chair_single',
      'label' => 'Contact chairs of single area',
      'recipients' => ['user'],
    ],
  ];

  foreach ($types as $type) {
    $type['module'] = 'conference';
    $defaults[$type['type']] = entity_create('mailer_type', $type);
  }

  return $defaults;
}

/**
 * Implements hook_mailer_info().
 */
function conference_mailer_info() {
  return [
    'conference_area_chair_single' => [
      'interface' => 'block',
      'recipients' => [
        'user',
        'custom',
      ],
    ],
    'conference_area_chair_single_page' => [
      'interface' => 'page',
      'recipients' => [
        'user',
        'custom',
      ],
    ],
  ];
}