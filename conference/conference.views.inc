<?php
/**
 * @file
 * Customizes views for Conference module.
 */

/**
 * Implements hook_views_plugin().
 *
 * @return array
 */
function conference_views_plugins() {
  return [
    'module' => 'views',
    'argument default' => [
      'conference_current_id' => [
        'title' => t('Current conference ID'),
        'handler' => 'conference_views_plugin_argument_default_conference_current_id',
      ],
    ],
  ];
}

/**
 * Implements hook_views_pre_build().
 *
 * - Sets the default value of a conference_reference filter to the
 *   current conference.
 * - Restricts the areas listed in a conference_areas filter to the
 *   areas managed by the current user.
 */
function conference_views_pre_build(&$view) {
  if (!empty($view->filter['conference_reference_target_id_entityreference_filter'])) {
    // Sets the default value for the conference filter
    $view->filter['conference_reference_target_id_entityreference_filter']->value = [Conference::current()];
  }

  foreach (conference_area_filter_names() as $filter) {
    if (!empty($view->filter[$filter])) {
      // Always use current user's ID
      $area_chair_uid = $GLOBALS['user']->uid;

      $user_areas_result = array_keys((array) Conference::getUserAreas($area_chair_uid));
      $default_area = reset($user_areas_result);
      // Sets the default value for the area filter
      $view->filter[$filter]->value = $default_area;
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function conference_form_views_exposed_form_alter(&$form, $form_state) {
  $view = $form_state['view'];

  foreach (conference_area_filter_names() as $filter) {
    if (!empty($view->filter[$filter])) {
      _conference_restrict_areas_by_user($form, $view, $filter);
    }
  }
}

/**
 * Restricts the list of areas in the admin/conference/session/chair exposed
 * form to areas chaired by the user in the view argument.
 *
 * @see conference_form_views_exposed_form_alter()
 */
function _conference_restrict_areas_by_user(&$form, $view, $area_field_name) {
  // Always use current user's ID
  $area_chair_uid = $GLOBALS['user']->uid;
  $user_areas_result = Conference::getUserAreas($area_chair_uid);

  // Check permission of current user
  if (!user_access('filter view by any area')) {
    if (!empty($user_areas_result)) {
      foreach ($user_areas_result as $nid => $area) {
        $areas_restricted[$nid] = strip_tags($view->filter[$area_field_name]->value_options[$nid]);
      }
    }
  }
  else {
    if ($area_chair_uid == $GLOBALS['user']->uid && !empty($user_areas_result)) {
      foreach ($user_areas_result as $nid => $area) {
        $areas_restricted[t('Your areas')][$nid] = strip_tags($view->filter[$area_field_name]->value_options[$nid]);
      }

      $areas_restricted[t('Other areas')] = array_diff($form['area']['#options'], $areas_restricted[t('Your areas')]);
    }
  }

  if (!empty($areas_restricted)) {
    $id = $view->filter[$area_field_name]->options['expose']['identifier'];
    $form[$id]['#options'] = $areas_restricted;
  }
}

/**
 * Returns a list of area filter names.
 */
function conference_area_filter_names() {
  $area_filters = [
    'conference_area_target_id_entityreference_filter',
    'conference_areas_target_id_entityreference_filter',
  ];

  drupal_alter('conference_area_filter_names', $area_filters);

  return $area_filters;
}
