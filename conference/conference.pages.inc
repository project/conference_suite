<?php
/**
 * @file
 * Page callback file for the Conference module.
 */

/**
 * Menu callback: delivers the node for the current conference.
 */
function conference_pages_current() {
  menu_set_active_item('node/' . Conference::current());
  return menu_execute_active_handler(NULL, FALSE);
}