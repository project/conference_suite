<?php
/**
 * @file
 * conference_date.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function conference_date_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'conference_date_references';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'conference_date';
  $view->human_name = 'Conference date references';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Conference Date: Conference */
  $handler->display->display_options['relationships']['conference']['id'] = 'conference';
  $handler->display->display_options['relationships']['conference']['table'] = 'conference_date';
  $handler->display->display_options['relationships']['conference']['field'] = 'conference';
  $handler->display->display_options['relationships']['conference']['label'] = 'Conference';
  $handler->display->display_options['relationships']['conference']['required'] = TRUE;
  /* Field: Content: Conference year */
  $handler->display->display_options['fields']['conference_year']['id'] = 'conference_year';
  $handler->display->display_options['fields']['conference_year']['table'] = 'field_data_conference_year';
  $handler->display->display_options['fields']['conference_year']['field'] = 'conference_year';
  $handler->display->display_options['fields']['conference_year']['relationship'] = 'conference';
  $handler->display->display_options['fields']['conference_year']['label'] = '';
  $handler->display->display_options['fields']['conference_year']['element_label_colon'] = FALSE;
  /* Field: Conference Date: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'conference_date';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  /* Sort criterion: Conference Date: Start date */
  $handler->display->display_options['sorts']['from_date']['id'] = 'from_date';
  $handler->display->display_options['sorts']['from_date']['table'] = 'conference_date';
  $handler->display->display_options['sorts']['from_date']['field'] = 'from_date';
  /* Contextual filter: Conference Date: Conference */
  $handler->display->display_options['arguments']['conference']['id'] = 'conference';
  $handler->display->display_options['arguments']['conference']['table'] = 'conference_date';
  $handler->display->display_options['arguments']['conference']['field'] = 'conference';
  $handler->display->display_options['arguments']['conference']['default_action'] = 'default';
  $handler->display->display_options['arguments']['conference']['default_argument_type'] = 'conference_current_id';
  $handler->display->display_options['arguments']['conference']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['conference']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['conference']['summary_options']['items_per_page'] = '25';

  /* Display: Dates */
  $handler = $view->new_display('entityreference', 'Dates', 'dates');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'conference_year' => 'conference_year',
    'type' => 'type',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['row_options']['separator'] = '';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['conference_date_references'] = $view;

  $view = new view();
  $view->name = 'conference_dates';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'conference_date';
  $view->human_name = 'Conference dates';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Conference Dates';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer conference-date entities';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'conference_year' => 'conference_year',
    'type' => 'type',
    'from_date' => 'from_date',
    'to_date' => 'to_date',
    'hard_deadline' => 'hard_deadline',
    'name' => 'name',
    'created' => 'created',
    'changed' => 'changed',
    'edit_link' => 'edit_link',
    'type_1' => 'type_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'conference_year' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'from_date' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'to_date' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'hard_deadline' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'changed' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_link' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['content'] = 'Start date and deadlines are expressed in the current conference\'s time zone.';
  $handler->display->display_options['header']['area']['format'] = 'markdown';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There is no timeline.';
  $handler->display->display_options['empty']['area']['format'] = 'plain_text';
  /* Relationship: Conference Date: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'conference_date';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['label'] = 'Author';
  /* Relationship: Conference Date: Conference */
  $handler->display->display_options['relationships']['conference']['id'] = 'conference';
  $handler->display->display_options['relationships']['conference']['table'] = 'conference_date';
  $handler->display->display_options['relationships']['conference']['field'] = 'conference';
  $handler->display->display_options['relationships']['conference']['label'] = 'Conference';
  $handler->display->display_options['relationships']['conference']['required'] = TRUE;
  /* Field: Content: Conference year */
  $handler->display->display_options['fields']['conference_year']['id'] = 'conference_year';
  $handler->display->display_options['fields']['conference_year']['table'] = 'field_data_conference_year';
  $handler->display->display_options['fields']['conference_year']['field'] = 'conference_year';
  $handler->display->display_options['fields']['conference_year']['relationship'] = 'conference';
  $handler->display->display_options['fields']['conference_year']['label'] = 'Year';
  /* Field: Conference Date: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'conference_date';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  /* Field: Conference Date: Start date */
  $handler->display->display_options['fields']['from_date']['id'] = 'from_date';
  $handler->display->display_options['fields']['from_date']['table'] = 'conference_date';
  $handler->display->display_options['fields']['from_date']['field'] = 'from_date';
  $handler->display->display_options['fields']['from_date']['date_format'] = 'short';
  $handler->display->display_options['fields']['from_date']['second_date_format'] = 'html5_tools_iso8601';
  /* Field: Conference Date: End date */
  $handler->display->display_options['fields']['to_date']['id'] = 'to_date';
  $handler->display->display_options['fields']['to_date']['table'] = 'conference_date';
  $handler->display->display_options['fields']['to_date']['field'] = 'to_date';
  $handler->display->display_options['fields']['to_date']['label'] = 'Soft deadline';
  $handler->display->display_options['fields']['to_date']['date_format'] = 'short';
  $handler->display->display_options['fields']['to_date']['second_date_format'] = 'html5_tools_iso8601';
  /* Field: Conference Date: Hard deadline */
  $handler->display->display_options['fields']['hard_deadline']['id'] = 'hard_deadline';
  $handler->display->display_options['fields']['hard_deadline']['table'] = 'views_entity_conference_date';
  $handler->display->display_options['fields']['hard_deadline']['field'] = 'hard_deadline';
  $handler->display->display_options['fields']['hard_deadline']['date_format'] = 'short';
  $handler->display->display_options['fields']['hard_deadline']['second_date_format'] = 'html5_tools_iso8601';
  $handler->display->display_options['fields']['hard_deadline']['link_to_entity'] = 0;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Author';
  /* Field: Conference Date: Date created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'conference_date';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Created';
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  /* Field: Conference Date: Date changed */
  $handler->display->display_options['fields']['changed']['id'] = 'changed';
  $handler->display->display_options['fields']['changed']['table'] = 'conference_date';
  $handler->display->display_options['fields']['changed']['field'] = 'changed';
  $handler->display->display_options['fields']['changed']['label'] = 'Updated';
  $handler->display->display_options['fields']['changed']['date_format'] = 'short';
  $handler->display->display_options['fields']['changed']['second_date_format'] = 'html5_tools_iso8601';
  /* Field: Conference Date: Edit link */
  $handler->display->display_options['fields']['edit_link']['id'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['table'] = 'views_entity_conference_date';
  $handler->display->display_options['fields']['edit_link']['field'] = 'edit_link';
  $handler->display->display_options['fields']['edit_link']['label'] = '';
  $handler->display->display_options['fields']['edit_link']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['edit_link']['alter']['text'] = 'Edit';
  $handler->display->display_options['fields']['edit_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['edit_link']['link_to_entity'] = 0;
  /* Filter criterion: Conference Date: Conference */
  $handler->display->display_options['filters']['conference']['id'] = 'conference';
  $handler->display->display_options['filters']['conference']['table'] = 'conference_date';
  $handler->display->display_options['filters']['conference']['field'] = 'conference';
  $handler->display->display_options['filters']['conference']['group'] = 1;
  $handler->display->display_options['filters']['conference']['exposed'] = TRUE;
  $handler->display->display_options['filters']['conference']['expose']['operator_id'] = 'conference_op';
  $handler->display->display_options['filters']['conference']['expose']['label'] = 'Conference';
  $handler->display->display_options['filters']['conference']['expose']['operator'] = 'conference_op';
  $handler->display->display_options['filters']['conference']['expose']['identifier'] = 'conference';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/conference/timeline';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Timeline';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['conference_dates'] = $view;

  return $export;
}
