<?php
/**
 * @file
 * conference_date.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function conference_date_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-conference-conference_timezone'.
  $field_instances['node-conference-conference_timezone'] = array(
    'bundle' => 'conference',
    'default_value' => NULL,
    'default_value_function' => '_conference_date_conference_timezone_default',
    'default_value_php' => '',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'conference_timezone',
    'label' => 'Time zone',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Time zone');

  return $field_instances;
}
