<?php
/**
 * @file
 * Defines date-related variables.
 */

/**
 * Implements hook_configure_variable_info().
 */
function conference_date_configure_variable_info() {
  $variables = [
    'conference_date_allowed_timezones' => [
      'options callback' => 'system_time_zones',
      'element' => [
        '#type' => 'checkboxes',
        '#title' => t('Allowed time zones'),
        '#options' => [],
      ],
      'default' => [],
    ],
  ];

  return $variables;
}

/**
 * Provides default values.
 */
function conference_date_configure_variable_defaults() {
  return [
    'conference_date_allowed_timezones' => system_time_zones(),
  ];
}

/**
 * Implements hook_configure_variable_set_value_alter().
 *
 * Removes empty elements from the time-zone array before saving.
 */
function conference_date_configure_variable_set_value_alter($variable_name, &$value) {
  if ($variable_name == 'conference_date_allowed_timezones') {
    $value = array_filter($value);
  }
}
