<?php
/**
 * @file
 * Functions to handle date conversions.
 */
function conference_date_convert_timezone($field_name) {
  $field_info = field_info_field($field_name);
  $timezone = variable_get('date_default_timezone', date_default_timezone_get());

  if ($field_info['settings']['tz_handling'] == 'site') {
    $tables[] = "field_data_{$field_name}";
    $tables[] = "field_revision_{$field_name}";

    $map = array_intersect_key(field_info_field_map(), array_flip([$field_name]));

    foreach ($tables as $table) {
      $schema = drupal_get_schema($table);
      $new_table = "tmp_$table";

      if (!db_table_exists($new_table)) {
        db_create_table($new_table, $schema);

        $rows = db_select($table, 't')
          ->fields('t');

        db_insert($new_table)
          ->from($rows)
          ->execute();
      }
    }

    foreach ($map as $field => $info) {
      foreach ($info['bundles'] as $entity_type => $bundles) {
        foreach ($bundles as $bundle_name) {
          $instance = field_info_instance($entity_type, $field, $bundle_name);
          $field_info_instances[] = $instance;
          field_delete_instance($instance, TRUE);
        }
      }
    }

    field_sync_field_status();
    field_purge_batch(10);

    unset($field_info['settings']['timezone_db']);
    $field_info['settings']['tz_handling'] = 'date';
    field_create_field($field_info);

    foreach ($field_info_instances as $instance) {
      field_create_instance($instance);
    }

    field_sync_field_status();

    // Restore data
    foreach ($tables as $table) {
      $new_table = "tmp_$table";

      $rows = db_select("tmp_$table", 't')
        ->fields('t')
        ->execute()
        ->fetchAllAssoc('entity_id');

      $insert = db_insert($table)
        ->fields(['entity_type', 'bundle', 'deleted', 'entity_id', 'revision_id', 'language', 'delta', "{$field_name}_value", "{$field_name}_value2", "{$field_name}_timezone", "{$field_name}_offset", "{$field_name}_offset2"]);

      foreach ($rows as $nid => $row) {
        $row = (array) $row;

        $start = new DateObject($row["{$field_name}_value"], $timezone);
        $end = new DateObject($row["{$field_name}_value"], $timezone);

        $row["{$field_name}_timezone"] = $timezone;
        $row["{$field_name}_offset"] = $start->getOffset();
        $row["{$field_name}_offset2"] = $end->getOffset();

        $insert->values($row);
      }

      $insert->execute();

      db_drop_table($new_table);
    }
  }


  return '';
}