<?php
/**
 * @file
 * conference_date.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function conference_date_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'conference_timezone'.
  $field_bases['conference_timezone'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'conference_timezone',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(),
      'allowed_values_function' => '_conference_date_conference_timezone_values',
      'allowed_values_php' => '',
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
