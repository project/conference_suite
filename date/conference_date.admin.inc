<?php
/**
 * @file
 * Provides the administrative interface for conference_date entities.
 *
 * @todo Change menu paths.
 */

/**
 * Generates the date type editing form.
 */
function conference_date_type_form($form, &$form_state, $conference_date_type, $op = 'edit') {

  if ($op == 'clone') {
    $conference_date_type->label .= ' (cloned)';
    $conference_date_type->type = '';
  }

  $form['label'] = [
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $conference_date_type->label,
    '#description' => t('The human-readable name of this date type.'),
    '#required' => TRUE,
    '#size' => 30,
  ];

  // Machine-readable type name.
  $form['type'] = [
    '#type' => 'machine_name',
    '#default_value' => isset($conference_date_type->type) ? $conference_date_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $conference_date_type->isLocked() && $op != 'clone',
    '#machine_name' => [
      'exists' => 'conference_date_types',
      'source' => ['label'],
    ],
    '#description' => t('A unique machine-readable name for this date type. It must only contain lowercase letters, numbers, and underscores.'),
  ];

  $form['description'] = [
    '#type' => 'textarea',
    '#default_value' => isset($conference_date_type->description) ? $conference_date_type->description : '',
    '#description' => t('Description of the date type.'),
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save date type'),
    '#weight' => 40,
  ];

  if (!$conference_date_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete date type'),
      '#weight' => 45,
      '#limit_validation_errors' => [],
      '#submit' => ['conference_date_type_form_submit_delete'],
    ];
  }
  return $form;
}

/**
 * Submit handler for creating/editing conference_date_type.
 */
function conference_date_type_form_submit(&$form, &$form_state) {
  $conference_date_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  conference_date_type_save($conference_date_type);

  // Redirect user back to list of date types.
  $form_state['redirect'] = 'admin/structure/conference-date';
}

function conference_date_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/conference-date/' . $form_state['conference_date_type']->type . '/delete';
}

/**
 * Conference Date Type delete form.
 */
function conference_date_type_form_delete_confirm($form, &$form_state, $conference_date_type) {
  $form_state['conference_date_type'] = $conference_date_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['conference_date_type_id'] = ['#type' => 'value', '#value' => entity_id('conference_date_type', $conference_date_type)];
  return confirm_form($form,
    t('Are you sure you want to delete date type %title?', ['%title' => entity_label('conference_date_type', $conference_date_type)]),
    'admin/conference/timeline/' . entity_id('conference_date_type', $conference_date_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Conference Date Type delete form submit handler.
 */
function conference_date_type_form_delete_confirm_submit($form, &$form_state) {
  $conference_date_type = $form_state['conference_date_type'];
  conference_date_type_delete($conference_date_type);

  watchdog('conference_date_type', '@type: deleted %title.', ['@type' => $conference_date_type->type, '%title' => $conference_date_type->label]);
  drupal_set_message(t('@type %title has been deleted.', ['@type' => $conference_date_type->type, '%title' => $conference_date_type->label]));

  $form_state['redirect'] = 'admin/structure/conference-date';
}

/**
 * Page to select Conference Date Type to add new conference date.
 */
function conference_date_admin_add_page() {
  $items = [];
  foreach (conference_date_types() as $conference_date_type_key => $conference_date_type) {
    $items[] = l(entity_label('conference_date_type', $conference_date_type), 'admin/conference/timeline/add/' . $conference_date_type_key);
  }
  return ['list' => ['#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of conference date to create.')]];
}

/**
 * Add new conference date page callback.
 */
function conference_date_add($type) {
  $conference_date_type = conference_date_types($type);

  $conference_date = entity_create('conference_date', ['type' => $type]);
  drupal_set_title(t('Create @name', ['@name' => entity_label('conference_date_type', $conference_date_type)]));

  $output = drupal_get_form('conference_date_form', $conference_date);

  return $output;
}

/**
 * Conference Date Form.
 */
function conference_date_form($form, &$form_state, $conference_date) {
  $form_state['conference_date'] = $conference_date;
  $type = conference_date_types($conference_date->type)->label;
  drupal_set_title(t('Edit %date_type date', ['%date_type' => $type]), PASS_THROUGH);

  $form['conference'] = [
    '#type' => 'select',
    '#title' => t('Conference'),
    '#options' => [t('- Select -')] + Conference::optionsList(),
    '#default_value' => $conference_date->conference,
    '#required' => TRUE,
  ];

  $form['from_date'] = [
    '#type' => 'date',
    '#title' => t('Start date'),
    '#description' => t('Effective at the beginning of the day in the time zone of the selected conference.'),
    '#default_value' => [
      'year' => format_date($conference_date->from_date, 'custom', 'Y'),
      'month' => format_date($conference_date->from_date, 'custom', 'n'),
      'day' => format_date($conference_date->from_date, 'custom', 'j'),
    ],
    '#required' => TRUE,
  ];

  $form['to_date'] = [
    '#type' => 'date',
    '#title' => t('End date'),
    '#description' => t('Effective at the end of the day in the time zone of the selected conference.'),
    '#default_value' => [
      'year' => format_date($conference_date->to_date, 'custom', 'Y'),
      'month' => format_date($conference_date->to_date, 'custom', 'n'),
      'day' => format_date($conference_date->to_date, 'custom', 'j'),
    ],
    '#required' => TRUE,
  ];

  $form['buffer'] = [
    '#type' => module_exists('elements') ? 'numberfield' : 'textfield',
    '#title' => t('Buffer'),
    '#description' => t('Allow the end date to be a soft deadline (i.e. not strictly enforced) by adding a positive number of hours as a buffer. Enter 0 to make it a hard deadline (not recommended).'),
    '#default_value' => $conference_date->buffer / 3600,
    '#size' => 7,
  ];

  $form['uid'] = [
    '#type' => 'value',
    '#value' => $conference_date->uid,
  ];

  $form['changed'] = [
    '#type' => 'value',
    '#value' => empty($conference_date->is_new) ? $conference_date->changed : REQUEST_TIME,
  ];

  field_attach_form('conference_date', $conference_date, $form, $form_state);

  $submit = [];
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = [
    '#weight' => 100,
  ];

  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save date'),
    '#validate' => ['conference_date_form_submit_validate'],
    '#submit' => $submit + ['conference_date_form_submit'],
  ];

  // Show Delete button if we edit conference date.
  $conference_date_id = entity_id('conference_date', $conference_date);
  if (!empty($conference_date_id) && conference_date_access('delete', $conference_date)) {
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => ['conference_date_form_delete_submit'],
    ];
  }

  return $form;
}

/**
 * Save-button validation handler for conference_date_form().
 */
function conference_date_form_submit_validate($form, &$form_state) {
  // Dirty trick to make sure date values are sorted as year-month-day
  krsort($form_state['values']['from_date']);
  krsort($form_state['values']['to_date']);

  // Just for validation
  $from = strtotime(implode('-', $form_state['values']['from_date']) . ' 00:00:00');
  $to = strtotime(implode('-', $form_state['values']['to_date']) . ' 23:59:59');

  if ($to <= $from) {
    form_set_error('to_date', t('The end date should come after the start date'));
  }

  $form_state['values']['buffer'] = $form_state['values']['buffer'] * 3600;
}

/**
 * Conference Date submit handler.
 */
function conference_date_form_submit($form, &$form_state) {
  $conference_date = $form_state['conference_date'];

  entity_form_submit_build_entity('conference_date', $conference_date, $form, $form_state);
  conference_date_save($conference_date);

  $conference_date_uri = entity_uri('conference_date', $conference_date);
  $form_state['redirect'] = $conference_date_uri['path'];

  drupal_set_message(t('Conference Date %title saved.', ['%title' => entity_label('conference_date', $conference_date)]));
}

function conference_date_form_delete_submit($form, &$form_state) {
  $destination = [];
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $conference_date = $form_state['conference_date'];
  $conference_date_uri = entity_uri('conference_date', $conference_date);
  $form_state['redirect'] = [$conference_date_uri['path'] . '/delete', ['query' => $destination]];
}

/**
 * Delete confirmation form.
 */
function conference_date_delete_form($form, &$form_state, $conference_date) {
  $form_state['conference_date'] = $conference_date;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['conference_date_type_id'] = ['#type' => 'value', '#value' => entity_id('conference_date', $conference_date)];
  $conference_date_uri = entity_uri('conference_date', $conference_date);
  return confirm_form($form,
    t('Are you sure you want to delete conference date %title?', ['%title' => entity_label('conference_date', $conference_date)]),
    $conference_date_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function conference_date_delete_form_submit($form, &$form_state) {
  $conference_date = $form_state['conference_date'];
  conference_date_delete($conference_date->date_id);

  drupal_set_message(t('Conference Date %title deleted.', ['%title' => entity_label('conference_date', $conference_date)]));

  $form_state['redirect'] = '<front>';
}