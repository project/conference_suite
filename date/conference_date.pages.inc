<?php
/**
 * @file
 * Conference Date page callbacks.
 */
/**
 * Conference Date view callback.
 */
function conference_date_view($conference_date) {
  return entity_view('conference_date', [entity_id('conference_date', $conference_date) => $conference_date], 'full');
}
