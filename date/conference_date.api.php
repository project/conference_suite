<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on conference_date being loaded from the database.
 *
 * This hook is invoked during $conference_date loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of $conference_date entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_conference_date_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a $conference_date is inserted.
 *
 * This hook is invoked after the $conference_date is inserted into the database.
 *
 * @param ConferenceDate $conference_date
 *   The $conference_date that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_conference_date_insert(ConferenceDate $conference_date) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('conference_date', $conference_date),
      'extra' => print_r($conference_date, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a $conference_date being inserted or updated.
 *
 * This hook is invoked before the $conference_date is saved to the database.
 *
 * @param ConferenceDate $conference_date
 *   The $conference_date that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_conference_date_presave(ConferenceDate $conference_date) {
  $conference_date->name = 'foo';
}

/**
 * Responds to a $conference_date being updated.
 *
 * This hook is invoked after the $conference_date has been updated in the database.
 *
 * @param ConferenceDate $conference_date
 *   The $conference_date that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_conference_date_update(ConferenceDate $conference_date) {
  db_update('mytable')
    ->fields(array('extra' => print_r($conference_date, TRUE)))
    ->condition('id', entity_id('conference_date', $conference_date))
    ->execute();
}

/**
 * Responds to $conference_date deletion.
 *
 * This hook is invoked after the $conference_date has been removed from the database.
 *
 * @param ConferenceDate $conference_date
 *   The $conference_date that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_conference_date_delete(ConferenceDate $conference_date) {
  db_delete('mytable')
    ->condition('pid', entity_id('conference_date', $conference_date))
    ->execute();
}

/**
 * Act on a conference_date that is being assembled before rendering.
 *
 * @param $conference_date
 *   The conference_date entity.
 * @param $view_mode
 *   The view mode the conference_date is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to $conference_date->content prior to rendering. The
 * structure of $conference_date->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_conference_date_view($conference_date, $view_mode, $langcode) {
  $conference_date->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for conference_dates.
 *
 * @param $build
 *   A renderable array representing the conference_date content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * conference_date content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the conference_date rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_conference_date().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_conference_date_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Acts on conference_date_type being loaded from the database.
 *
 * This hook is invoked during conference_date_type loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of conference_date_type entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_conference_date_type_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a conference_date_type is inserted.
 *
 * This hook is invoked after the conference_date_type is inserted into the database.
 *
 * @param ConferenceDateType $conference_date_type
 *   The conference_date_type that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_conference_date_type_insert(ConferenceDateType $conference_date_type) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('conference_date_type', $conference_date_type),
      'extra' => print_r($conference_date_type, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a conference_date_type being inserted or updated.
 *
 * This hook is invoked before the conference_date_type is saved to the database.
 *
 * @param ConferenceDateType $conference_date_type
 *   The conference_date_type that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_conference_date_type_presave(ConferenceDateType $conference_date_type) {
  $conference_date_type->name = 'foo';
}

/**
 * Responds to a conference_date_type being updated.
 *
 * This hook is invoked after the conference_date_type has been updated in the database.
 *
 * @param ConferenceDateType $conference_date_type
 *   The conference_date_type that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_conference_date_type_update(ConferenceDateType $conference_date_type) {
  db_update('mytable')
    ->fields(array('extra' => print_r($conference_date_type, TRUE)))
    ->condition('id', entity_id('conference_date_type', $conference_date_type))
    ->execute();
}

/**
 * Responds to conference_date_type deletion.
 *
 * This hook is invoked after the conference_date_type has been removed from the database.
 *
 * @param BoookingType $conference_date_type
 *   The conference_date_type that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_conference_date_type_delete(ConferenceDateType $conference_date_type) {
  db_delete('mytable')
    ->condition('pid', entity_id('conference_date_type', $conference_date_type))
    ->execute();
}

/**
 * Define default conference_date_type configurations.
 *
 * @return
 *   An array of default conference_date_type, keyed by machine names.
 *
 * @see hook_default_conference_date_type_alter()
 */
function hook_default_conference_date_type() {
  $defaults['main'] = entity_create('conference_date_type', array(
    // …
  ));
  return $defaults;
}

/**
 * Alter default conference_date_type configurations.
 *
 * @param array $defaults
 *   An array of default conference_date_type, keyed by machine names.
 *
 * @see hook_default_conference_date_type()
 */
function hook_default_conference_date_type_alter(array &$defaults) {
  $defaults['main']->name = 'custom name';
}

/**
 * Control access to a conference date.
 * Modules may implement this hook if they want to have a say in whether or not
 * a given user has access to perform a given operation on a conference date.
 */
function hook_conference_date_access($conference_date, $op, $account) {
  return CONFERENCE_DATE_ALLOW;
}
