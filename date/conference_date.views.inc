<?php
/**
 * @file
 * Customizes views for Conference module.
 */

/**
 * Implements hook_views_pre_build().
 *
 * Sets the default value of a conference property filter to the
 * current conference.
 */
function conference_date_views_pre_build(&$view) {
  if ( !empty($view->filter['conference']) ) {
    // Sets the default value for the conference filter
    $view->filter['conference']->value = [Conference::current()];
  }
}

/**
 * Implements hook_views_data_alter().
 */
function conference_date_views_data_alter(&$data) {
  foreach ($data as $table => $config) {
    if (preg_match("/conference_date$/", $table)) {
      foreach (['from_date', 'to_date'] as $item) {
        if (!empty($config[$item])) {
          if (in_array($config[$item]['field']['handler'], ['views_handler_field_date'])) {
            $data[$table][$item]['field']['handler'] = 'conference_date_' . $config[$item]['field']['handler'];
          }
        }
      }
    }
  }
}
