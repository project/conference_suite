<?php
/**
 * @file
 * ConferenceDate entity controller.
 */
class ConferenceDateController extends EntityAPIController {
  public function create(array $values = []) {
    global $user;
    $values += [
      'from_date' => REQUEST_TIME,
      'to_date' => REQUEST_TIME,
      'buffer' => 43200,
      'conference' => Conference::current(),
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    ];
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = []) {
    $content['#entity'] = $entity;
    $content['#bundle'] = $entity->type;
    $content += parent::buildContent($entity, $view_mode, $langcode, $content);

    $content['backlink'] = [
      '#markup' => '<p>' . l(t('Go to conference timeline.'), 'admin/conference/timeline') . '</p>'
    ];

    $content['status'] = [
      '#markup' => '<p>' . conference_date_status_options_list()[$entity->status] . '</p>'
    ];

    return $content;
  }
}

class ConferenceDateTypeController extends EntityAPIControllerExportable {
  public function create(array $values = []) {
    $values += [
      'label' => '',
      'description' => '',
    ];
    return parent::create($values);
  }

  /**
   * Save Conference Date Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * UI controller for Conference Date Type.
 */
class ConferenceDateTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Conference Date Types.';
    return $items;
  }
}

/**
 * ConferenceDate class.
 */
class ConferenceDate extends Entity {
  /**
   * {@inheritdoc}
   */
  public function __construct(array $values = [], $entityType = NULL) {
    parent::__construct($values, $entityType);
    $this->hard_deadline = $this->to_date + $this->buffer;
    $this->status = $this->status();
  }

  protected function defaultLabel() {
    $bundle = conference_date_type_load($this->bundle());
    $conference = entity_metadata_wrapper('node', $this->wrapper()->conference->value());
    try {
      $year = $conference->conference_year->value();
      return "$year {$bundle->label()}";
    }
    catch (EntityMetadataWrapperException $e) {
      return $bundle->label();
    }
  }

  protected function defaultUri() {
    return ['path' => 'admin/conference/timeline/' . $this->identifier()];
  }

  public function status() {
    if (REQUEST_TIME < $this->from_date) {
      return CONFERENCE_DATE_FUTURE;
    }

    if (REQUEST_TIME < $this->to_date) {
      return CONFERENCE_DATE_CURRENT;
    }

    if (REQUEST_TIME < $this->hard_deadline) {
      return CONFERENCE_DATE_SOFT_PAST;
    }

    if (REQUEST_TIME > $this->hard_deadline) {
      return CONFERENCE_DATE_HARD_PAST;
    }
  }

  public function save() {
    if (is_array($this->from_date)) {
      $timezone = new DateTimeZone(conference_date_timezone($this->conference));

      $from_date = (new DateTime('now', $timezone))
        ->setDate($this->from_date['year'], $this->from_date['month'], $this->from_date['day'])
        ->setTime('00', '00', '00');
      $this->from_date = $from_date->getTimestamp();

      $to_date = (new DateTime('now', $timezone))
        ->setDate($this->to_date['year'], $this->to_date['month'], $this->to_date['day'])
        ->setTime('23', '59', '59');
      $this->to_date = $to_date->getTimestamp();
    }
    $this->changed = REQUEST_TIME;
    // Ensure date information is properly reloaded into the conference node.
    cache_clear_all($this->conference, 'cache_entity_node');

    return parent::save();
  }
}

/**
 * ConferenceDateType class.
 */
class ConferenceDateType extends Entity {
  public $type;
  public $label;
  public $weight = 0;

  public function __construct($values = []) {
    parent::__construct($values, 'conference_date_type');
  }

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
