<?php
/**
 * @file
 * Conference Date install functions.
 */
/**
 * Helper function that returns a schema field definition for UUID fields.
 */
function conference_date_uuid_schema_field_definition() {
  return [
    'type' => 'char',
    'length' => 36,
    'not null' => TRUE,
    'default' => '',
    'description' => 'The Universally Unique Identifier.',
  ];
}

/**
 * Implements hook_schema().
 */
function conference_date_schema() {
  $schema = [];

  $schema['conference_date'] = [
    'description' => 'The base table for conference dates.',
    'fields' => [
      'date_id' => [
        'description' => 'The primary identifier for the conference date.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'type' => [
        'description' => 'The type (bundle) of the conference date.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
      ],
      'uid' => [
        'description' => 'The ID of the account that created the conference date.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'from_date' => [
        'description' => 'The Unix timestamp of the conference date’s start date.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'to_date' => [
        'description' => 'The Unix timestamp of the conference date’s end date.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'buffer' => [
        'description' => 'The number of seconds of the buffer period.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'conference' => [
        'description' => 'The node ID of the conference to which this date belongs.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'created' => [
        'description' => 'The Unix timestamp of when the conference date was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'changed' => [
        'description' => 'The Unix timestamp of when the conference date was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'uuid' => conference_date_uuid_schema_field_definition(),
    ],
    'indexes' => [
      'uuid' => ['uuid'],
    ],
    'primary key' => ['date_id'],
  ];

  $schema['conference_date_type'] = [
    'description' => 'Stores information about all defined date types.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique date type ID.',
      ],
      'type' => [
        'description' => 'The machine-readable name of this type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ],
      'label' => [
        'description' => 'The human-readable name of this type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'description' => [
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ],
      'uuid' => conference_date_uuid_schema_field_definition(),
    ] + entity_exportable_schema_fields(),
    'indexes' => [
      'uuid' => ['uuid'],
    ],
    'primary key' => ['id'],
    'unique keys' => [
      'type' => ['type'],
    ],
  ];

  return $schema;
}

/**
 * Update all conference dates to use correct time zone.
 */
function conference_date_update_7001(&$sandbox) {
  $dates = (new EntityFieldQuery)
    ->entityCondition('entity_type', 'conference_date')
    ->execute();

  if (!empty($dates)) {
    $dates = conference_date_load_multiple(array_keys($dates['conference_date']));
    $keys = ['year', 'month', 'day'];

    foreach ($dates as $date) {
      $date->from_date = array_combine($keys, explode('-', format_date($date->from_date, 'custom', 'Y-m-d')));
      $date->to_date = array_combine($keys, explode('-', format_date($date->to_date, 'custom', 'Y-m-d')));
      conference_date_save($date);
    }
  }
}
