<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on conference_booking being loaded from the database.
 *
 * This hook is invoked during $conference_booking loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of $conference_booking entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_conference_booking_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a $conference_booking is inserted.
 *
 * This hook is invoked after the $conference_booking is inserted into the database.
 *
 * @param Booking $conference_booking
 *   The $conference_booking that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_conference_booking_insert(Booking $conference_booking) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('conference_booking', $conference_booking),
      'extra' => print_r($conference_booking, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a $conference_booking being inserted or updated.
 *
 * This hook is invoked before the $conference_booking is saved to the database.
 *
 * @param Booking $conference_booking
 *   The $conference_booking that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_conference_booking_presave(Booking $conference_booking) {
  $conference_booking->name = 'foo';
}

/**
 * Responds to a $conference_booking being updated.
 *
 * This hook is invoked after the $conference_booking has been updated in the database.
 *
 * @param Booking $conference_booking
 *   The $conference_booking that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_conference_booking_update(Booking $conference_booking) {
  db_update('mytable')
    ->fields(array('extra' => print_r($conference_booking, TRUE)))
    ->condition('id', entity_id('conference_booking', $conference_booking))
    ->execute();
}

/**
 * Responds to $conference_booking deletion.
 *
 * This hook is invoked after the $conference_booking has been removed from the database.
 *
 * @param Booking $conference_booking
 *   The $conference_booking that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_conference_booking_delete(Booking $conference_booking) {
  db_delete('mytable')
    ->condition('pid', entity_id('conference_booking', $conference_booking))
    ->execute();
}

/**
 * Act on a conference_booking that is being assembled before rendering.
 *
 * @param $conference_booking
 *   The conference_booking entity.
 * @param $view_mode
 *   The view mode the conference_booking is rendered in.
 * @param $langcode
 *   The language code used for rendering.
 *
 * The module may add elements to $conference_booking->content prior to rendering. The
 * structure of $conference_booking->content is a renderable array as expected by
 * drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_conference_booking_view($conference_booking, $view_mode, $langcode) {
  $conference_booking->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for conference_bookings.
 *
 * @param $build
 *   A renderable array representing the conference_booking content.
 *
 * This hook is called after the content has been assembled in a structured
 * array and may be used for doing processing which requires that the complete
 * conference_booking content structure has been built.
 *
 * If the module wishes to act on the rendered HTML of the conference_booking rather than
 * the structured content array, it may use this hook to add a #post_render
 * callback. Alternatively, it could also implement hook_preprocess_conference_booking().
 * See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_conference_booking_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Acts on conference_booking_type being loaded from the database.
 *
 * This hook is invoked during conference_booking_type loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of conference_booking_type entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_conference_booking_type_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a conference_booking_type is inserted.
 *
 * This hook is invoked after the conference_booking_type is inserted into the database.
 *
 * @param BookingType $conference_booking_type
 *   The conference_booking_type that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_conference_booking_type_insert(BookingType $conference_booking_type) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('conference_booking_type', $conference_booking_type),
      'extra' => print_r($conference_booking_type, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a conference_booking_type being inserted or updated.
 *
 * This hook is invoked before the conference_booking_type is saved to the database.
 *
 * @param BookingType $conference_booking_type
 *   The conference_booking_type that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_conference_booking_type_presave(BookingType $conference_booking_type) {
  $conference_booking_type->name = 'foo';
}

/**
 * Responds to a conference_booking_type being updated.
 *
 * This hook is invoked after the conference_booking_type has been updated in the database.
 *
 * @param BookingType $conference_booking_type
 *   The conference_booking_type that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_conference_booking_type_update(BookingType $conference_booking_type) {
  db_update('mytable')
    ->fields(array('extra' => print_r($conference_booking_type, TRUE)))
    ->condition('id', entity_id('conference_booking_type', $conference_booking_type))
    ->execute();
}

/**
 * Responds to conference_booking_type deletion.
 *
 * This hook is invoked after the conference_booking_type has been removed from the database.
 *
 * @param BoookingType $conference_booking_type
 *   The conference_booking_type that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_conference_booking_type_delete(BookingType $conference_booking_type) {
  db_delete('mytable')
    ->condition('pid', entity_id('conference_booking_type', $conference_booking_type))
    ->execute();
}

/**
 * Define default conference_booking_type configurations.
 *
 * @return
 *   An array of default conference_booking_type, keyed by machine names.
 *
 * @see hook_default_conference_booking_type_alter()
 */
function hook_default_conference_booking_type() {
  $defaults['main'] = entity_create('conference_booking_type', array(
    // …
  ));
  return $defaults;
}

/**
 * Alter default conference_booking_type configurations.
 *
 * @param array $defaults
 *   An array of default conference_booking_type, keyed by machine names.
 *
 * @see hook_default_conference_booking_type()
 */
function hook_default_conference_booking_type_alter(array &$defaults) {
  $defaults['main']->name = 'custom name';
}

/**
 * Allows modules to set access restrictions to the checkout page.
 */
function hook_conference_booking_checkout_access($account) {
  // If certain conditions are met:
  return CHECKOUT_DENY;
}

/**
 * Control access to a booking.
 * Modules may implement this hook if they want to have a say in whether or not
 * a given user has access to perform a given operation on a booking.
 */
function hook_conference_booking_access($booking, $op, $account) {
  return CONFERENCE_BOOKING_ALLOW;
}
