<?php
/**
 * @file
 * conference_booking.products.inc
 */

function conference_booking_products_export_form() {
  drupal_set_title(t('Export products'));

  $products = (new EntityFieldQuery)
    ->entityCondition('entity_type', 'commerce_product')
    ->propertyCondition('status', 1);
    ->execute();

  if (!empty($products['commerce_product'])) {
    $export = drupal_json_encode(entity_load('commerce_product', array_keys($products['commerce_product'])));

    $form['export'] = [
      '#type' => 'textarea',
      '#rows' => 20,
      '#title' => t('Exported products'),
      '#default_value' => $export,
    ];
  }
  else {
    $form = [];
  }

  return $form;
}

function conference_booking_products_import_form() {
  drupal_set_title(t('Import products'));

  $form['import'] = [
    '#type' => 'textarea',
    '#rows' => 20,
    '#title' => t('Paste products in JSON format'),
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Import products'),
  ];

  return $form;
}

function conference_booking_products_import_form_submit($form, &$form_state) {
  $products = drupal_json_decode($form_state['values']['import']);

  foreach ( $products as $product ) {
    if (empty($product['uuid'])) {
      $product['uuid'] = uuid_generate();
    }
    entity_uuid_save('commerce_product', (object) $product);
  }
}