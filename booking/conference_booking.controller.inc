<?php
/**
 * @file
 * Booking entity controller.
 */
class BookingController extends EntityAPIController {

  public function create(array $values = []) {
    global $user;
    $values += [
      'title' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    ];
    return parent::create($values);
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = []) {
    $content['checkin'] = [
      '#markup' => theme('conference_booking_content', [
        'booking' => $entity,
        'owner' => user_load($entity->uid),
        'checkin_date' => $entity->checkin,
        'checkin_officer' => user_load($entity->checkin_uid),
      ])
    ];

    $content['backlink'] = [
      '#markup' => '<p>' . l(t('Go to conference registrations.'), 'admin/conference/registrations') . '</p>'
    ];

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

  public function cancel($ids, $account = NULL) {
    $bookings = $ids ? $this->load($ids) : FALSE;
    if (!$bookings) {
      // Do nothing, in case invalid or no ids have been passed.
      return;
    }

    $account = empty($account) ? $GLOBALS['user'] : $account;

    foreach ( $bookings as $booking ) {
      $booking->canceled = time();
      $booking->cancel_uid = $account->uid;
      $this->save($booking);
      $this->invoke('cancel', $booking);
    }
  }

  public function uncancel($ids) {
    $bookings = $ids ? $this->load($ids) : FALSE;
    if (!$bookings) {
      // Do nothing, in case invalid or no ids have been passed.
      return;
    }

    foreach ( $bookings as $booking ) {
      $booking->canceled = 0;
      $booking->cancel_uid = 0;
      $this->save($booking);
      $this->invoke('uncancel', $booking);
    }
  }
}

class BookingTypeController extends EntityAPIControllerExportable {
  public function create(array $values = []) {
    $values += [
      'label' => '',
      'description' => '',
    ];
    return parent::create($values);
  }

  /**
   * Save Booking Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * UI controller for Booking Type.
 */
class BookingTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Booking types.';
    return $items;
  }
}

/**
 * Booking class.
 */
class Booking extends Entity {
  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return ['path' => 'admin/conference/booking/' . $this->identifier()];
  }

  public function cancel($account = NULL) {
    return entity_get_controller($this->entityType)->cancel([$this->booking_id], $account);
  }

  public function uncancel($account = NULL) {
    return entity_get_controller($this->entityType)->uncancel([$this->booking_id], $account);
  }
}

/**
 * Booking Type class.
 */
class BookingType extends Entity {
  public $type;
  public $label;
  public $weight = 0;

  public function __construct($values = []) {
    parent::__construct($values, 'conference_booking_type');
  }

  function isLocked() {
    return isset($this->status) && empty($this->is_new) && (($this->status & ENTITY_IN_CODE) || ($this->status & ENTITY_FIXED));
  }
}
