<?php
/**
 * @file
 * Rules support.
 */
/**
 * Implements hook_rules_condition_info().
 */
function conference_booking_rules_condition_info() {
  return [
    'conference_booking_rules_is_user_registered_condition' => [
      'label' => t('User is registered'),
      'parameter' => [
        'user' => [
          'label' => t('User'),
          'type' => 'user',
        ],
      ],
      'group' => t('Conference'),
    ],
  ];
}

/**
 * Rules condition callback: is user registered?
 *
 * @todo Move to conference_registration.
 */
function conference_booking_rules_is_user_registered_condition($account) {
  $registered = is_user_registered($account);
  return !empty($registered);
}

/**
 * Implements hook_rules_action_info().
 */
function conference_booking_rules_action_info() {
  return [
    'conference_booking_rules_user_register_conference_action' => [
      'label' => t('Register user to current conference'),
      'group' => t('Conference Booking'),
      'parameter' => [
        'user' => [
          'type' => 'user',
          'label' => t('User'),
        ],
      ],
    ],
    'conference_booking_rules_save_reservations_action' => [
      'label' => t('Save reservations for order’s owner'),
      'group' => t('Conference Booking'),
      'parameter' => [
        'commerce_order' => [
          'type' => 'commerce_order',
          'label' => t('Commerce order'),
        ],
      ],
    ],
  ];
}

function conference_booking_rules_user_register_conference_action($account) {
  actions_do('conference_booking_user_register_conference_action', $account);
}

/**
 * Saves (dinner) reservations for a certain user, if corresponding order exists.
 *
 * @deprecated
 */
function conference_booking_rules_save_reservations_action($commerce_order) {

}
