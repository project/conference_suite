<?php
/**
 * @file
 * Allows merging of checkin data.
 */

/**
 * Implements hook_usermerge_actions_supported().
 */
function conference_booking_usermerge_actions_supported() {
  return [
    'conference_booking' => t('Reassign check-ins in conference bookings.'),
  ];
}

/**
 * Implements hook_usermerge_build_review_form_elements().
 */
function conference_booking_usermerge_build_review_form_elements($review, $account_properties, $user_to_delete, $user_to_keep) {
  $user_to_delete_checkin_count = count(_conference_booking_usermerge_get_checkins($user_to_delete));
  $user_to_keep_checkin_count = count(_conference_booking_usermerge_get_checkins($user_to_keep));

  if ( $user_to_delete_checkin_count <> 0 || $user_to_keep_checkin_count <> 0) {
    $review['conference_booking'] = [
      '#tree' => TRUE,
      '#theme' => 'usermerge_data_review_form_table',
      '#title' => t('Conference booking check-ins'),
      '#attributes' => ['no_merge', 'property_label' => t('Entity')],
    ];

    $review['conference_booking']['conference_booking']['property_name'] = [
      '#type' => 'markup',
      '#markup' => 'conference_booking',
    ];

    if ( $user_to_delete_checkin_count <> 0 ) {
      $review['conference_booking']['conference_booking']['options']['user_to_delete'] = [
        '#type' => 'markup',
        '#markup' => format_plural($user_to_delete_checkin_count, '1 %entity entity to be reassigned', '@count %entity entities to be reassigned', ['%entity' => 'conference_booking']),
      ];
    }

    if ( $user_to_keep_checkin_count <> 0 ) {
      $review['conference_booking']['conference_booking']['options']['user_to_keep'] = [
        '#type' => 'markup',
        '#markup' => format_plural($user_to_keep_checkin_count, '1 %entity entity to be maintained', '@count %entity entities to be maintained', ['%entity' => 'conference_booking']),
      ];
    }

    return $review;
  }
}

/**
 * Implements hook_usermerge_merge_accounts().
 */
function conference_booking_usermerge_merge_accounts($user_to_delete, $user_to_keep, $review) {
  $user_to_delete_checkins = _conference_booking_usermerge_get_checkins($user_to_delete);

  if (!empty($user_to_delete_checkins)) {
    $query = db_update('conference_booking')
      ->fields(['checkin_uid' => $user_to_keep->uid])
      ->condition('checkin_uid', $user_to_delete->uid)
      ->execute();
  }
}

/**
 * Finds which conference_booking entities have been checked in by a user.
 */
function _conference_booking_usermerge_get_checkins($account) {
  $checkins = (new EntityFieldQuery)
    ->entityCondition('entity_type', 'conference_booking')
    ->propertyCondition('checkin_uid', $account->uid)
    ->execute();

  return !empty($checkins) ? reset($checkins) : [];
}