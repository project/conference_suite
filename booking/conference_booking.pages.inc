<?php
/**
 * @file
 * Conference Booking page callbacks.
 */
/**
 * Booking view callback.
 */
function conference_booking_view($booking) {
  return entity_view('conference_booking', [entity_id('conference_booking', $booking) => $booking], 'full');
}
