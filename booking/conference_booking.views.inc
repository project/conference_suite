<?php
/**
 * @file
 * Customizes views for Conference Booking.
 */

/**
 * Implements hook_views_data_alter().
 */
function conference_booking_views_data_alter(&$data) {
  $data['views_entity_user']['conference_registration']['field']['handler'] = 'conference_booking_entity_views_handler_field_conference_registration';
  $data['views_entity_user']['conference_registration']['filter'] = [
    'handler' => 'conference_booking_entity_views_handler_filter_conference_registration',
    'label' => t('Registered'),
    'type' => 'yes-no',
    'use equal' => TRUE,
  ];

  $data['views_entity_commerce_line_item']['conference_booking_recipient'] = [
    'title' => t('Booking recipient'),
    'help' => t('The user account for which the booking is being made.'),
    'field' => [
      'handler' => 'conference_booking_entity_views_handler_field_conference_booking_recipient',
      'type' => 'user',
      'real field' => 'entity object',
    ],
  ];

  $conference_booking_data = &$data['conference_booking'];

  foreach ($conference_booking_data as &$definition) {
    if ($filter = &$definition['filter'] ?? []) {
      if ($filter['handler'] == 'views_handler_filter_date') {
        $filter['handler'] = 'date_views_filter_handler_simple';
      }
    }
  }
}

/**
 * Extends template_preprocess_views_view_field().
 */
function conference_booking_preprocess_views_view_field(&$vars) {
  $field = $vars['field'];
  $view = $vars['view'];
  $row = $vars['row'];
  $output = $vars['output'];

  switch ($field->field_alias) {
    case 'conference_booking_checkin' :
      switch ($view->current_display) {
        case 'user' :
          // Show ticket link on users' conferences page
          if ($row->node_field_data_conference_reference__field_data_conference_ == Conference::current()) {
            $account = user_load(arg(1));
            $conference = node_load($row->node_field_data_conference_reference__field_data_conference_);
            $conference_year = Conference::year($conference);

            if ($row->conference_booking_checkin == 0) {
              $output = t('Conference ticket');
            }

            $output = l($output, 'user/' . arg(1) . '/conferences/' . $conference_year);
          }
        break;
      }
    break;
  }

  $vars['output'] = $output;
}
