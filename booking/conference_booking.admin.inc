<?php
/**
 * @file
 * Provides the administrative interface for conference_booking entities.
 *
 * @todo Change menu paths.
 */

/**
 * Generates the booking type editing form.
 */
function conference_booking_type_form($form, &$form_state, $booking_type, $op = 'edit') {

  if ($op == 'clone') {
    $booking_type->label .= ' (cloned)';
    $booking_type->type = '';
  }

  $form['label'] = [
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $booking_type->label,
    '#description' => t('The human-readable name of this booking type.'),
    '#required' => TRUE,
    '#size' => 30,
  ];

  // Machine-readable type name.
  $form['type'] = [
    '#type' => 'machine_name',
    '#default_value' => isset($booking_type->type) ? $booking_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $booking_type->isLocked() && $op != 'clone',
    '#machine_name' => [
      'exists' => 'conference_booking_types',
      'source' => ['label'],
    ],
    '#description' => t('A unique machine-readable name for this booking type. It must only contain lowercase letters, numbers, and underscores.'),
  ];

  $form['description'] = [
    '#type' => 'textarea',
    '#default_value' => isset($booking_type->description) ? $booking_type->description : '',
    '#description' => t('Description of the booking type.'),
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save booking type'),
    '#weight' => 40,
  ];

  if (!$booking_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete booking type'),
      '#weight' => 45,
      '#limit_validation_errors' => [],
      '#submit' => ['conference_booking_type_form_submit_delete'],
    ];
  }
  return $form;
}

/**
 * Submit handler for creating/editing booking_type.
 */
function conference_booking_type_form_submit(&$form, &$form_state) {
  $booking_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  conference_booking_type_save($booking_type);

  // Redirect user back to list of booking types.
  $form_state['redirect'] = 'admin/structure/conference-booking';
}

function conference_booking_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/conference-booking/' . $form_state['conference_booking_type']->type . '/delete';
}

/**
 * Booking type delete form.
 */
function conference_booking_type_form_delete_confirm($form, &$form_state, $booking_type) {
  $form_state['booking_type'] = $booking_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['booking_type_id'] = ['#type' => 'value', '#value' => entity_id('conference_booking_type', $booking_type)];
  return confirm_form($form,
    t('Are you sure you want to delete booking type %title?', ['%title' => entity_label('booking_type', $booking_type)]),
    'booking/' . entity_id('conference_booking_type', $booking_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Booking type delete form submit handler.
 */
function conference_booking_type_form_delete_confirm_submit($form, &$form_state) {
  $booking_type = $form_state['booking_type'];
  conference_booking_type_delete($booking_type);

  watchdog('conference_booking_type', '@type: deleted %title.', ['@type' => $booking_type->type, '%title' => $booking_type->label]);
  drupal_set_message(t('@type %title has been deleted.', ['@type' => $booking_type->type, '%title' => $booking_type->label]));

  $form_state['redirect'] = 'admin/structure/conference-booking';
}

/**
 * Page to select booking Type to add new booking.
 */
function conference_booking_admin_add_page() {
  $items = [];
  foreach (conference_booking_types() as $booking_type_key => $booking_type) {
    $items[] = l(entity_label('conference_booking_type', $booking_type), 'booking/add/' . $booking_type_key);
  }
  return ['list' => ['#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of booking to create.')]];
}

/**
 * Add new booking page callback.
 */
function conference_booking_add($type) {
  $booking_type = conference_booking_types($type);

  $booking = entity_create('conference_booking', ['type' => $type]);
  drupal_set_title(t('Create @name', ['@name' => entity_label('conference_booking_type', $booking_type)]));

  $output = drupal_get_form('conference_booking_form', $booking);

  return $output;
}

/**
 * Booking Form.
 */
function conference_booking_form($form, &$form_state, $booking) {
  $form_state['booking'] = $booking;

  $form['title'] = [
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $booking->title,
  ];

  $form['uid'] = [
    '#type' => 'value',
    '#value' => $booking->uid,
  ];

  field_attach_form('conference_booking', $booking, $form, $form_state);

  $submit = [];
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = [
    '#weight' => 100,
  ];

  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save booking'),
    '#submit' => $submit + ['conference_booking_form_submit'],
  ];

  // Show Delete button if we edit booking.
  $booking_id = entity_id('conference_booking', $booking);
  if (!empty($booking_id) && conference_booking_access('edit', $booking)) {
    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => ['conference_booking_form_submit_delete'],
    ];
  }

  $form['#validate'][] = 'conference_booking_form_validate';

  return $form;
}

function conference_booking_form_validate($form, &$form_state) {

}

/**
 * Booking submit handler.
 */
function conference_booking_form_submit($form, &$form_state) {
  $booking = $form_state['booking'];

  entity_form_submit_build_entity('conference_booking', $booking, $form, $form_state);
  conference_booking_save($booking);

  $booking_uri = entity_uri('conference_booking', $booking);
  $form_state['redirect'] = $booking_uri['path'];

  drupal_set_message(t('Booking %title saved.', ['%title' => entity_label('conference_booking', $booking)]));
}

function conference_booking_form_submit_delete($form, &$form_state) {
  $booking = $form_state['booking'];
  $booking_uri = entity_uri('conference_booking', $booking);
  $form_state['redirect'] = $booking_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function conference_booking_delete_form($form, &$form_state, $booking) {
  $form_state['booking'] = $booking;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['booking_type_id'] = ['#type' => 'value', '#value' => entity_id('conference_booking', $booking)];
  $booking_uri = entity_uri('conference_booking', $booking);
  return confirm_form($form,
    t('Are you sure you want to delete booking %title?', ['%title' => entity_label('conference_booking', $booking)]),
    $booking_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function conference_booking_delete_form_submit($form, &$form_state) {
  $booking = $form_state['booking'];
  conference_booking_delete($booking);

  drupal_set_message(t('Booking %title deleted.', ['%title' => entity_label('conference_booking', $booking)]));

  $form_state['redirect'] = '<front>';
}