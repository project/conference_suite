<?php
/**
 * @file
 * Provides check-in functionality.
 *
 * @todo Adapt to new entity structure.
 */

/**
 * Displays a user's conference registration (ticket), including a QR code.
 *
 * @todo This depends on booking type, which of course is admin-defined. Change accordingly.
 * @todo Replace Google-based QR-code generation with something else.
 */
function conference_booking_registration_view($uid, $conference_year) {
  $account = user_load($uid);

  // Fetch conference year from URL and retrieve the relevant conference ID
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'conference')
    ->fieldCondition('conference_year', 'value', $conference_year);

  $result = $query->execute();

  if (count($result)) {
    $conference_ids = array_keys($result['node']);
    // The query should return only one result, hence the reset()
    $conference_id = reset($conference_ids);

    // Is the user registered? Find registrations.
    $user_registrations = (new EntityFieldQuery)
      ->entityCondition('entity_type', 'conference_booking')
      ->entityCondition('bundle', 'registration')
      ->propertyCondition('uid', $account->uid)
      ->fieldCondition('conference_reference', 'target_id', $conference_id)
      ->execute();

    if (!empty($user_registrations)) {
      $user_registrations = entity_load('conference_booking', array_keys($user_registrations['conference_booking']));
      $registration = reset($user_registrations);

      $output = '<p>' . t('!person is registered for the !conference_year conference.', array('!person' => format_username($account), '!conference_year' => $conference_year)) . '</p>';

      // Is check-in available? Check current conference
      // Don't show ticket after event
      $wconference = entity_metadata_wrapper('node', $conference_id);
      $conference_end = $wconference->conference_dates->value()['value2'] + 86400;

      if ($conference_id == Conference::current() && REQUEST_TIME <= $conference_end) {
      // Check-in is available
        // Person is already checked in
        if ($registration->checkin <> 0) {
          $output = t('!person is registered for the !conference_year conference and has already checked in.', array('!person' => format_username($account), '!conference_year' => $conference_year));
        }
        else {
          $booking_id = $registration->booking_id;

          // Show QR code
          $output .=  '<p>' . t('You can show this code, either printed or on screen, at conference check-in, to facilitate the check-in process:') . '</p>';
          $check_in_url = url('user/' . $uid . '/registration/' . $booking_id . '/' . _conference_booking_checkin_hash($account, $booking_id), array('absolute' => TRUE));

        $output .= "<p class=\"qr-code\"><img src=\"https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=" . urlencode(_conference_booking_checkin_url($account, $booking_id)) . "\" /></p>";
        }
      }
    }
    else {
      $output = '<p>' . t('!person is not registered for the !conference_year conference.', array('!person' => format_username($account), '!conference_year' => $conference_year)) . '</p>';
    }
  }

  if (!empty($output)) {
    $output .= '<p class="no-print">' . l(t('Go back to !person’s conference registrations', array('!person' => format_username($account))), 'user/' . $uid . '/conferences') . '</p>';
    return $output;
  }
}

/**
 * Displays the registration check-in form.
 */
function conference_booking_checkin($form, &$form_state) {
  $uid = $form_state['build_info']['args'][0];
  $wbooking = entity_metadata_wrapper('conference_booking', $form_state['build_info']['args'][1]);
  $hash = !empty($form_state['build_info']['args'][2]) ? $form_state['build_info']['args'][2] : 'checkin';

  // Miscellaneous variables
  if (isset($_GET['destination'])) {
    $cancel_url = $_GET['destination'];
  }
  else {
    $cancel_url = 'user/' . $uid . '/conferences';
  }
  $nothing_to_show = l(t('Go back.'), $cancel_url);

  if (empty($wbooking->booking_id->value())) {
    drupal_set_message(t('The booking you requested does not exist.'), 'error');
    $output = '<p>' . $nothing_to_show . '</p>';
  }
  else {
    $account = user_load($uid);

    if ($wbooking->uid->raw() != $account->uid) {
      drupal_set_message(t('The booking you requested doesn’t belong to this person.'), 'error');
      $output = $nothing_to_show;
    }
    else {
      if ($hash != _conference_booking_checkin_hash($account, $wbooking->booking_id->value())) {
        drupal_set_message(t('The check-in link provided is invalid.'), 'error');
        $output = $nothing_to_show;
      }
      else {
        // See if user has already checked in
        if ($wbooking->checkin->value() <> 0) {
          $output = '<p>' . t('!person has already checked in. !back_link', array('!person' => theme('username', array('account' => $account)), '!back_link' => l(t('Go back to conference registrations.'), $cancel_url))) . '</p>';
        }
        else {
          $conference = $wbooking->conference_reference->raw();

          $output = '<p>' . t('Do you want to check in !person to the !conference_year conference?', array('!person' => theme('username', array('account' => $account)), '!conference_year' => Conference::year($conference))) . '</p>';
          $submit = TRUE;
        }
      }
    }
  }

  if (!empty($output)) {
    $form['output'] = array(
      '#type' => 'markup',
      '#markup' => $output
    );
  }

  if (!empty($submit)) {
    $booking_data = [
      'uid' => $account->uid,
      'booking_id' => $wbooking->booking_id->value(),
    ];

    $form['booking_data'] = [
      '#type' => 'hidden',
      '#value' => serialize($booking_data),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Check in'),
    ];

    $form['cancel'] = [
      '#type' => 'markup',
      '#markup' => "<p class=\"no-print\">or</p>\n<p class=\"no-print\">" . l(t('Cancel'), $cancel_url) . '</p>',
    ];
  }

  return $form;
}

/**
 * Submits the registration check-in form.
 */
function conference_booking_checkin_submit($form, &$form_state) {
  $booking_data = unserialize($form_state['values']['booking_data']);
  $wbooking = entity_metadata_wrapper('conference_booking', $booking_data['booking_id']);
  $account = $wbooking->uid->value();

  if ($wbooking->checkin->value() == 0) {
    $wbooking->checkin = REQUEST_TIME;
    $wbooking->checkin_uid = $GLOBALS['user']->uid;
    $wbooking->save();
    drupal_set_message(t('%person was checked in successfully.', ['%person' => format_username($account)]));
  }
}

/**
 * Displays the check-in cancellation form.
 */
function conference_booking_checkin_cancel($form, &$form_state) {
  $wbooking = entity_metadata_wrapper('conference_booking', $form_state['build_info']['args'][0]);

  $placeholders = [
    '!person' => theme('username', ['account' => $wbooking->uid->value()]),
    '!year' => Conference::year($wbooking->conference_reference->raw()),
  ];

  if (isset($_GET['destination'])) {
    $cancel_url = $_GET['destination'];
  }
  else {
    $cancel_url = 'admin/conference/registrations';
  }

  if ($wbooking->checkin->value() <> 0) {
    $markup = '<p>' . t('Do you really want to cancel !person’s check-in for the !year conference?', $placeholders) . '</p>';
    $submit = t('Yes, cancel this check-in');
  }
  else {
    $markup = '<p>' . t('There is nothing to cancel. !person is not checked in for the !year conference.', $placeholders) . '</p>';
  }

  if (!empty($markup)) {
    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => $markup,
    ];
  }

  if (!empty($submit)) {
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $submit,
    ];

    $form['booking_id'] = [
      '#type' => 'hidden',
      '#value' => $wbooking->booking_id->value(),
    ];

    $form['cancel'] = [
      '#type' => 'markup',
      '#markup' => "<p class=\"no-print\">or</p>\n<p class=\"no-print\">" . l(t('Go back without doing anything'), $cancel_url) . '</p>'
    ];
  }

  return $form;
}

/**
 * Submits the check-in cancellation form.
 */
function conference_booking_checkin_cancel_submit($form, &$form_state) {
  $wbooking = entity_metadata_wrapper('conference_booking', $form_state['values']['booking_id']);
  $wbooking->checkin = 0;
  $wbooking->checkin_uid = $GLOBALS['user']->uid;
  $wbooking->save();
  drupal_set_message(t('This check-in was canceled successfully.'));
}
