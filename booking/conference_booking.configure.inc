<?php
/**
 * @file
 * Defines conference-booking variables.
 */

/**
 * Implements hook_configure_variable_info().
 */
function conference_booking_configure_variable_info() {
  $variables = [
    'conference_booking_withdraw_notify_person' => [
      'group' => 'withdraw_email',
      'type' => 'group',
      'title' => t('Notification of withdrawal to person'),
      'items' => [
        'subject' => [
          'title' => t('Subject of notification to withdrawn person'),
        ],
        'body' => [
          'title' => t('Body of notification to withdrawn person'),
        ],
      ],
    ],
    'conference_booking_withdraw_notify_chair' => [
      'group' => 'withdraw_email',
      'type' => 'group',
      'title' => t('Notification of withdrawal to area chair'),
      'items' => [
        'subject' => [
          'title' => t('Subject of withdrawal notification to area chair'),
        ],
        'body' => [
          'title' => t('Body of withdrawal notification to area chair'),
        ],
      ],
    ],
  ];

  foreach ($variables as $name => $variable) {
    if (in_array($variable['group'], ['withdraw_email'])) {
      if (!isset($variables[$name]['element']['#type'])) {
        $variables[$name]['element']['#type'] = 'textfield';
      }
    }
    if (isset($variable['items'])) {
      foreach ($variable['items'] as $item_name => $item) {
        switch ($item_name) {
          case 'body' :
            $variables[$name]['items'][$item_name]['element'] = [
              '#type' => 'textarea',
              '#rows' => 10,
            ];
          break;
        }
      }
    }
  }

  return $variables;
}

/**
 * Implements hook_configure_variable_info_alter().
 *
 * Adds blocks and views to conference dashboard.
 */
function conference_booking_configure_variable_info_alter(&$variables) {
  $variables['conference_dashboard_setup']['items'] += [
    // Admin blocks
    'registrations',
    'reservations',
    'speakers_unregistered',
    'session_chairs_unregistered',
    // Area blocks
    'speakers_area_registered',
    'speakers_area_unregistered',
    'session_chairs_area_registered',
    'session_chairs_area_unregistered',
  ];
}

/**
 * Implements hook_configure_variable_defaults().
 */
function conference_booking_configure_variable_defaults() {
  return [
    'conference_booking_withdraw_notify_person' => [
      'subject' => 'You have been withdrawn from the !year conference',
      'body' => "Dear !person,

As requested, you have been withdrawn from the !year [[site:name]][1] Conference. If you believe this to be in error, please let us know by replying to this email.

-- The [site:name] Web Genie


[1]: [site:url]"
    ],
    'conference_booking_withdraw_notify_chair' => [
      'subject' => 'Presentation withdrawn from !area',
      'body' => "Dear !person,

The presentation titled **!title** (presented by !presenters) has been withdrawn from the !area Area of the !year [[site:name]][1] Annual Conference. If you believe this to be in error, please let us know by replying to this email.

-- The [site:name] Web Genie


[1]: [site:url]"
    ],
  ];
}

/**
 * Implements hook_configure_variable_defaults_alter().
 */
function conference_booking_configure_variable_defaults_alter(&$defaults) {
  $defaults['conference_dashboard_setup'] += [
    'registrations' => [
      'name' => 'Conference registrations',
      'display' => 'Registrations',
      'view' => 'conference_registrations:registrations_all',
      'path' => 'admin/conference/registrations',
      'context' => 'admin',
      'section' => 'conference',
      'enabled' => 1
    ],
    'reservations' => [
      'name' => 'Reservations',
      'view' => 'reservations:count',
      'arguments' => 'function:conference_current, dinner',
      'path' => 'admin/conference/reservations',
      'query' => 'type:dinner',
      'context' => 'admin',
      'section' => 'conference',
      'enabled' => 1
    ],
    'speakers_unregistered' => [
      'name' => 'Unregistered speakers',
      'display' => 'Unregistered',
      'view' => 'conference_speakers:counter',
      'arguments' => 'any, function:conference_current, no, approved',
      'path' => 'admin/conference/speakers',
      'query' => 'registered:2, approval:1',
      'context' => 'admin',
      'section' => 'speakers',
      'total' => 0,
      'enabled' => 1
    ],
    'session_chairs_unregistered' => [
      'name' => 'Unregistered session chairs',
      'view' => 'conference_session_chairs:counter',
      'arguments' => 'any, function:conference_current, no',
      'path' => 'admin/conference/chairs',
      'query' => 'registered:2',
      'context' => 'admin',
      'section' => 'sessions',
      'total' => 0,
      'enabled' => 1
    ],
    'speakers_area_registered' => [
      'name' => 'Registered speakers',
      'display' => 'Registered',
      'view' => 'conference_speakers:counter',
      'arguments' => '%area, function:conference_current, yes, approved',
      'path' => 'admin/conference/areas/speakers',
      'query' => 'area:%area, approval:1, registered:1',
      'context' => 'areas',
      'section' => 'speakers',
      'total' => 0,
      'enabled' => 1,
    ],
    'speakers_area_unregistered' => [
      'name' => 'Unregistered speakers',
      'display' => 'Unregistered',
      'view' => 'conference_speakers:counter',
      'arguments' => '%area, function:conference_current, no, approved',
      'path' => 'admin/conference/areas/speakers',
      'query' => 'area:%area, approval:1, registered:2',
      'context' => 'areas',
      'section' => 'speakers',
      'total' => 0,
      'enabled' => 1,
    ],
    'session_chairs_area_registered' => [
      'name' => 'Registered session chairs',
      'view' => 'conference_session_chairs:counter',
      'arguments' => '%area, function:conference_current, yes',
      'path' => 'admin/conference/areas/chairs',
      'query' => 'area:%area, registered:1',
      'context' => 'areas',
      'section' => 'sessions',
      'total' => 0,
      'enabled' => 1,
    ],
    'session_chairs_area_unregistered' => [
      'name' => 'Unregistered session chairs',
      'view' => 'conference_session_chairs:counter',
      'arguments' => '%area, function:conference_current, no',
      'path' => 'admin/conference/areas/chairs',
      'query' => 'area:%area, registered:2',
      'context' => 'areas',
      'section' => 'sessions',
      'total' => 0,
      'enabled' => 1,
    ],
  ];
}