<?php
/**
 * @file
 * Tabs for Conference Booking.
 */

/**
 * Menu callback: presentation-withdraw tab.
 */
function conference_booking_tab_page($node = NULL, $type) {
  switch ($type) {
    case 'withdraw' :
      return drupal_get_form('conference_booking_node_withdraw_confirm_form', $node);
    break;
  }

  return [];
}

/**
 * Form constructor: node-withdrawal confirmation.
 */
function conference_booking_node_withdraw_confirm_form($form, &$form_state, $node) {
  $destination = drupal_get_destination();
  $form['#node'] = $node;
  $w = entity_metadata_wrapper('node', $node);
  // Always provide entity id in the same form key as in the entity edit form.
  $form['nid'] = ['#type' => 'value', '#value' => $w->nid->value()];
  $year = Conference::year($w->conference_reference->raw());
  $title = t('Withdraw %title?', ['%title' => $node->title]);
  drupal_set_title($title); // Otherwise confirm_form() won't override the default
  return confirm_form($form,
    $title,
    $destination['destination'],
    t('Are you sure you want to withdraw %title from the !year conference?', ['%title' => $node->title, '!year' => $year]),
    t('Yes, withdraw'),
    t('No, go back')
  );
}

/**
 * Submit handler for conference_booking_node_withdraw_confirm_form().
 */
function conference_booking_node_withdraw_confirm_form_submit($form, &$form_state) {
  actions_do('conference_booking_node_withdraw_action', $form['#node']);
}
