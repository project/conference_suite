<?php

/**
 * Defines a computed field for a booking recipient on a line item.
 */
class conference_booking_entity_views_handler_field_conference_booking_recipient extends entity_views_handler_field_entity {

  /**
   * {@inheritdoc}
   */
  public function get_value($values, $field = NULL) {
    $value = parent::get_value($values, $field);
    $return = [];

    if ($recipients = $value->data['context']['recipients']) {
      $return = user_load_multiple($recipients);
    }

    return $return;
  }

}
