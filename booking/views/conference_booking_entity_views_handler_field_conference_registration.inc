<?php
/**
 * @file
 * Contains the conference_booking_entity_views_handler_field_conference_registration class.
 */

/**
 * @see entity_views_field_definition()
 * @ingroup views_field_handlers
 *
 * @todo Move to conference_registration.
 */
class conference_booking_entity_views_handler_field_conference_registration extends entity_views_handler_field_boolean {
  /**
   * Overridden to use a metadata wrapper.
   */
  public function get_value($values, $field = NULL) {
    $argument = !empty($this->options['conference_registration_argument']) ? $this->options['conference_registration_argument'] : NULL;

    if ( !empty($argument) ) {
      $argument_value = $this->view->field[$argument]->get_value($values);

      // If it's an array, assume it's an entity reference field
      if ( is_array($argument_value) ) {
        if ( !empty($argument_value[0]['target_id']) ) {
          $argument = $argument_value[0]['target_id'];
        }
        else {
          $argument = NULL;
        }
      }
      else {
        $argument = $argument_value;
      }
    }

    return conference_booking_conference_registration_get($values->{$this->field_alias}, ['argument' => $argument]);
  }

  /**
   * Provide a list of available fields.
   */
  function field_options() {
    $options = array();

    $field_handlers = $this->view->display_handler->get_handlers('field');
    foreach ($field_handlers as $field => $handler) {
      if ($handler->table != 'views') {
        $options[$field] = $handler->ui_name();
      }
    }

    return $options;
  }

  /**
   * Provide options for this handler.
   */
  public function option_definition() {
    $options = parent::option_definition() + EntityFieldHandlerHelper::option_definition($this);
    $options['conference_registration_argument'] = ['default' => FALSE];
    return $options;
  }

  /**
   * Provide a options form for this handler.
   */
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $field_options = $this->field_options();

    $field_options = $this->field_options();

    $form['conference_registration_argument'] = [
      '#title' => t('Argument'),
      '#type' => 'select',
      '#description' => t('Field or contextual filter whose argument to pass to the return function.'),
      '#options' => array_merge([t('- Don’t pass an argument -')], $field_options),
      '#default_value' => $this->options['conference_registration_argument'],
    ];

    EntityFieldHandlerHelper::options_form($this, $form, $form_state);
  }
}
