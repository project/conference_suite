<?php
/**
 * @file
 * Implementations of Conference Email hooks.
 */

/**
 * Implements hook_conference_email_email_keys().
 */
function conference_booking_conference_email_email_keys() {
  return [
    'conference_booking' => [
      '*',
      'withdraw_notify_person',
      'withdraw_notify_chair',
    ],
  ];
}
