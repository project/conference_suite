<?php
/**
 * @file
 * Declares variables for Conference Preferences.
 */

/**
 * Implements hook_configure_variable_info().
 */
function conference_preferences_configure_variable_info() {
  return [
    'conference_preferences_area:area_id' => [
      'type' => 'meta',
      'title' => t('Area preferences'),
      'items' => [],
    ],
  ];
}

/**
 * Implements hook_configure_variable_defaults().
 */
function conference_preferences_configure_variable_defaults() {
  return [
    'conference_preferences_area' => [],
  ];
}
