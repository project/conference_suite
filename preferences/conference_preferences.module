<?php
/**
 * @file
 * Code for the Conference Preferences module.
 */

/**
 * Implements hook_permission().
 */
function conference_preferences_permission() {
  return [
    'access preferences page' => [
      'title' => t('Access preferences page'),
    ],
  ];
}

/**
 * Implements hook_menu().
 */
function conference_preferences_menu() {
  $items['admin/conference/dashboard/preferences'] = [
    'title' => 'Preferences',
    'access arguments' => ['access preferences page'],
    'page callback' => 'conference_preferences_page',
    'file' => 'conference_preferences.pages.inc',
    'type' => MENU_LOCAL_TASK,
  ];

  return $items;
}

/**
 * Implements hook_theme().
 */
function conference_preferences_theme() {
  return [
    'conference_preferences_form' => array(
      'render element' => 'form',
      'file' => 'conference_preferences.pages.inc',
    ),
  ];
}

/**
 * Checks whether a preference for a certain area is set.
 *
 * Declare only if function doesn't exist, to prevent conflict on module install.
 */
if (!function_exists('conference_preferences_check')) {
  function conference_preferences_check($key, $area) {
    $preferences = cvget('conference_preferences_area', ['area_id' => $area]);
    return !empty($preferences[$key]);
  }
}
