<?php
/**
 * @file
 * Preferences page and related functions.
 */

/**
 * Menu callback: preferences page.
 */
function conference_preferences_page() {
  drupal_set_title(t('Area preferences'));

  $data = [];
  $user_areas = Conference::getUserAreas(NULL, TRUE);

  if (!empty($user_areas)) {
    $data['user'] = t('Your areas');
    $data += $user_areas;
  }

  if (user_access('access admin dashboard blocks')) {
    $areas = (new EntityFieldQuery)
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'area')
      ->propertyOrderBy('title')
      ->execute();
    if (!empty($areas['node'])) {
      $areas = node_load_multiple(array_keys($areas['node']));

      if (!empty($data['user'])) {
        $data['admin'] = t('Other areas');
        $areas = array_diff_key($areas, $user_areas);
      }
      $data += $areas;
    }
  }

  return drupal_get_form('conference_preferences_form', $data);
}

/**
 * Form constructor: area preferences.
 *
 * @see theme_conference_preferences_form()
 */
function conference_preferences_form($form, &$form_state, $data) {
  $definition = configure_variable_get_info('conference_preferences_area');
  $options = $definition['items'];
  array_walk($options, function(&$item) {
    $item = $item['title'];
  });

  $form['checkboxes'] = [
    '#tree' => TRUE,
  ];

  foreach ($data as $key => $row) {
    if (is_numeric($key)) {
      $preferences = (array) cvget('conference_preferences_area', ['area_id' => $key]);
      $preferences = array_filter($preferences);
      $preferences = array_combine(array_keys($preferences), array_keys($preferences));

      $form['areas'][$key]['title'] = [
        '#markup' => $row->title,
      ];

      $form['checkboxes'][$key] = [
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => $preferences,
      ];
    }
    else {
      $form['areas'][$key] = [
        '#markup' => $row,
      ];
    }
  }

  $form['definition'] = [
    '#type' => 'value',
    '#value' => $definition,
  ];

  $form['#attached']['css'][] = drupal_get_path('module', 'conference_preferences') . '/conference_preferences.css';

  if (!empty($data)) {
    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save preferences'),
    ];
  }

  return $form;
}

/**
 * Submit handler for conference_preferences_form().
 */
function conference_preferences_form_submit($form, &$form_state) {
  $preferences = $form_state['values']['checkboxes'];
  $default = configure_variable_default('conference_preferences_area');

  foreach ($preferences as $nid => $values) {
    $values = array_filter($values);
    cvset('conference_preferences_area', $values, ['area_id' => $nid]);
  }

  drupal_set_message(t('Your preferences were saved.'));
}

/**
 * Returns HTML for the area preferences’ form.
 *
 * @see conference_preferences_form()
 */
function theme_conference_preferences_form($variables) {
  $form = $variables['form'];

  if (!empty($form['areas'])) {
    foreach (element_children($form['areas']) as $key) {
      if (is_numeric($key)) {
        $rows[$key]['area'] = [
          'data' => drupal_render($form['areas'][$key]),
        ];

        foreach (element_children($form['checkboxes'][$key]) as $pref) {
          $form['checkboxes'][$key][$pref]['#title'] = "{$rows[$key]['area']['data']}: {$form['checkboxes'][$key][$pref]['#title']}";
          // $form['checkboxes'][$key][$pref]['#return_value'] = TRUE;
          $form['checkboxes'][$key][$pref]['#title_display'] = 'invisible';
          $rows[$key][$pref] = [
            'data' => drupal_render($form['checkboxes'][$key][$pref]),
            'class' => ['checkbox'],
            'title' => $form['checkboxes'][$key][$pref]['#title'],
          ];
        }
      }
      else {
        $rows[$key]['area'] = [
          'data' => drupal_render($form['areas'][$key]),
          'colspan' => count($form['definition']['#value']['items']) + 1,
          'class' => ['subheader'],
        ];
      }
    }
  }

  $header['area'] = t('Area');
  $rows = !empty($rows) ? $rows : [];

  foreach($form['definition']['#value']['items'] as $name => $item) {
    $header[$name] = [
      'data' => $item['title'],
      'class' => ['checkbox'],
    ];
  }

  return theme('table', ['header' => $header, 'rows' => $rows, 'sticky' => TRUE, 'empty' => t('You have no areas to manage.')]) . drupal_render_children($form);
}
