<?php
/**
 * @file
 * conference_submissions.views.inc
 */

/**
 * Implements hook_views_bulk_operations_form_alter().
 *
 * Performs an additional access check for the
 * conference_submissions_notify_speaker action.
 *
 * Removes submit button in config form if no valid presentation nodes
 * have been selected.
 *
 * @see conference_submissions_action_access()
 * @see conference_submissions_add_to_session_action_form()
 */
function conference_submissions_views_bulk_operations_form_alter(&$form, &$form_state, $vbo) {
  switch ( $form_state['step'] ) {
    case 'views_form_views_form' :
      if (isset($form['select']['action::conference_submissions_notify_speaker'])) {
        $form['select']['action::conference_submissions_notify_speaker']['#access'] = conference_submissions_action_access('notify_speaker');
      }
      elseif (isset($form['select']['operation']['#options']['action::conference_submissions_notify_speaker'])) {
        if (!conference_submissions_action_access('notify_speaker')) {
          unset($form['select']['operation']['#options']['action::conference_submissions_notify_speaker']);
        }
      }

      if (isset($form['select']['action::conference_submissions_add_to_session_action'])) {
        $form['select']['action::conference_submissions_add_to_session_action']['#access'] = conference_submissions_action_access('add_to_session_action');
      }
      elseif (isset($form['select']['operation']['#options']['action::conference_submissions_add_to_session_action'])) {
        if (!conference_submissions_action_access('add_to_session_action')) {
          unset($form['select']['operation']['#options']['action::conference_submissions_add_to_session_action']);
        }
      }

      $form['select']['#pre_render'] = ['conference_submissions_views_bulk_operations_pre_render'];
    break;

    case 'views_bulk_operations_config_form' :
      switch ($form_state['values']['operation']) {
        case 'action::conference_submissions_add_to_session_action' :
          // Remove submit button if no nodes are available
          if (empty($form_state['values']['views_bulk_operations'])) {
            $query = drupal_get_query_parameters($_GET, ['q']);
            $form['actions']['submit'] = [
              '#markup' => l(t('Go back'), $vbo->view->get_url(), ['query' => $query]),
            ];
          }
          else {
            $form['actions']['submit']['#value'] = t('Confirm order and continue');
          }
        break;
      }
    break;
  }
}

/**
 * Pre-render function for the VBO selector.
 */
function conference_submissions_views_bulk_operations_pre_render($element) {
  if (!empty($element['operation']['#options'])) {
    if (count($element['operation']['#options']) == 1 && isset($element['operation']['#options'][0])) {
      $element = [];
    }
  }
  else {
    foreach ( element_children($element) as $element_child ) {
      if ( isset($element[$element_child]['#access']) && empty($element[$element_child]['#access']) ) {
        unset($element[$element_child]);
      }
    }

    // Empty the selector if it has no children
    if ( !count(element_children($element)) ) {
      $element = [];
    }
  }

  return $element;
}
