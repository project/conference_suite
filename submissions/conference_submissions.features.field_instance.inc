<?php

/**
 * @file
 * Conference_submissions.features.field_instance.inc.
 */

/**
 * Implements hook_field_default_field_instances().
 */
function conference_submissions_field_default_field_instances() {
  $field_instances = [];

  // Exported field_instance: 'node-conference-conference_allow_resubmit'.
  $field_instances['node-conference-conference_allow_resubmit'] = [
    'bundle' => 'conference',
    'default_value' => [
      0 => [
        'value' => 0,
      ],
    ],
    'deleted' => 0,
    'description' => 'Allow resubmitting presentations from the previous year (useful for canceled conferences).',
    'display' => [
      'default' => [
        'label' => 'above',
        'module' => 'list',
        'settings' => [],
        'type' => 'list_default',
        'weight' => 14,
      ],
      'full' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
      'home_page' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
      'teaser' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_allow_resubmit',
    'label' => 'Allow resubmissions',
    'required' => 0,
    'settings' => [
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'options',
      'settings' => [
        'display_label' => 0,
      ],
      'type' => 'options_onoff',
      'weight' => 31,
    ],
  ];

  // Exported field_instance: 'node-presentation-conference_nonpresenters'.
  $field_instances['node-presentation-conference_nonpresenters'] = [
    'bundle' => 'presentation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => [
      'conference_schedule_xml' => [
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => [
          'bypass_access' => 1,
          'link' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 1,
      ],
      'conference_submissions_tabular' => [
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => [
          'bypass_access' => FALSE,
          'link' => FALSE,
        ],
        'type' => 'entityreference_label',
        'weight' => 1,
      ],
      'default' => [
        'label' => 'above',
        'module' => 'conference_submissions',
        'settings' => [
          'skip_access_check' => 1,
          'target_label_link' => 'restricted',
          'view_mode' => 'conference_submissions_tabular',
        ],
        'type' => 'conference_submissions_tabular',
        'weight' => 2,
      ],
      'full' => [
        'label' => 'above',
        'module' => 'conference_submissions',
        'settings' => [
          'skip_access_check' => 0,
          'target_label_link' => 'restricted',
          'view_mode' => 'conference_submissions_tabular',
        ],
        'type' => 'conference_submissions_tabular',
        'weight' => 1,
      ],
      'markdown' => [
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => [
          'bypass_access' => FALSE,
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 3,
      ],
      'printable' => [
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => [
          'links' => TRUE,
          'skip_access_check' => 0,
          'use_content_language' => TRUE,
          'view_mode' => 'full',
        ],
        'type' => 'entityreference_entity_view',
        'weight' => 1,
      ],
      'teaser' => [
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => [
          'links' => 1,
          'skip_access_check' => 1,
          'use_content_language' => TRUE,
          'view_mode' => 'full',
        ],
        'type' => 'entityreference_entity_view',
        'weight' => 1,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_nonpresenters',
    'label' => 'Nonpresenting coauthors',
    'required' => 0,
    'settings' => [
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'entityreference',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ],
      'type' => 'entityreference_autocomplete',
      'weight' => 6,
    ],
  ];

  // Exported field_instance: 'node-presentation-conference_presentation_resubd'.
  $field_instances['node-presentation-conference_presentation_resubd'] = [
    'bundle' => 'presentation',
    'default_value' => [
      0 => [
        'value' => 0,
      ],
    ],
    'deleted' => 0,
    'description' => '',
    'display' => [
      'conference_schedule_xml' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
      'conference_submissions_tabular' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
      'default' => [
        'label' => 'above',
        'module' => 'list',
        'settings' => [],
        'type' => 'list_default',
        'weight' => 10,
      ],
      'full' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
      'teaser' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_presentation_resubd',
    'label' => 'Resubmitted',
    'required' => 0,
    'settings' => [
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'options',
      'settings' => [
        'display_label' => 1,
      ],
      'type' => 'options_onoff',
      'weight' => 31,
    ],
  ];

  // Translatables
  // Included for use with string extractors like potx.
  t('Allow resubmissions');
  t('Allow resubmitting presentations from the previous year (useful for canceled conferences).');
  t('Resubmitted');

  return $field_instances;
}
