<?php
/**
 * @file
 * conference_submissions.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function conference_submissions_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'area_emails';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'users';
  $view->human_name = 'Area emails';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'email chaired areas people';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'semanticviews_default';
  $handler->display->display_options['row_plugin'] = 'semanticviews_fields';
  $handler->display->display_options['row_options']['skip_blank'] = 1;
  $handler->display->display_options['row_options']['semantic_html'] = array(
    'name' => array(
      'element_type' => 'div',
      'class' => '',
    ),
    'mail' => array(
      'element_type' => 'div',
      'class' => '',
    ),
  );
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['id'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['table'] = 'users';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['field'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['label'] = 'Presentation';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['required'] = TRUE;
  /* Relationship: Flags: presentation_approve */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['relationship'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'Approved';
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'presentation_approve';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  /* Field: User: E-mail */
  $handler->display->display_options['fields']['mail']['id'] = 'mail';
  $handler->display->display_options['fields']['mail']['table'] = 'users';
  $handler->display->display_options['fields']['mail']['field'] = 'mail';
  $handler->display->display_options['fields']['mail']['label'] = '';
  $handler->display->display_options['fields']['mail']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['mail']['link_to_user'] = '0';
  /* Contextual filter: Content: Area (conference_area) */
  $handler->display->display_options['arguments']['conference_area_target_id']['id'] = 'conference_area_target_id';
  $handler->display->display_options['arguments']['conference_area_target_id']['table'] = 'field_data_conference_area';
  $handler->display->display_options['arguments']['conference_area_target_id']['field'] = 'conference_area_target_id';
  $handler->display->display_options['arguments']['conference_area_target_id']['relationship'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['arguments']['conference_area_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['conference_area_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['conference_area_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['conference_area_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['conference_area_target_id']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Field: Conference (conference_reference) */
  $handler->display->display_options['arguments']['conference_reference_target_id']['id'] = 'conference_reference_target_id';
  $handler->display->display_options['arguments']['conference_reference_target_id']['table'] = 'field_data_conference_reference';
  $handler->display->display_options['arguments']['conference_reference_target_id']['field'] = 'conference_reference_target_id';
  $handler->display->display_options['arguments']['conference_reference_target_id']['relationship'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['arguments']['conference_reference_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['conference_reference_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['conference_reference_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['conference_reference_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['conference_reference_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Approved presenters */
  $handler = $view->new_display('attachment', 'Approved presenters', 'approved');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';

  /* Display: Pending presenters */
  $handler = $view->new_display('attachment', 'Pending presenters', 'pending');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['id'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['table'] = 'users';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['field'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['label'] = 'Presentation';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['required'] = TRUE;
  /* Relationship: Flags: presentation_approve */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['relationship'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'Approved';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'presentation_approve';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  /* Relationship: Flags: presentation_reject */
  $handler->display->display_options['relationships']['flag_content_rel_1']['id'] = 'flag_content_rel_1';
  $handler->display->display_options['relationships']['flag_content_rel_1']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel_1']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel_1']['relationship'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['flag_content_rel_1']['label'] = 'Rejected';
  $handler->display->display_options['relationships']['flag_content_rel_1']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel_1']['flag'] = 'presentation_reject';
  $handler->display->display_options['relationships']['flag_content_rel_1']['user_scope'] = 'any';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Flags: Flagged */
  $handler->display->display_options['filters']['flagged']['id'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['table'] = 'flagging';
  $handler->display->display_options['filters']['flagged']['field'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['filters']['flagged']['value'] = '0';
  /* Filter criterion: Flags: Flagged */
  $handler->display->display_options['filters']['flagged_1']['id'] = 'flagged_1';
  $handler->display->display_options['filters']['flagged_1']['table'] = 'flagging';
  $handler->display->display_options['filters']['flagged_1']['field'] = 'flagged';
  $handler->display->display_options['filters']['flagged_1']['relationship'] = 'flag_content_rel_1';
  $handler->display->display_options['filters']['flagged_1']['value'] = '0';

  /* Display: Rejected presenters */
  $handler = $view->new_display('attachment', 'Rejected presenters', 'rejected');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['id'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['table'] = 'users';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['field'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['label'] = 'Presentation';
  $handler->display->display_options['relationships']['reverse_conference_presenters_node']['required'] = TRUE;
  /* Relationship: Flags: presentation_reject */
  $handler->display->display_options['relationships']['flag_content_rel_1']['id'] = 'flag_content_rel_1';
  $handler->display->display_options['relationships']['flag_content_rel_1']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel_1']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel_1']['relationship'] = 'reverse_conference_presenters_node';
  $handler->display->display_options['relationships']['flag_content_rel_1']['label'] = 'Rejected';
  $handler->display->display_options['relationships']['flag_content_rel_1']['flag'] = 'presentation_reject';
  $handler->display->display_options['relationships']['flag_content_rel_1']['user_scope'] = 'any';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: User: Active */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'users';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '1';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Session chairs */
  $handler = $view->new_display('attachment', 'Session chairs', 'chairs');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Entity Reference: Referencing entity */
  $handler->display->display_options['relationships']['reverse_conference_session_chairs_node']['id'] = 'reverse_conference_session_chairs_node';
  $handler->display->display_options['relationships']['reverse_conference_session_chairs_node']['table'] = 'users';
  $handler->display->display_options['relationships']['reverse_conference_session_chairs_node']['field'] = 'reverse_conference_session_chairs_node';
  $handler->display->display_options['relationships']['reverse_conference_session_chairs_node']['label'] = 'Session';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Areas (conference_areas) */
  $handler->display->display_options['arguments']['conference_areas_target_id']['id'] = 'conference_areas_target_id';
  $handler->display->display_options['arguments']['conference_areas_target_id']['table'] = 'field_data_conference_areas';
  $handler->display->display_options['arguments']['conference_areas_target_id']['field'] = 'conference_areas_target_id';
  $handler->display->display_options['arguments']['conference_areas_target_id']['relationship'] = 'reverse_conference_session_chairs_node';
  $handler->display->display_options['arguments']['conference_areas_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['conference_areas_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['conference_areas_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['conference_areas_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['conference_areas_target_id']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Field: Conference (conference_reference) */
  $handler->display->display_options['arguments']['conference_reference_target_id']['id'] = 'conference_reference_target_id';
  $handler->display->display_options['arguments']['conference_reference_target_id']['table'] = 'field_data_conference_reference';
  $handler->display->display_options['arguments']['conference_reference_target_id']['field'] = 'conference_reference_target_id';
  $handler->display->display_options['arguments']['conference_reference_target_id']['relationship'] = 'reverse_conference_session_chairs_node';
  $handler->display->display_options['arguments']['conference_reference_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['conference_reference_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['conference_reference_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['conference_reference_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['conference_reference_target_id']['summary_options']['items_per_page'] = '25';
  $export['area_emails'] = $view;

  $view = new view();
  $view->name = 'conference_submissions_node_references';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Conference Submissions node references';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Field: Conference (conference_reference) */
  $handler->display->display_options['arguments']['conference_reference_target_id']['id'] = 'conference_reference_target_id';
  $handler->display->display_options['arguments']['conference_reference_target_id']['table'] = 'field_data_conference_reference';
  $handler->display->display_options['arguments']['conference_reference_target_id']['field'] = 'conference_reference_target_id';
  $handler->display->display_options['arguments']['conference_reference_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['conference_reference_target_id']['default_argument_type'] = 'conference_current_id';
  $handler->display->display_options['arguments']['conference_reference_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['conference_reference_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['conference_reference_target_id']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

  /* Display: Presentation */
  $handler = $view->new_display('entityreference', 'Presentation', 'presentation');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'presentation' => 'presentation',
  );
  $export['conference_submissions_node_references'] = $view;

  $view = new view();
  $view->name = 'conference_submissions_taxonomy_references';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Conference submissions taxonomy references';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'session' => 'session',
  );

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'name' => 'name',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['conference_submissions_taxonomy_references'] = $view;

  return $export;
}
