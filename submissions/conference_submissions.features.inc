<?php

/**
 * @file
 * conference_submissions.features.inc
 */

/**
 * Implements hook_views_api().
 */
function conference_submissions_views_api($module = NULL, $api = NULL) {
  return ["api" => "3.0"];
}
