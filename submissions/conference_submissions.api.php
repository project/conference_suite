<?php
/**
 * @file
 * API for conference_submissions.
 */

/**
 * Allows other modules to save data entered in the account_details
 * step (Step 2) of the proposal form.
 *
 * @deprecated
 */
function hook_conference_submissions_node_form_account_details($account, $data) {

}

/**
 * Allows other modules to save data entered in the account_details
 * step (Step 2) of the proposal form.
 *
 * @deprecated
 */
function hook_conference_submissions_node_form_account_details_submi($account, $data) {

}

/**
 * Allows other modules to declare arrays of presentation types,
 * keyed by machine name, with the following properties:
 *   - translatable name
 *   - quota: how many presentations of this type a user is allowed
 *   - weight
 */
function hook_conference_submissions_presentation_types() {
  return [
    'paper' => [
      'name' => t('Paper'),
      'weight' => -10000,
    ],
  ];
}

/**
 * Allows other modules to alter allowed values for fields
 * provided by conference_submissions.
 */
function hook_conference_submissions_presentation_types_alter(&$types) {

}

/**
 * Allows modules to return a custom node object to use with node forms.
 *
 * @return
 *   A node object (full or stub).
 */
function hook_conference_submissions_node_form_load($key) {

}

/**
 * Allows modules to alter the array of recipients in a notification.
 */
function hook_conference_submissions_notify_recipients_alter(&$accounts, &$variables) {

}
