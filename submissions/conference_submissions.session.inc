<?php
/**
 * @file
 *
 * Functions related to custom session forms.
 */

/**
 * Outputs HTML for draggable presentations in VBO config form.
 *
 * @see conference_submissions_add_to_session_action_form()
 */
function theme_conference_submissions_add_to_session_sort($variables) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form) as $nid) {
    $form[$nid]['weight']['#attributes']['class'] = array('conference-submissions-presentation-order-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form[$nid]['title']),
        // drupal_render($form[$nid]['roles']),
        drupal_render($form[$nid]['weight']),
        // drupal_render($form[$nid]['configure']),
        // drupal_render($form[$nid]['disable']),
      ),
      'class' => array('draggable'),
    );
  }
  $header = array(t('Title'), t('Weight'));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'conference-submissions-presentation-order')));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('conference-submissions-presentation-order', 'order', 'sibling', 'conference-submissions-presentation-order-weight');

  return $output;
}