<?php
/**
 * @file
 * Declares variables for Conference Submissions.
 */

/**
 * Implements hook_configure_variable_info().
 */
function conference_submissions_configure_variable_info() {
  $variables = [
    'conference_submissions_guidelines_node' => [
      'title' => t('Submission guidelines'),
      'group' => 'submission_email', // This is not appropriate, but it will do for now
      'type' => 'reference',
      'type arguments' => ['node', 'help'],
    ],
    'conference_submissions_grace_period' => [
      'group' => 'submission_email',  // This is not appropriate, but it will do for now
      'default' => '24',
      'element' => [
        '#title' => t('Submissions grace period'),
        '#type' => 'numberfield',
        '#description' => t('Number of hours during which a submission can be modified by its author.'),
      ],
    ],
    'conference_submissions_notifications' => [
      'group' => 'submission_email',
      'element' => [
        '#type' => 'select',
        '#title' => t('Email notifications'),
        '#options' => [
          0 => t('No notifications'),
          'immediate' => t('Immediate notifications'),
          'delayed' => t('Delayed notifications'),
        ],
        '#description' => t('Choose whether to enable submission notifications to submitters and area chairs. If you choose “Delayed notifications,” notifications to submitters will still be sent immediately, but notifications to area chairs and presenters will be sent only after the grace period is over.'),
      ],
    ],
    'conference_submissions_notify_submitter_presentation' => [
      'group' => 'submission_email',
      'type' => 'group',
      'title' => t('Notification email to submitter'),
      'items' => [
        'subject' => [
          'title' => t('Subject of notification to submitter'),
        ],
        'body' => [
          'title' => t('Body of notification to submitter'),
        ],
      ],
    ],
    'conference_submissions_notify_chair_presentation' => [
      'group' => 'submission_email',
      'type' => 'group',
      'title' => t('Notification email to area chair'),
      'items' => [
        'subject' => [
          'title' => t('Subject of notification to area chair'),
        ],
        'body' => [
          'title' => t('Body of notification to area chair'),
        ],
      ],
    ],
    'conference_submissions_notify_old_chair_presentation' => [
      'group' => 'submission_email',
      'type' => 'group',
      'title' => t('Notification email to old area chair'),
      'items' => [
        'subject' => [
          'title' => t('Subject of notification to old area chair'),
        ],
        'body' => [
          'title' => t('Body of notification to old area chair'),
        ],
      ],
    ],
    'conference_submissions_notify_person_presentation' => [
      'group' => 'submission_email',
      'type' => 'group',
      'title' => t('Notification email to speaker'),
      'items' => [
        'subject' => [
          'title' => t('Subject of notification to speaker'),
        ],
        'body' => [
          'title' => t('Body of notification to speaker'),
        ],
      ],
    ],
    'conference_submissions_notify_person_session' => [
      'group' => 'submission_email',
      'type' => 'group',
      'title' => t('Notification email to session chair'),
      'items' => [
        'subject' => [
          'title' => t('Subject of notification to session chair'),
        ],
        'body' => [
          'title' => t('Body of notification to session chair'),
        ],
      ],
    ],
    'user_mail_conference_submissions_auto_user_created' => [
      'type' => 'group',
      'title' => t('Notification email to newly created user'),
      'items' => [
        'subject' => [
          'title' => t('Subject of notification to newly created users'),
        ],
        'body' => [
          'title' => t('Body of notification to newly created users'),
        ],
      ],
    ],
    'conference_submissions_notify_person_approval' => [
      'group' => 'approval_email',
      'type' => 'group',
      'title' => t('Notification email to approved speaker'),
      'items' => [
        'subject' => [
          'title' => t('Subject of notification to approved speaker'),
        ],
        'body' => [
          'title' => t('Body of notification to approved speaker'),
        ],
      ],
    ],
    'conference_submissions_notify_person_rejection' => [
      'group' => 'approval_email',
      'type' => 'group',
      'title' => t('Notification email to rejected speaker'),
      'items' => [
        'subject' => [
          'title' => t('Subject of notification to rejected speaker'),
        ],
        'body' => [
          'title' => t('Body of notification to rejected speaker'),
        ],
      ],
    ],
    'conference_submissions_node_form:form_id' => [
      'type' => 'meta',
      'title' => t('Conference Submissions temporary form data'),
    ],
  ];

  foreach ( $variables as $name => $variable ) {
    if ( in_array(@$variable['group'], ['submission_email', 'approval_email']) ) {
      if ( !isset($variables[$name]['element']['#type']) ) {
        $variables[$name]['element']['#type'] = 'textfield';
      }
    }
    if ( isset($variable['items']) ) {
      foreach ( $variable['items'] as $item_name => $item ) {
        switch ( $item_name ) {
          case 'body' :
            $variables[$name]['items'][$item_name]['element'] = [
              '#type' => 'textarea',
              '#rows' => 10,
            ];
          break;
        }
      }
    }
  }

  return $variables;
}

/**
 * Implements hook_configure_variable_info_alter().
 */
function conference_preferences_configure_variable_info_alter(&$variables) {
  $variables['conference_preferences_area']['items'] += [
    'notification_immediate_approval' => [
      'title' => t('Immediate approval notification'),
    ],
    'notification_immediate_rejection' => [
      'title' => t('Immediate rejection notification'),
    ],
  ];
}

/**
 * Implements hook_configure_variable_defaults().
 *
 * @todo Adapt to current variable structure.
 * @todo Generalize text.
 */
function conference_submissions_configure_variable_defaults() {
  return [
    'conference_submissions_grace_period' => 24,
    'conference_submissions_notifications' => 'immediate',
    'conference_submissions_notify_submitter_presentation' => [
      'subject' => 'You have submitted a presentation',
      'body' => "Dear !person,

You have successfully submitted a presentation to the **!area** Area of the !year [[site:name]][1] Annual Conference.

The title of your presentation is:

**!title**

This presentation is waiting for approval. You will receive a response in the coming weeks. You can also check the status of your submission at !url_user.

!salutation

-- The [site:name] Web Genie


[1]: [site:url]"
    ],
    'conference_submissions_notify_chair_presentation' => [
      'subject' => 'New presentation submitted',
      'body' => "Dear !person,

!submitter !submitter_email submitted a new presentation to the **!area** Area of the !year [[site:name]][1] Annual Conference.

The title of the presentation is:

**!title**

Use the [conference dashboard][2] to manage your area and approve presentations.

!salutation

-- The [site:name] Web Genie


[1]: [site:url]
[2]: [site:url]admin/conference/dashboard"
    ],
    'conference_submissions_notify_old_chair_presentation' => [
      'subject' => 'Presentation removed from your area',
      'body' => "Dear !person,

The presentation titled \"!title\" (submitted by !submitter !submitter_email) was removed from the **!area** Area of the !year [[site:name]][1] Annual Conference.

Use the [conference dashboard][2] to manage your area and approve presentations.

!salutation

-- The [site:name] Web Genie


[1]: [site:url]
[2]: [site:url]admin/conference/dashboard"
    ],
    'conference_submissions_notify_person_presentation' => [
      'subject' => 'You have been added as a speaker',
      'body' => "Dear !person,

You have been submitted as a potential speaker to the **!area** Area of the !year [[site:name]][1] Annual Conference.

The title of your presentation is:

**!title**

This presentation is waiting for approval. You will receive a response in the coming weeks. You can also check the status of the submission at !url_user.

!salutation

-- The [site:name] Web Genie


[1]: [site:url]"
    ],
    'conference_submissions_notify_person_session' => [
      'subject' => 'You have been added as a session chair',
      'body' => "Dear !person,

You have been added as a session chair to the **!area** Area of the !year [[site:name]][1] Annual Conference.

The title of the session is:

**!title**

!salutation

-- The [site:name] Web Genie


[1]: [site:url]"
    ],
    'conference_submissions_notify_person_approval' => [
      'subject' => 'Your presentation has been approved',
      'body' => "Dear !person,

Congratulations! Your presentation titled \"!title\" has been approved for inclusion in the **!area** Area of the !year [[site:name]][1] Annual Conference.

In order to be included in the conference program, you must be a [site:name] member for the current year, as well as registered to the !year conference. [Find information on how to register on the [site:name] website.][2]

!salutation

-- The [site:name] Web Genie


[1]: [site:url]
[2]: [site:url]conference"
    ],
    'conference_submissions_notify_person_rejection' => [
      'subject' => 'Your presentation has not been approved',
      'body' => "Dear !person,

Your presentation titled \"!title,\" submitted to the **!area** Area of the !year [[site:name]][1] Annual Conference, was not approved for inclusion in the program.

We hope you'll consider submitting a proposal for next year's conference.

Best wishes,

-- The [site:name] Web Genie


[1]: [site:url]
[2]: [site:url]conference"
    ],
  ];
}

/**
 * Implements hook_configure_variable_defaults().
 */
function conference_preferences_configure_variable_defaults_alter(&$defaults) {
  $defaults['conference_preferences_area'] = [
    'notification_immediate_approval' => FALSE,
    'notification_immediate_rejection' => FALSE,
  ];
}
