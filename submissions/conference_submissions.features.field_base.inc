<?php

/**
 * @file
 * conference_submissions.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function conference_submissions_field_default_field_bases() {
  $field_bases = [];

  // Exported field_base: 'conference_allow_resubmit'.
  $field_bases['conference_allow_resubmit'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'conference_allow_resubmit',
    'indexes' => [
      'value' => [
        0 => 'value',
      ],
    ],
    'locked' => 0,
    'module' => 'list',
    'settings' => [
      'allowed_values' => [
        0 => '',
        1 => '',
      ],
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
    ],
    'translatable' => 0,
    'type' => 'list_boolean',
  ];

  // Exported field_base: 'conference_nonpresenters'.
  $field_bases['conference_nonpresenters'] = [
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'conference_nonpresenters',
    'field_permissions' => [
      'type' => 0,
    ],
    'indexes' => [
      'target_entity' => [
        0 => 'target_id',
      ],
      'target_id' => [
        0 => 'target_id',
      ],
    ],
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => [
      'handler' => 'views',
      'handler_settings' => [
        'behaviors' => [
          'views-select-list' => [
            'status' => 0,
          ],
        ],
        'view' => [
          'args' => [],
          'display_name' => 'authenticated',
          'view_name' => 'conference_user_references',
        ],
      ],
      'handler_submit' => 'Change handler',
      'profile2_private' => FALSE,
      'target_type' => 'user',
    ],
    'translatable' => 0,
    'type' => 'entityreference',
  ];

  // Exported field_base: 'conference_presentation_resubd'.
  $field_bases['conference_presentation_resubd'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'conference_presentation_resubd',
    'field_permissions' => [
      'type' => 0,
    ],
    'indexes' => [
      'value' => [
        0 => 'value',
      ],
    ],
    'locked' => 0,
    'module' => 'list',
    'settings' => [
      'allowed_values' => [
        0 => '',
        1 => '',
      ],
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
    ],
    'translatable' => 0,
    'type' => 'list_boolean',
  ];

  return $field_bases;
}
