<?php
/**
 * @file
 * Implementations of Conference Email hooks.
 */

/**
 * Implements hook_conference_email_email_keys().
 */
function conference_submissions_conference_email_email_keys() {
  return [
    'conference_submissions' => [
      '*',
      'notify_submitter_presentation',
      'notify_chair_presentation',
      'notify_old_chair_presentation',
      'notify_person_presentation',
      'notify_person_session',
      'notify_person_approval',
      'notify_person_rejection',
    ],
  ];
}
