<?php

/**
 * @file
 * Adds conference_forms support to conference_submissions.
 */

/**
 * Implements hook_conference_forms_info().
 */
function conference_submissions_conference_forms_info() {
  $info['submission'] = [
    'base path' => 'conference/submission',
    'title' => 'Submit a proposal',
    'access callback' => 'conference_submissions_access',
    'steps' => [
      // The 'start' step is mandatory
      'start' => [
        'handler' => 'ConferenceSubmissionsStepStart',
        'weight' => 0,
      ],
      'accounts' => [
        'handler' => 'ConferenceSubmissionsStepAccounts',
        'weight' => 1,
      ],
      'create' => [
        'handler' => 'ConferenceSubmissionsStepCreate',
        'page callback' => 'build_page',
        'weight' => 2,
      ],
    ],
  ];

  $info['resubmission'] = [
    'base path' => 'conference/resubmit',
    'title' => 'Resubmit your paper',
    'access callback' => 'conference_submissions_access',
    'steps' => [
      // This is here because a start step is mandatory, and to make sure that
      // even the base path leads to the same place as the submission form.
      'start' => [
        'handler' => 'ConferenceSubmissionsStepStart',
        'weight' => 0,
      ],
      'resubmit' => [
        'handler' => 'ConferenceSubmissionsStepResubmit',
        'weight' => 1,
      ],
    ],
  ];

  return $info;
}
