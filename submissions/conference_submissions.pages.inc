<?php
/**
 * @file
 *
 * Functions related to custom node forms.
 */

/**
 * Menu callback: replaces node_add().
 *
 * @see node_add()
 */
function conference_submissions_node_add($type, $node = NULL) {
  if (!empty($node) && ($node->type != $type)) {
    return MENU_NOT_FOUND;
  }

  if ($type == 'presentation' && !user_access('use simplified presentation form')) {
    drupal_goto('conference/submission');
  }

  $node = $node ?: entity_create('node', [
    'type' => $type,
    'title' => '',
    'uid' => $GLOBALS['user']->uid,
    'name' => $GLOBALS['user']->name,
    'is_new' => TRUE,
    'language' => LANGUAGE_NONE,
  ]);

  module_load_include('inc', 'node', 'node.pages');
  return drupal_get_form($type . '_node_form', $node);
}
