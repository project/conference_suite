<?php

/**
 * Class ConferenceSubmissionsStepResubmit
 */
class ConferenceSubmissionsStepResubmit extends ConferenceSubmissionsStepBase {

  /**
   * {@inheritdoc}
   */
  public function build_form($form, &$form_state) {
    return confirm_form(
      [],
      t('Resubmit !year presentation', ['!year' => Conference::year(Conference::previous())]),
      'conference/submission',
      t('Do you want to resubmit %title?', ['%title' => $this->data['node']->label()]),
      t('Yes, resubmit presentation'),
      t('No, submit new presentation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submit_form($form, &$form_state) {
    try {
      $clone = entity_metadata_wrapper('node', entity_create('node', [
        'type' => 'presentation',
        'language' => $this->data['node']->get('language')->value(),
        'uid' => $this->user->uid,
      ]));

      foreach (array_keys($this->data['node']->getPropertyInfo()) as $property_name) {
        if (in_array($property_name, [
          'url',
          'created',
          'conference_reference',
          'author',
        ])) {
          continue;
        }

        try {
          $clone->get($property_name)->set($this->data['node']->get($property_name)->value());
        } catch (\EntityMetadataWrapperException $e) {
          // Most likely: property doesn't support writing (e.g. nid, vid).
        }
      }

      $clone->get('created')->set(REQUEST_TIME);
      $clone->get('conference_reference')->set(Conference::current());
      $clone->get('conference_presentation_resubd')->set(TRUE);
      // Replace original node with clone.
      $this->data['node'] = $clone;
      $form_state['submission']['presenters']['existing'] = $this->data['node']->get('conference_presenters')->value();
    }
    catch (EntityMetadataWrapperException $e) {
      drupal_goto('conference/submission');
    }

    if ($this->data['node']->save()) {
      $title = $this->data['node']->label();
      drupal_set_message(t('Your presentation %title from the !year conference was successfully resubmitted.', ['%title' => $title, '!year' => Conference::year(Conference::previous())]), 'status', TRUE);
      $form_state['redirect'] = $this->data['node']->get('url')->value();
    }
  }

}
