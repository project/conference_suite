<?php

/**
 * Class ConferenceSubmissionsStepAccounts
 */
class ConferenceSubmissionsStepAccounts extends ConferenceSubmissionsStepBase {

  /**
   * {@inheritdoc}
   */
  public function build_form($form, &$form_state) {
    $subtitle = t('Presenters’ personal details');
    $form['subtitle'] = [
      '#markup' => "<h3>$subtitle</h3>",
    ];

    $form_state['submission'] = $this->data['submission'] ?? [];

    if (!empty($form_state['submission']['presenters']['existing'])) {
      $form['existing_accounts'] = [
        '#type' => 'container',
        '#tree' => TRUE,
      ];

      foreach ($form_state['submission']['presenters']['existing'] as $uid => $existing) {
        // #access is FALSE so this can be used as a placeholder.
        $form['existing_accounts'][$uid] = [
          '#type' => 'fieldset',
          '#title' => t('Details for !presenter', ['!presenter' => format_username($existing)]),
          '#access' => FALSE,
        ];
      }
    }

    if (!empty($form_state['submission']['presenters']['new'])) {
      $form['new_accounts'] = [
        '#type' => 'container',
        '#tree' => TRUE,
      ];

      foreach ($form_state['submission']['presenters']['new'] as $uid => $new) {
        // This hook is deprecated.
        $elements = module_invoke_all('conference_submissions_node_form_account_details');
        // #access is FALSE so this can be used as a placeholder.
        $form['new_accounts'][$uid] = [
          '#type' => 'fieldset',
          '#title' => t('Details for !email', ['!email' => $new->mail]),
          '#access' => !empty($elements),
        ];

        foreach ($elements as $name => $element) {
          $form['new_accounts'][$uid][$name] = $element;
        }
      }
    }

    return parent::build_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submit_form($form, &$form_state) {
    $new = $form_state['submission']['presenters']['new'] ?? [];
    $existing = $form_state['submission']['presenters']['existing'] ?? [];

    foreach ($form_state['values']['new_accounts'] ?? [] as $uid => $data) {
      $account = $form_state['submission']['presenters']['new'][$uid];
      module_invoke_all('conference_submissions_node_form_account_details_submit', $account, $data);
    }

    $form_state['submission']['presenters']['existing'] = $existing + $new;

    parent::submit_form($form, $form_state);
  }

}