<?php

/**
 * Class ConferenceSubmissionsStepStart
 */
class ConferenceSubmissionsStepStart extends ConferenceSubmissionsStepBase {

  /**
   * {@inheritdoc}
   */
  public function build_form($form, &$form_state) {
    // Check existing paper in current conference.
    $existing_presentation = _user_has_nodes($this->user, ['presenter'], 'paper');

    if (!$existing_presentation && conference_submissions_resubmission_enabled() && $previous = conference_submissions_previous_node($this->user)){
      $year = Conference::year(Conference::previous());

      $form['resubmit'] = [
        '#type' => 'fieldset',
        '#title' => t('Resubmit !year presentation', ['!year' => $year]),
        '#description' => '<p>' . t(
          'You have an approved presentation titled !title from the !year conference that you can resubmit. <strong>Would you like to resubmit it?</strong> Since it has already been approved, you will not be able to change it. To submit a new presentation, skip to the following section.',
          ['!title' => l($previous->label(), $previous->get('url')->value(), ['attributes' => ['target' => '_blank']]), '!year' => $year]
          ) . '</p>',
        '#tree' => TRUE,
      ];

      $form['resubmit']['node'] = [
        '#type' => 'value',
        '#value' => $previous,
      ];

      $form['resubmit']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Resubmit !year paper', ['!year' => $year]),
        '#name' => 'resubmit-submit',
      ];

      // The title of the following section.
      $subtitle = t('Submit new presentation');
    }
    else {
      $subtitle = t('Basic information');
    }

    $form['subtitle'] = [
      '#markup' => "<h3>$subtitle</h3>",
    ];

    $types = conference_submissions_presentation_types(TRUE);

    if (count($types) > 1) {
      $options = conference_submissions_field_allowed_values([
        'field_name' => 'conference_presentation_type',
        'use_in_form' => TRUE,
      ]);

      $default_value = conference_submissions_field_default_value(NULL, NULL, ['field_name' => 'conference_presentation_type'], NULL);
      $default_value = reset($default_value)['value'];

      $form['presentation_type'] = [
        '#type' => 'select',
        '#title' => t('Presentation type'),
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => $default_value ?? '',
      ];
    }
    else {
      $types = array_keys($types);
      $form['presentation_type'] = [
        '#type' => 'value',
        '#value' => reset($types),
      ];
    }

    $wrapper_id = drupal_html_id('presenter-email-add-more-wrapper');

    $form['presenter_email'] = [
      '#type' => 'item',
      '#title' => t('Authors’ email addresses'),
      '#tree' => TRUE,
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ];

    $form['presenter_email']['help'] = [
      '#markup' => t('Enter email addresses for all the authors in your proposal.'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];

    if (user_access('create users on submission')) {
      $form['presenter_email']['help']['#markup'] .= ' ' . t('If we can’t match them with existing accounts, we’ll ask you to provide their personal information and we’ll create new accounts for them.');
    }
    else {
      $form['presenter_email']['help']['#markup'] .= ' ' . t('All authors must have already created accounts on the website.');
    }

    $items_count = empty($form_state['field_state']['presenter_email']['items_count']) ? 1 : $form_state['field_state']['presenter_email']['items_count'];

    for ($i = 1; $i <= $items_count; $i++) {
      $element = [
        '#type' => 'item',
        '#tree' => TRUE,
        '#title' => $items_count > 1 ? t('Author @delta', ['@delta' => $i]) : NULL,
      ];

      $element['presenter_email'] = [
        '#field_name' => 'presenter_email',
        '#type' => 'emailfield',
      ];

      $element['nonpresenting_coauthor'] = [
        '#type' => $i != 1 ? 'checkbox' : 'value',
        '#title' => t('Nonpresenting coauthor'),
      ];

      $form['presenter_email']['elements'][] = $element;
    }

    if ($items_count == 1) {
      $account = $GLOBALS['user'];
      $form['presenter_email']['elements'][0]['#default_value'] = !$existing_presentation ? $account->mail : '';

      // If only one presentation type is set, check quota immediately.
      if (count($types) == 1) {
        $type = reset($types);
        if ($count = $this->is_over_quota($type, $account)) {
          drupal_set_message(t('You already have !presentation_count in the system. You can only add other people to this presentation.', ['!presentation_count' => format_plural($count, 'one presentation', '@count presentations')]), 'warning');
          $form['presenter_email']['elements'][0]['#default_value'] = '';
        }
      }
    }

    $form['presenter_email']['add_more'] = [
      '#type' => 'submit',
      '#value' => t('Add another author’s email'),
      '#submit' => [[get_class(), 'add_more_submit']],
      '#validate' => [],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [get_class(), 'add_more'],
        'wrapper' => $wrapper_id,
        'effect' => 'fade',
      ],
    ];

    $form_state['field_state']['presenter_email']['items_count'] = count(element_children($form['presenter_email']['elements']));

    return parent::build_form($form, $form_state);
  }

  /**
   * Ajax callback in response to a new empty widget being added to the form.
   *
   * This returns the new page content to replace the page content made obsolete
   * by the form submission.
   *
   * @see conference_submissions_add_more_submit()
   * @see field_add_more_js()
   */
  public static function add_more($form, $form_state) {
    $button = $form_state['triggering_element'];
    // Go one level up in the form, to the widgets container.
    $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));
    // Add a DIV around the delta receiving the Ajax effect.
    $delta = count(element_children($element['elements']));
    $element[$delta]['#prefix'] = '<div class="ajax-new-content">' . (isset($element[$delta]['#prefix']) ? $element[$delta]['#prefix'] : '');
    $element[$delta]['#suffix'] = (isset($element[$delta]['#suffix']) ? $element[$delta]['#suffix'] : '') . '</div>';

    return $element;
  }

  /**
   * Submit handler for the "Add another item" button of a field form.
   *
   * This handler is run regardless of whether JS is enabled or not. It makes
   * changes to the form state. If the button was clicked with JS disabled, then
   * the page is reloaded with the complete rebuilt form. If the button was
   * clicked with JS enabled, then ajax_form_callback() calls ::add_more() to
   * return just the changed part of the form.
   *
   * @see field_add_more_submit()
   */
  public static function add_more_submit($form, &$form_state) {
    $button = $form_state['triggering_element'];
    // Go one level up in the form, to the widgets container.
    $element = drupal_array_get_nested_value($form, array_slice($button['#array_parents'], 0, -1));
    // Increment the items count.
    $field_state = &$form_state['field_state'][reset($element['#array_parents'])];
    $field_state['items_count']++;
    $form_state['rebuild'] = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function validate_form($form, &$form_state) {
    // Clean up form values.
    $state = $form_state;
    form_state_values_clean($state);
    $form_state['submission'] = $state['values'];
    $form_state['submission']['presenters'] = [];
    $presenters = $form_state['submission']['presenter_email']['elements'];
    $emails = array_filter(array_column($presenters, 'presenter_email'));
    $nonpresenting = array_filter(array_column($presenters, 'nonpresenting_coauthor'));
    $type = $form_state['submission']['presentation_type'];
    $type_name = conference_submissions_presentation_types()[$type]['name'];

    if (empty($emails)) {
      form_set_error("presenter_email][elements][0", t('You must enter at least one email address to proceed.'));
    }
    else {
      $new = FALSE;
      $error = FALSE;

      foreach ($emails as $delta => $email) {
        $user = $this->get_user($email, $new, $error);

        if ($error) {
          $errors[] = $email;
          form_set_error("presenter_email][elements][$delta");
        }

        if ($new) {
          $form_state['submission']['presenters']['new'][$user->uid] = $user;
        }
        else {
          if (!isset($nonpresenting[$delta]) && ($count = $this->is_over_quota($type, $user))) {
            form_set_error("presenter_email][elements][$delta", t('!user already has !presentation_count of type %type_name in the system and cannot be added to a new one.', [
              '!user' => format_username($user) . " ($email)",
              '!presentation_count' => format_plural($count, 'one presentation', '@count presentations'),
              '%type_name' => $type_name,
            ]));
          }
          else {
            if (!empty($user)) {
              $form_state['submission']['presenters']['existing'][$user->uid] = $user;
            }
          }
        }
      }

      if ($nonpresenting) {
        $form_state['submission']['nonpresenting'] = array_intersect_key($emails, $nonpresenting);
      }

      if (!empty($errors)) {
        $emails = implode(', ', $errors);
        $singular = 'We couldn’t find an account linked to the address %email. Make sure you entered it correctly.';
        $plural = 'We couldn’t find any accounts linked to these addresses: %email. Make sure you entered them correctly.';
        $reminder = t('Each author must create their own account before you can add them to a proposal.');
        drupal_set_message(format_plural(count($errors), $singular, $plural, ['%email' => $emails]) . ' ' . $reminder, 'error');
      }
    }

    unset($form_state['submission']['presenter_email']);
  }

  /**
   * {@inheritdoc}
   */
  public function submit_form($form, &$form_state) {
    if ($form_state['triggering_element']['#name'] == 'resubmit-submit') {
      /** @var \EntityDrupalWrapper $node */
      if ($node = entity_metadata_wrapper('node', $form_state['values']['resubmit']['node'])) {
        $this->data['node'] = $node;
        $this->data['resubmit'] = TRUE;
      }
    }
    elseif (empty($form_state['submission']['presenters']['new'])) {
      // Skip a step.
      $this->next_step = ConferenceForms::getNextStep($this->form_info['type'], $this->next_step);
    }

    $this->data['submission'] = $form_state['submission'];

    parent::submit_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function goto_next($form, &$form_state) {
    if (!empty($this->data['resubmit'])) {
      return "conference/resubmit/{$this->data['data_id']}/resubmit";
    }

    return parent::goto_next($form, $form_state);
  }

  /**
   * Finds user given its e-mail address. If no user is found, one is created.
   * The $edit array must be provided to build a new user.
   *
   * @param $email
   * @param bool $new
   * @param bool $error
   *
   * @return mixed|object|null
   */
  protected function get_user($email, &$new = FALSE, &$error = FALSE) {
    // If multiple_email is enabled, use the relevant table, or fall back to the user table
    if (module_exists('multiple_email')) {
      $exists = multiple_email_find_address($email);

      if (!empty($exists)) {
        $user = user_load($exists->uid);
      }
    }
    else {
      $user = user_load_by_mail($email);
    }

    if (empty($user)) {
      if (user_access('create users on submission')) {
        if ($user = $this->create_user($email)) {
          $new = TRUE;
          return $user;
        }
      }

      $error = TRUE;
      return NULL;
    }

    $new = FALSE;
    $error = FALSE;

    return $user;
  }

  /**
   * Check whether a user is over quota.
   * The restriction applies only if the user is a presenter, not the node's
   * author.
   *
   * @param $type
   * @param null $account
   *
   * @return bool|int
   */
  protected function is_over_quota($type, $account = NULL) {
    $account = $account ?? $GLOBALS['user'];
    if (!user_access('override node quota', $account)) {
      if ($user_nodes = _user_has_nodes($account, 'presenter', $type)) {
        $types = conference_submissions_presentation_types();
        if ($types[$type]['quota'] == CONFERENCE_SUBMISSIONS_PRESENTATIONS_UNLIMITED) {
          return FALSE;
        }

        if (!empty($types[$type]) && count($user_nodes) >= $types[$type]['quota']) {
          return count($user_nodes);
        }
      }
    }

    return FALSE;
  }

  /**
   * Creates a user and returns the $user object.
   *
   * @param $email
   *
   * @return object|null
   *
   * @todo Make sure username generation works.
   */
  protected function create_user($email) {
    $user = (object) [
      // This is to trigger email_registration's name creation.
      'name' => 'email_registration_' . user_password(),
      'mail' => $email,
      'created' => REQUEST_TIME,
      'access' => 0,
      'login' => 0,
      'status' => 1,
      'language' => NULL,
      'init' => $email,
    ];

    try {
      user_save($user, ['pass' => user_password()]);

      try {
        _user_mail_notify('conference_submissions_auto_user_created', $user);
      }
      catch (\Exception $e) {
        // Nothing.
      }

      return $user;
    }
    catch (\Exception $e) {
      // Nothing.
    }

    return NULL;
  }

}