<?php

/**
 * Class ConferenceSubmissionsStepBase
 */
abstract class ConferenceSubmissionsStepBase extends ConferenceFormsStepBase {

  /**
   * ConferenceSubmissionsStepBase constructor.
   *
   * @param array $data
   * @param string $step
   * @param string $next_step
   * @param array $form_info
   */
  public function __construct(array $data = [], string $step = '', string $next_step = '', array $form_info = []) {
    parent::__construct($data, $step, $next_step, $form_info);

    if (empty($this->data['node'])) {
      $node = entity_create('node', [
        'type' => 'presentation',
        'title' => '',
        'uid' => $GLOBALS['user']->uid,
        'name' => $GLOBALS['user']->name,
        'is_new' => TRUE,
        'language' => LANGUAGE_NONE,
      ]);
      /** @var \EntityMetadataWrapper $wnode */
      $wnode = entity_metadata_wrapper('node', $node);
      $this->data['node'] = $wnode;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build_form($form, &$form_state) {
    // Add a notice at the top of the presentation form.
    if (user_access('override node quota')) {
      $guidelines = cvget('conference_submissions_guidelines_node');

      if (!empty($guidelines)) {
        drupal_set_message(t('Have you read our !guidelines?', ['!guidelines' => l(t('submission guidelines'), 'node/' . $guidelines, ['attributes' => ['target' => '_blank']])]), 'question', FALSE);
      }
    }

    return parent::build_form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submit_form($form, &$form_state) {
    if (!$form_state['rebuild'] && !empty($form_state['submission']['presenters']['existing'])) {
      if (!empty($form_state['submission']['presentation_type'])) {
        $this->data['node']->get('conference_presentation_type')->set($form_state['submission']['presentation_type']);
      }

      if ($existing = $form_state['submission']['presenters']['existing'] ?? []) {
        if ($nonpresenting = $form_state['submission']['nonpresenting'] ?? []) {
          $existing = array_filter($existing, function (&$_existing) use (&$nonpresenting) {
            if ($_nonpresenting = array_search($_existing->mail, $nonpresenting)) {
              $nonpresenting[$_nonpresenting] = $_existing;
              // Remove from $existing array.
              return FALSE;
            }

            return TRUE;
          });
        }
        $this->data['node']->get('conference_presenters')->set(array_values($existing));
        $this->data['node']->get('conference_nonpresenters')->set(array_values($nonpresenting));
      }

      // Let other modules take care of modifications they've made to the form.
      $node = $this->data['node']->value();
      drupal_alter('conference_submissions_node_form_submit', $node, $form_state['submission']);
    }

    parent::submit_form($form, $form_state);
  }

}
