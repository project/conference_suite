<?php

class ConferenceSubmissionsStepCreate extends ConferenceSubmissionsStepBase {

  /**
   * Builds a node form.
   */
  public static function build_page($type, $data = [], $step = NULL) {
    if (!empty($data['node'])) {
      module_load_include('inc', 'node', 'node.pages');
      return drupal_get_form('presentation_node_form', $data['node']->value());
    }

    return [];
  }

}
