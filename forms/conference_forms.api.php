<?php

/**
 * Allows other modules to declare forms steps.
 */
function hook_conference_forms_info() {
  $info['registration'] = [
    'base path' => 'conference/registration',
    'title' => 'Conference registration', // Not translated because it will be used in hook_menu()
    'steps' => [
      // The 'start' step is mandatory
      'start' => [
        'handler' => 'ConferenceRegistrationStepStart',
        'weight' => 0,
      ],
      'register' => [
        'handler' => 'ConferenceRegistrationStepRegister',
        'weight' => 1,
        'next path' => 'cart', // Necessary for the last step, or the form will redirect to the beginning
      ],
    ],
  ];

  return $info;
}

/**
 * Allows other modules to alter forms steps.
 */
function hook_conference_forms_info_alter(&$info) {
  $info['registration']['steps']['register']['handler'] = 'ExampleConferenceRegistrationStepRegister';
  $info['registration']['steps']['register']['weight'] = 2;
}
