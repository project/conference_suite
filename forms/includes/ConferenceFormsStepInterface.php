<?php

/**
 * Class ConferenceFormsStepBase
 */
interface ConferenceFormsStepInterface {

  /**
   * Provides the form step's menu item.
   *
   * @param string $type
   * @param array $form_info
   * @param string $step
   *
   * @return array
   */
  public static function menu_item(string $type, array $form_info, string $step);

  /**
   * The form step's constructor.
   *
   * @param $form
   * @param $form_state
   *
   * @return array
   */
  public function build_form($form, &$form_state);

  /**
   * Validation handler for the form step.
   *
   * @param $form
   * @param $form_state
   */
  public function validate_form($form, &$form_state);

  /**
   * Submission handler for the form step. Takes care of redirecting the form.
   * @param $form
   * @param $form_state
   */
  public function submit_form($form, &$form_state);

}