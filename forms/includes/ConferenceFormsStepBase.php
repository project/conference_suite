<?php

/**
 * Class ConferenceFormsStepBase
 */
abstract class ConferenceFormsStepBase implements ConferenceFormsStepInterface {

  /**
   * The temporary form data.
   *
   * @var array
   */
  public $data;

  /**
   * The conference node.
   *
   * @var bool|mixed
   */
  public $conference;

  /**
   * The active user.
   *
   * @var \stdClass
   */
  public $user;

  /**
   * Name of the current form step.
   *
   * @var string
   */
  public $step;

  /**
   * Name of the next form step.
   *
   * @var string
   */
  public $next_step;

  /**
   * Information on the full form workflow.
   *
   * @var array
   *
   * @see \ConferenceForms::getInfo()
   */
  public $form_info = [];

  /**
   * Storage of loaded accounts.
   * @var array
   */
  protected $accounts = [];

  /**
   * ConferenceFormsStepBase constructor.
   *
   * @param array $data
   * @param string $step
   * @param string $next_step
   * @param array $form_info
   */
  public function __construct($data = [], $step = '', $next_step = '', $form_info = []) {
    if (empty($this->conference)) {
      $conference_nid = !empty($this->data['conference']) ? $this->data['conference'] : Conference::current();
      $this->conference = node_load($conference_nid);
    }

    global $user;
    $this->user = $user;

    $this->data = $data ?: [
      'data_id' => substr(sha1($this->user->uid . REQUEST_TIME . rand(0, microtime(TRUE))), 0, 10),
      'time' => REQUEST_TIME,
      'conference' => $this->conference->nid,
      'expire' => REQUEST_TIME + $this->expire(),
    ];

    $this->step = $step;
    $this->next_step = $next_step;
    $this->form_info = $form_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function menu_item(string $type, array $form_info, string $step) {
    if ($step != 'start') {
      $path = "{$form_info['base path']}/%conference_forms_form/$step";
      $title = !empty($form_info['steps'][$step]['title']) ? $form_info['steps'][$step]['title'] : $form_info['title'];
      $base_path_count = count(explode('/', $form_info['base path']));
      $arguments = [$type, $base_path_count, $base_path_count + 1];
    }

    $arguments = empty($arguments) ? [$type] : $arguments;

    return [
      'path' => empty($path) ? $form_info['base path'] : $path,
      'title' => empty($title) ? $form_info['title'] : $title,
      'page callback' => !empty($form_info['steps'][$step]['page callback']) ? "{$form_info['steps'][$step]['handler']}::{$form_info['steps'][$step]['page callback']}" :'drupal_get_form',
      'page arguments' => !empty($form_info['steps'][$step]['page callback']) ? $arguments : array_merge(['conference_forms_form'], $arguments),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build_form($form, &$form_state) {
    $this->form_actions($form);
    return $form;
  }

  /**
   * Adds action buttons to a form.
   *
   * @param $form
   */
  protected function form_actions(&$form) {
    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 10000,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Continue'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validate_form($form, &$form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submit_form($form, &$form_state) {
    $form_state['redirect'] = $this->goto_next($form, $form_state);
    $this->save();
    $this->post_save();
  }

  /**
   * Provides allowed step patterns for this form step.
   *
   * @return array
   */
  protected function step_patterns() {
    return [
      $this->step,
    ];
  }

  /**
   * Extracts a step name and an account object from the key of a form value.
   *
   * @param $value
   *
   * @return array|bool
   */
  protected function match_step_pattern($value) {
    $steps = implode('|', $this->step_patterns());
    if (preg_match("/(?<type>" . $steps . ")\-(?<uid>.*)/", $value, $match)) {
      $uid = $match['uid'];

      if (!isset($this->accounts[$uid])) {
        $this->accounts[$match['uid']] = user_load($uid);
      }

      return ['type' => $match['type'], 'account' => $this->accounts[$uid]];
    }

    return [];
  }

  /**
   * Returns the redirect path for the next form step.
   *
   * @param $form
   * @param $form_state
   *
   * @return string
   */
  protected function goto_next($form, &$form_state) {
    if (isset($this->form_info['steps'][$this->step]['next path'])) {
      return $this->form_info['steps'][$this->step]['next path'];
    }

    return "{$this->form_info['base path']}/{$this->data['data_id']}/{$this->next_step}";
  }

  /**
   * Save data to database.
   */
  protected function save() {
    ConferenceForms::save($this->data);
  }

  /**
   * Performs operations after saving form data to cache.
   */
  protected function post_save() {
  }

  /**
   * Cache expiration in seconds.
   *
   * @return int
   */
  protected function expire() {
    return 3600;
  }

}
