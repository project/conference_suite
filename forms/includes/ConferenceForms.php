<?php

/**
 * Helper methods for conference forms.
 */
class ConferenceForms {

  /**
   * An array of form declarations.
   *
   * @var null|array
   */
  public static $info = NULL;

  /**
   * An array of form-step names keyed by type.
   *
   * @var array
   */
  public static $steps = [];

  /**
   * Gets form declarations.
   *
   * @param null $type
   * @param bool $cache_rebuild
   *
   * @return array|mixed|null
   */
  public static function getInfo($type = NULL, $cache_rebuild = FALSE) {
    if (is_null(static::$info)) {
      // Check cache first
      $cache = cache_get('conference_forms_info', 'cache');

      if (is_object($cache) && !$cache_rebuild) {
        static::$info = $cache->data;
      }
      else {
        // Build info array
        static::$info = module_invoke_all('conference_forms_info');
        drupal_alter('conference_forms_info', static::$info);

        foreach (static::$info as $form_type => &$form_info) {
          uasort($form_info['steps'], 'drupal_sort_weight');
          // Force start step to always come first, even if other modules alter its weight
          $start = ['start' => $form_info['steps']['start']];
          unset($form_info['steps']['start']);
          $form_info['steps'] = array_merge($start, $form_info['steps']);
          $form_info['type'] = $form_type;
        }

        cache_set('conference_forms_info', static::$info);
      }
    }

    return static::$info[$type] ?? static::$info;
  }

  /**
   * Gets array of step names.
   *
   * @param $type
   *
   * @return mixed
   */
  public static function getSteps($type) {
    if (empty(static::$steps[$type])) {
      $form_info = ConferenceForms::getInfo($type);
      static::$steps[$type] = array_keys($form_info['steps']);
    }

    return static::$steps[$type];
  }

  /**
   * Gets the numeric key of a given step.
   *
   * @param $type
   * @param $step
   *
   * @return false|int|string
   */
  public static function getStepKey($type, $step) {
    return empty($step) ? 0 : array_search($step, static::getSteps($type));
  }

  public static function getStepName($type, $step) {
    return ConferenceForms::getSteps($type)[static::getStepKey($type, $step)];
  }

  /**
   * Gets a given step.
   *
   * @param $type
   * @param $step
   *
   * @return mixed
   */
  public static function getStep($type, $step) {
    $form_info = ConferenceForms::getInfo($type);

    return $form_info['steps'][static::getStepName($type, $step)];
  }

  /**
   * Gets the name of the next step.
   *
   * @param $type
   * @param $step
   *
   * @return string
   */
  public static function getNextStep($type, $step) {
    return static::getSteps($type)[static::getStepKey($type, $step) + 1] ?? '';
  }

  /**
   * Loads form data from cache.
   *
   * @param $data_id
   *
   * @return array
   */
  public static function load($data_id) {
    $cache = cache_get("conference_forms_form:$data_id");
    return !empty($cache->data) ? $cache->data : [];
  }

  /**
   * Saves form data to cache.
   *
   * @param array $data
   */
  public static function save($data) {
    cache_set("conference_forms_form:{$data['data_id']}", $data, 'cache', $data['expire']);
  }

}
