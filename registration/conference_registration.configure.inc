<?php
/**
 * @file
 * Declares variables for Conference Registration.
 */

/**
 * Implements hook_configure_variable_info().
 */
function conference_registration_configure_variable_info() {
  return [
    'conference_registration_provisional_registration:uid' => [
      'type' => 'meta',
      'title' => t('Temporary provisional-registration data'),
    ],
    'conference_registration_admin_emails' => [
      'group' => 'conference_registration_email',
      'element' => [
        '#type' => 'textarea',
        '#title' => t('Admins to receive registration notifications'),
        '#description' => t('Add email addresses, one per line, which will be notified only if corresponding users exist, and if those users have the “receive admin registration notifications” permission. Note: the subject and body of the notification are not exposed in the configuration, and must be changed in code.'),
      ],
    ],
    'conference_registration_notify_admin' => [
      'group' => 'conference_registration_email',
      'type' => 'group',
      'title' => t('Registration notification to admin'),
      'items' => [
        'subject' => [
          'title' => t('Subject of registration notification to admin'),
        ],
        'body' => [
          'title' => t('Body of registration notification to admin'),
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_configure_variable_defaults().
 */
function conference_registration_configure_variable_defaults() {
  return [
    'conference_registration_notify_admin' => [
      'subject' => 'New conference registration',
      'body' => "[!person][1] just registered for the !year conference.

[1]: [!url_person]"
    ],
  ];
}
