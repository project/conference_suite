<?php
/**
 * @file
 * conference_registration.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function conference_registration_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'conference_registration_references';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'conference_date';
  $view->human_name = 'Conference registration references';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Conference Date: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'conference_date';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  /* Field: Conference Date: Start date */
  $handler->display->display_options['fields']['from_date']['id'] = 'from_date';
  $handler->display->display_options['fields']['from_date']['table'] = 'conference_date';
  $handler->display->display_options['fields']['from_date']['field'] = 'from_date';
  $handler->display->display_options['fields']['from_date']['label'] = '';
  $handler->display->display_options['fields']['from_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['from_date']['date_format'] = 'html5_tools_iso8601';
  $handler->display->display_options['fields']['from_date']['second_date_format'] = 'html5_tools_iso8601';
  /* Field: Conference Date: End date */
  $handler->display->display_options['fields']['to_date']['id'] = 'to_date';
  $handler->display->display_options['fields']['to_date']['table'] = 'conference_date';
  $handler->display->display_options['fields']['to_date']['field'] = 'to_date';
  $handler->display->display_options['fields']['to_date']['label'] = '';
  $handler->display->display_options['fields']['to_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['to_date']['date_format'] = 'html5_tools_iso8601';
  $handler->display->display_options['fields']['to_date']['second_date_format'] = 'html5_tools_iso8601';
  /* Sort criterion: Conference Date: Start date */
  $handler->display->display_options['sorts']['from_date']['id'] = 'from_date';
  $handler->display->display_options['sorts']['from_date']['table'] = 'conference_date';
  $handler->display->display_options['sorts']['from_date']['field'] = 'from_date';
  $handler->display->display_options['sorts']['from_date']['granularity'] = 'day';
  /* Contextual filter: Conference Date: Conference */
  $handler->display->display_options['arguments']['conference']['id'] = 'conference';
  $handler->display->display_options['arguments']['conference']['table'] = 'conference_date';
  $handler->display->display_options['arguments']['conference']['field'] = 'conference';
  $handler->display->display_options['arguments']['conference']['default_action'] = 'default';
  $handler->display->display_options['arguments']['conference']['default_argument_type'] = 'conference_current_id';
  $handler->display->display_options['arguments']['conference']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['conference']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['conference']['summary_options']['items_per_page'] = '25';

  /* Display: Entity Reference */
  $handler = $view->new_display('entityreference', 'Entity Reference', 'dates');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'entityreference_style';
  $handler->display->display_options['style_options']['search_fields'] = array(
    'type' => 'type',
    'date_id' => 0,
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'entityreference_fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['conference_registration_references'] = $view;

  return $export;
}
