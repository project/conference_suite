<?php
/**
 * @file
 * conference_registration.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function conference_registration_commerce_product_default_types() {
  $items = array(
    'conference_registration' => array(
      'type' => 'conference_registration',
      'name' => 'Conference registration',
      'description' => '',
      'help' => '',
      'revision' => 1,
    ),
  );
  return $items;
}

/**
 * Implements hook_views_api().
 */
function conference_registration_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
