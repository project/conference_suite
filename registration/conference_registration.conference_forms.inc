<?php

/**
 * @file
 * Adds conference_forms support to conference_registration.
 */

/**
 * Implements hook_conference_forms_info().
 *
 * @return mixed
 */
function conference_registration_conference_forms_info() {
  $info['registration'] = [
    'base path' => 'conference/registration',
    'title' => 'Conference registration',
    'access callback' => 'conference_registration_access',
    'steps' => [
      // The 'start' step is mandatory
      'start' => [
        'handler' => 'ConferenceRegistrationStepStart',
        'weight' => 0,
      ],
      'register' => [
        'handler' => 'ConferenceRegistrationStepRegister',
        'weight' => 1,
      ],
      'cart' => [
        'handler' => 'ConferenceRegistrationStepCart',
        'page callback' => 'build_page',
        'weight' => 1000,
      ],
    ],
  ];

  return $info;
}