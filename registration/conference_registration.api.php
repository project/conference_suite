<?php
/**
 * @file
 * API for conference_registration.
 */

/**
 * Allows other modules to determine registration dates.
 */
function hook_conference_registration_open() {
  // Example from conference_schedule
  return [
    'conference_schedule_deadline',
  ];
}

/**
 * Allows other modules to add criteria to the product query.
 *
 * @param $products EntityFieldQuery
 * @param $account
 */
function hook_conference_registration_select_products_query_alter($products, $account, $conference) {

}

/**
 * Allows other modules to add criteria to the product query.
 *
 * @param $products array
 * @param $account
 */
function hook_conference_registration_select_products_alter($products, $account, $conference) {

}

/**
 * Allows other modules to modify the registration-workflow submit handler.
 */
function hook_conference_registration_select_submit_alter(&$form, &$form_state) {

}