<?php

/**
 * Class ConferenceRegistrationStepRegister
 */
class ConferenceRegistrationStepRegister extends ConferenceRegistrationStepBase {

  use ConferenceRegistrationRegisterTrait;

  /**
   * @inheritdoc
   */
  public function build_form($form, &$form_state) {
    $items = 0;

    static $recipients;

    if (empty($recipients)) {
      if (in_array('self', $this->data['recipient'])) {
        $recipients['self'] = $this->user;
      }

      if (in_array('other', $this->data['recipient'])) {
        foreach ($this->data['recipients'] as $uid => $account) {
          if ($account->uid == $this->user->uid) {
            // If 'other' and one of the email addresses belongs to the active user, mark it as such
            $recipients['self'] = $account;
            unset($this->data['recipients'][$uid]);
            break;
          }
        }

        $recipients['other'] = $this->data['recipients'];
      }
    }

    if (!empty($recipients['self'])) {
      $form['self'] = [
        '#type' => 'fieldset',
        '#title' => t('Your registration options'),
      ];
      $items = 2;
      $this->select_products($form['self'], $recipients['self'], $items);
    }

    if (!empty($recipients['other'])) {
      foreach ($recipients['other'] as $uid => $account) {
        $form["other-$uid"] = [
          '#type' => 'fieldset',
          '#title' => t('Register !name', ['!name' => format_username($account)]),
        ];

        $items = $items + 2;
        $this->select_products($form["other-$uid"], $account, $items);
      }
    }

    return parent::build_form($form, $form_state);
  }

  /**
   * @inheritdoc
   */
  public function submit_form($form, &$form_state) {
    form_state_values_clean($form_state);
    $values = $form_state['values'];

    $products = commerce_product_load_multiple(array_filter($values, function (&$value) {
      return !is_array($value);
    }));

    // Extract products per user
    foreach ($values as $key => $pid) {
      $this->process_form_values($key, $pid, $products);
    }

    parent::submit_form($form, $form_state);
  }

  /**
   * Form-construction helper: builds list of available registration products
   * per user.
   *
   * @param $form
   * @param $account
   * @param $type
   * @param $conference
   * @param $items
   * @param string $product_type
   */
  protected function select_products(&$form, $account, &$items) {
    // Registration status
    if ($this->is_user_registered($account)) {
      $this->empty_registration_form_element($form, $items, $account);
    }
    else {
      // Find currently available products
      $products = conference_registration_get_open_products($this->conference, $account);
      $this->registration_form_element($form, $items, $account, $products);
    }
  }

  /**
   * Adds a form element warning that the user is already registered.
   *
   * @param $form
   * @param $items
   * @param $account
   */
  protected function empty_registration_form_element(&$form, &$items, $account) {
    $year = Conference::year($this->conference);

    $form["empty-registration-{$account->uid}"] = [
      '#markup' => '<p>' . t('!name is already registered to the !year conference.', [
          '!name' => format_username($account),
          '!year' => $year,
        ]) . '</p>',
    ];
    $items--;
  }

  /**
   * Adds registration form elements.
   *
   * @param $form
   * @param $items
   * @param $account
   * @param $products
   */
  protected function registration_form_element(&$form, &$items, $account, $products) {
    if (empty($products['commerce_product'])) {
      $form["empty-registration-{$account->uid}"] = [
        '#markup' => '<p>' . t('There are no registration levels available for !name.', ['!name' => format_username($account)]) . '</p>',
      ];
      $items--;
    }
    else {
      if (!empty($special['commerce_product'])) {
        $products['commerce_product'] = array_diff_key($products['commerce_product'], $special['commerce_product']);
      }

      $products = commerce_product_load_multiple(array_keys($products['commerce_product']));

      // Build radio buttons
      $form["registration-{$account->uid}"] = [
        '#type' => 'radios',
        '#title' => t('Registration level'),
        '#options' => [],
        '#required' => TRUE,
      ];
      foreach ($products as $pid => $product) {
        $price = field_view_field('commerce_product', $product, 'commerce_price', 'node_full');
        $form["registration-{$account->uid}"]['#options'][$pid] = $product->title . ' – ' . strip_tags(drupal_render($price));
      }
    }
  }

  /**
   * @inheritdoc
   */
  protected function step_patterns() {
    return [
      'registration',
    ];
  }

  /**
   * Wrapper for is_user_registered(). Useful if child classes need to extend it.
   *
   * @param $account
   *
   * @return \EntityFieldQuery|mixed
   */
  protected function is_user_registered($account) {
    return is_user_registered($account);
  }
}