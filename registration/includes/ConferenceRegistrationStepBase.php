<?php

/**
 * Class ConferenceRegistrationStepBase
 */
abstract class ConferenceRegistrationStepBase extends ConferenceFormsStepBase {

  /**
   * Open registration dates.
   *
   * @var array|null
   */
  public $dates;

  /**
   * ConferenceRegistrationStepBase constructor.
   *
   * @param array $data
   * @param string $step
   * @param string $next_step
   * @param array $form_info
   */
  public function __construct(array $data = [], string $step = '', string $next_step = '', array $form_info = []) {
    parent::__construct($data, $step, $next_step, $form_info);
    $this->dates = conference_registration_get_open_dates();
  }

}