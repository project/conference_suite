<?php

/**
 * Class ConferenceRegistrationStepStart
 */
class ConferenceRegistrationStepStart extends ConferenceRegistrationStepBase {

  /**
   * @inheritdoc
   */
  public function build_form($form, &$form_state) {
    if (!conference_registration_open($this->data['conference'])) {
      if (!user_access('bypass registration deadline')) {
        drupal_set_title(t('Registration is not open'));
        return [
          'message' => [
            '#markup' => '<p>' . t('Registration for the @year conference is not open.', ['@year' => Conference::year($this->data['conference'])]) . '</p>',
          ],
        ];
      }

      drupal_set_message(t('Registration for the @year conference is not open to the general public.', ['@year' => Conference::year($this->data['conference'])]), 'warning');
    }

    $form = $this->form_elements();

    return parent::build_form($form, $form_state);
  }

  /**
   * Builds form elements independently of access conditions.
   *
   * @return array
   * @throws \EntityMetadataWrapperException
   */
  protected function form_elements() {
    $form['recipient'] = [
      '#type' => 'checkboxes',
      '#title' => t('Registration recipient'),
      '#field_prefix' => t('Whom do you want to register for the !year conference?', ['!year' => Conference::year($this->conference)]),
      '#description' => t('You can register yourself as well as other people at the same time.'),
      '#required' => TRUE,
    ];

    if (!is_user_registered($this->user, $this->conference->nid)) {
      $form['recipient']['#options']['self'] = format_username($this->user);
    }

    $form['recipient']['#options']['other'] = t('Someone else');

    $form['recipients'] = [
      '#type' => 'textarea',
      '#title' => t('Email addresses of other registrants'),
      '#description' => t('Enter the email addresses of the people you want to register for the !year conference, one per line. You can register only people who already have accounts on the website. Make sure the email addresses you enter correspond to the ones they used to sign up for the website.', ['!year' => Conference::year($this->data['conference'])]),
      '#required' => TRUE,
      '#default_value' => html_entity_decode('&#8203;'),
      '#states' => [
        'invisible' => [
          '#edit-recipient-other' => ['checked' => FALSE],
        ],
      ],
    ];

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function validate_form($form, &$form_state) {
    $form_state['values']['recipients'] = str_replace(html_entity_decode('&#8203;'), '', $form_state['values']['recipients']);
    if ($form_state['values']['recipient']['other']) {
      $this->user;
      $recipients = explode("\n", $form_state['values']['recipients']);
      $valid = [];
      $invalid = [];
      $nonexistent = [];

      array_walk($recipients, function (&$mail) use (&$valid, &$invalid, &$nonexistent, &$self) {
        // General cleanup
        $mail = strtolower(trim(str_replace(' ', '', $mail)));
        $validate = valid_email_address($mail);
        if (!$validate) {
          $invalid[] = $mail;
        }

        if (module_exists('multiple_email')) {
          $exists = multiple_email_find_address($mail);

          if (!empty($exists)) {
            $account = user_load($exists->uid);
          }
        }
        else {
          $account = user_load_by_mail($mail);
        }

        if (empty($account)) {
          // @todo Do something about this
        }

        if ($validate && !empty($account)) {
          // Prevent registering the same user twice
          $valid[$account->uid] = $account;
        }
      });

      if (!empty($invalid)) {
        drupal_set_message(format_plural(count($invalid), 'The email address %mail is invalid and will not be considered.', 'These email addresses are invalid and will not be considered: %mail.', ['%mail' => implode(', ', $invalid)]), 'warning');
      }

      if (empty($valid) && empty($nonexistent)) {
        form_set_error('recipients', t('You must enter at least one valid email address belonging to an active account on this website.'));
      }

      // Replace with full user objects
      $form_state['values']['recipients'] = $valid + $nonexistent;
    }
  }

  /**
   * @inheritdoc
   */
  public function submit_form($form, &$form_state) {
    $this->data['recipient'] = array_filter($form_state['values']['recipient']);

    if (!empty($form_state['values']['recipients'])) {
      $this->data['recipients'] = $form_state['values']['recipients'];
    }

    parent::submit_form($form, $form_state);
  }
}