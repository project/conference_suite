<?php

trait ConferenceRegistrationRegisterTrait {

  /**
   * Adds line items to the cart data.
   *
   * @param $key
   * @param $value
   * @param array $products
   */
  protected function process_form_values($key, $value, $products = []) {
    /**
     * @var string $type
     * @var \stdClass $account
     */
    if (extract($this->match_step_pattern($key))) {
      $line_item_type = !empty($products[$value]) ? 'product' : $value;

      // Create line item
      $recipient = $account->uid;

      $line_item = entity_create('commerce_line_item', [
        'type' => $line_item_type,
        'data' => [
          'context' => [
            'recipients' => [$recipient],
            'add_to_cart_combine' => FALSE,
          ],
        ],
      ]);

      if ($line_item->type == 'existing') {
        $existing_items[$account->uid] = $line_item;
      }
      else {
        if (isset($products[$value])) {
          $this->data['cart'][$type][$account->uid] = [$products[$value], $line_item];
        }
      }
    }
  }

  /**
   * @inheritdoc
   */
  protected function post_save() {
    if ($this->next_step == 'cart' && !empty($this->data['cart'])) {
      // Add to cart
      foreach ($this->data['cart'] as $type => &$item) {
        foreach ($item as $uid => &$cart) {
          list($product, $line_item) = $cart;

          if (!empty($product) && !empty($line_item)) {
            static::add_to_cart($product, $line_item, $this->user);
            $cart[1] = $line_item;
          }
        }
      }

      $this->save();
    }
  }

  /**
   * Adds individual products to the shopping cart.
   *
   * @param $product
   * @param $line_item
   * @param bool $skip_check
   *
   * @throws \EntityMetadataWrapperException
   */
  public static function add_to_cart($product, &$line_item, $user, $skip_check = FALSE) {
    // If the line item passed to the function is new
    if (empty($line_item->line_item_id)) {
      if (!$skip_check) {
        $duplicate = static::check_line_item_duplicate($product, $line_item, $user);

        if (is_object($duplicate)) {
          commerce_line_item_delete($duplicate->line_item_id);
        }
      }

      // Create the new product line item of the same type.
      $quantity = empty($line_item->quantity) ? 1 : $line_item->quantity;
      $line_item = commerce_product_line_item_new($product, $quantity, $line_item->order_id, $line_item->data, $line_item->type);

      // Allow modules to prepare this as necessary. This hook is defined by the
      // Product Pricing module.
      drupal_alter('commerce_product_calculate_sell_price_line_item', $line_item);

      // Process the unit price through Rules so it reflects the user's actual
      // purchase price.
      rules_invoke_event('commerce_product_calculate_sell_price', $line_item);

      // Only attempt an Add to Cart if the line item has a valid unit price.
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

      if (!is_null($line_item_wrapper->commerce_unit_price->value())) {
        // Add the product to the specified shopping cart.
        $line_item = commerce_cart_product_add(
          $user->uid,
          $line_item,
          isset($line_item->data['context']['add_to_cart_combine']) ? $line_item->data['context']['add_to_cart_combine'] : TRUE
        );
      }
      else {
        drupal_set_message(t('%title could not be added to your cart.', ['%title' => $product->title]), 'error');
      }
    }
  }

  /**
   * Checks if a line item exists with the same product type and the same
   * recipient.
   *
   * @param $product
   * @param $line_item
   * @param $user
   *
   * @return mixed
   */
  public static function check_line_item_duplicate($product, &$line_item, $user) {
    if (empty($product) || empty($line_item) || empty($user)) {
      return NULL;
    }

    // Prevent adding the same type of product twice for the same recipient
    $recipient = reset($line_item->data['context']['recipients']);
    static $order;
    $order = empty($order) ? commerce_cart_order_load($user->uid) : $order;

    if (!empty($order)) {
      // There will never be duplicate products for the same account in the same registration form,
      // so it should be okay to use a static variable for the line items as well
      static $line_items;
      $line_items = empty($line_items) ? commerce_line_item_load_multiple(array_column($order->commerce_line_items[LANGUAGE_NONE], 'line_item_id')) : $line_items;

      foreach ($line_items as $lid => $item) {
        $item_recipient = reset($item->data['context']['recipients']);
        $item_product = commerce_product_load_by_sku($item->line_item_label);

        if ($item_product->type == $product->type && $item_recipient == $recipient) {
          return $item;
        }
      }
    }
  }

}
