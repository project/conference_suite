<?php

/**
 * Class ConferenceRegistrationStepCart
 */
class ConferenceRegistrationStepCart extends ConferenceRegistrationStepBase {

  /**
   * Displays the shopping cart form and associated information.
   *
   * @param string $type
   * @param null $data
   * @param null $step
   *
   * @return NULL|string
   * @throws \Exception
   *
   * @see commerce_cart_view()
   */
  public static function build_page($type = 'registration', $data = NULL, $step = NULL) {
    global $user;

    // First check to make sure we have a valid order.
    if ($order = commerce_cart_order_load($user->uid)) {
      $wrapper = entity_metadata_wrapper('commerce_order', $order);

      // Only show the cart form if we found product line items.
      if (commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types()) > 0) {

        // Add the form for editing the cart contents.
        $content = commerce_embed_view('commerce_cart_form', 'default', [$order->order_id], current_path());
      }
    }

    if (empty($content)) {
      // Default to displaying an empty message.
      $content = theme('commerce_cart_empty_page');
    }

    return $content;
  }
}
