<?php
/**
 * @file
 * Provides schedule-snapshot functionality.
 */
/**
 * Displays a table of active snapshots for the current conference.
 */
function conference_schedule_snapshot_snapshots() {
  drupal_set_title(t('Schedule snapshots'));
  static $conference;
  $conference = empty($conference) ? Conference::current() : $conference;
  $conference_year = Conference::year($conference);
  $snapshots = cvget('conference_schedule_snapshots', ['conference_id' => $conference]);

  $table = [
    'header' => [
      'date' => t('Date'),
      'status' => t('Status'),
      'operations' => t('Operations'),
    ],
  ];

  $operations = [
    'delete' => t('delete'),
    'final' => t('mark as final'),
  ];

  if ( !empty($snapshots) ) {
    foreach ( $snapshots as $snapshot ) {
      $row = [
        'date' => format_date($snapshot['date'], 'custom', 'c'),
        'status' => empty($snapshot['status']) ? NULL : conference_schedule_snapshot_get_status($snapshot['status']),
      ];

      foreach ( $operations as $operation => $title ) {
        $ops[$operation] = l($title, 'admin/conference/settings/schedule/snapshots/' . $snapshot['date'] . '/' . $operation);
      }

      if ( !empty($snapshot['status']) && $snapshot['status'] == 'final' ) {
        unset($ops['final']);
      }

      $row['operations'] = implode(' | ', $ops);

      $table['rows'][] = $row;
    }
  }
  else {
    $table['rows'][][] = [
      'data' => t('There are no snapshots for the !year conference.', ['!year' => $conference_year]),
      'colspan' => count($table['header']),
    ];
  }

  $form['snapshots'] = [
    '#markup' => theme('table', $table),
  ];

  return $form;
}

/**
 * Form constructor for the snapshot add form.
 */
function conference_schedule_snapshot_form() {
  $conference = Conference::current();
  $conference_year = Conference::year($conference);

  $form['conference'] = [
    '#type' => 'value',
    '#value' => $conference,
  ];

  $form['confirmation'] = [
    '#markup' => '<p>' . t('Do you want to create a schedule snapshot for the !year conference?', ['!year' => $conference_year]) . '</p>'
  ];

  $form['status'] = [
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => [
      t('- None -'),
      'administrative' => t('Administrative'),
      'tentative' => t('Tentative'),
    ],
  ];

  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Create snapshot'),
  ];

  $form['actions']['cancel'] = [
    '#type' => 'submit',
    '#name' => 'cancel',
    '#value' => t('Cancel'),
  ];

  return $form;
}

/**
 * Form submission handler for conference_schedule_snapshot_form().
 */
function conference_schedule_snapshot_form_submit($form, &$form_state) {
  if ( $form_state['triggering_element']['#name'] != 'cancel' ) {
    $conference = $form_state['values']['conference'];
    $snapshots = cvget('conference_schedule_snapshots', ['conference_id' => $conference]);
    $time = time();
    $snapshots[$time] = [
      'date' => $time,
    ];

    if ( !empty($form_state['values']['status']) ) {
      $snapshots[$time]['status'] = $form_state['values']['status'];
    }

    cvset('conference_schedule_snapshots', $snapshots, ['conference_id' => $conference]);
    drupal_set_message(t('New snapshot created.'));
  }

  $form_state['redirect'] = 'admin/conference/settings/schedule/snapshots';
}

/**
 * Performs a given operation on a snapshot.
 */
function conference_schedule_snapshot_edit($form, &$form_state, $snapshot, $operation) {
  $conference = Conference::current();
  $snapshots = cvget('conference_schedule_snapshots', ['conference_id' => $conference]);

  if ( !empty($snapshots[$snapshot]) ) {
    $form['operation'] = [
      '#type' => 'value',
      '#value' => $operation,
    ];

    $form['conference'] = [
      '#type' => 'value',
      '#value' => $conference,
    ];

    $form['snapshots'] = [
      '#type' => 'value',
      '#value' => $snapshots,
    ];

    $form[$operation] = [
      '#type' => 'value',
      '#value' => $snapshot,
    ];

    switch ( $operation ) {
      case 'delete' :
        return confirm_form(
          $form,
          t('Do you want to delete the selected snapshot?'),
          'admin/conference/settings/schedule/snapshots',
          t('This action cannot be undone.'),
          t('Yes, delete it'),
          t('No, go back')
        );
      break;

      case 'final' :
        return confirm_form(
          $form,
          t('Mark as final'),
          'admin/conference/settings/schedule/snapshots',
          t('Do you want to mark the selected snapshot as final?'),
          t('Yes, mark it'),
          t('No, go back')
        );
      break;
    }
  }
  else {
    return [
      'empty' => [
        '#markup' => '<p>' . t('The selected snapshot does not exist.') . '</p>'
      ],
      'back' => [
        '#markup' => '<p>' . l(t('Go back'), 'admin/conference/settings/schedule/snapshots') . '</p>'
      ],
    ];
  }
}

/**
 * Form submission handler for conference_schedule_snapshot_edit().
 */
function conference_schedule_snapshot_edit_submit($form, &$form_state) {
  $operation = $form_state['values']['operation'];
  $conference = $form_state['values']['conference'];
  $snapshots = $form_state['values']['snapshots'];
  $selected_snapshot = $form_state['values'][$operation];

  switch ( $operation ) {
    case 'delete' :
      unset($snapshots[$selected_snapshot]);

      drupal_set_message(t('Snapshot deleted.'));
    break;

    case 'final' :
      // Unmark all other snapshots
      foreach ( $snapshots as &$snapshot ) {
        if ( $snapshot['date'] != $selected_snapshot ) {
          $snapshot['status'] = FALSE;
        }
        else {
          $snapshot['status'] = 'final';
        }
      }

      drupal_set_message(t('Snapshot marked as final.'));
    break;
  }

  cvset('conference_schedule_snapshots', $snapshots, ['conference_id' => $conference]);
  $form_state['redirect'] = 'admin/conference/settings/schedule/snapshots';
}

function conference_schedule_snapshot_get_status($status) {
  $options = [
    'administrative' => t('Administrative'),
    'tentative' => t('Tentative'),
    'final' => t('Final'),
  ];

  if ( !empty($options[$status]) ) {
    return $options[$status];
  }
}
