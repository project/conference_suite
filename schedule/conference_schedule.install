<?php
/**
 * @file
 * Install file for Conference Schedule.
 *
 * @todo add conference_schedule_uninstall() to delete config variables.
 */
function conference_schedule_install() {
  $fields = field_info_field_map();

  foreach ( _conference_schedule_default_field_bases() as $field ) {
    if (empty($fields[$field['field_name']])) {
      field_create_field($field);
    }
  }

  foreach ( _conference_schedule_default_field_instances() as $instance ) {
    field_create_instance($instance);
  }

  node_types_rebuild();
  $types = node_type_get_types();
  // Body field
  node_add_body_field($types['conference_room'], st('Description'));
  node_add_body_field($types['conference_timeslot'], st('Description'));

  // Default settings
  variable_set('node_options_conference_room', ['status', 'revision']);
  variable_set('node_preview_conference_room', 0);
  variable_set('node_submitted_conference_room', 0);
  variable_set('node_options_conference_timeslot', ['status', 'revision']);
  variable_set('node_preview_conference_timeslot', 0);
  variable_set('node_submitted_conference_timeslot', 0);
  variable_set('ant_conference_timeslot', 1);
  variable_set('ant_pattern_conference_timeslot', '[node:field-timeslot:value:custom:l], [node:field-timeslot:value:custom:F] [node:field-timeslot:value:custom:j], [node:field-timeslot:value:custom:g]:[node:field-timeslot:value:custom:i] [node:field-timeslot:value:custom:a] to [node:field-timeslot:value2:custom:g]:[node:field-timeslot:value2:custom:i] [node:field-timeslot:value2:custom:a]');
  variable_set('ant_php_conference_timeslot', 0);

  // Enable comments by default
  variable_set('comment_conference_room', 0);
  variable_set('comment_conference_timeslot', 0);
}

/**
 * Implements hook_uninstall().
 */
function conference_schedule_uninstall() {
  $instances = _conference_schedule_default_field_instances();

  foreach ($instances as $instance) {
    field_delete_instance($instance, TRUE);
  }
}

function _conference_schedule_default_field_bases() {
  $field_bases['conference_room_reference'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'conference_room_reference',
    'field_permissions' => [
      'type' => 0,
    ],
    'indexes' => [
      'target_entity' => [
        0 => 'target_id',
      ],
      'target_id' => [
        0 => 'target_id',
      ],
    ],
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => [
      'handler' => 'views',
      'handler_settings' => [
        'behaviors' => [
          'views-select-list' => [
            'status' => 0,
          ],
        ],
        'view' => [
          'args' => [],
          'display_name' => 'room',
          'view_name' => 'conference_schedule_node_references',
        ],
      ],
      'handler_submit' => 'Change handler',
      'profile2_private' => FALSE,
      'target_type' => 'node',
    ],
    'translatable' => 0,
    'type' => 'entityreference',
  ];

  $field_bases['conference_timeslot_reference'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'conference_timeslot_reference',
    'field_permissions' => [
      'type' => 0,
    ],
    'indexes' => [
      'target_entity' => [
        0 => 'target_id',
      ],
      'target_id' => [
        0 => 'target_id',
      ],
    ],
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => [
      'handler' => 'views',
      'handler_settings' => [
        'behaviors' => [
          'views-select-list' => [
            'status' => 0,
          ],
        ],
        'view' => [
          'args' => [],
          'display_name' => 'timeslot',
          'view_name' => 'conference_schedule_node_references',
        ],
      ],
      'handler_submit' => 'Change handler',
      'profile2_private' => FALSE,
      'target_type' => 'node',
    ],
    'translatable' => 0,
    'type' => 'entityreference',
  ];

  $field_bases['conference_session_number'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'conference_session_number',
    'field_permissions' => [
      'type' => 2,
    ],
    'indexes' => [
      'format' => [
        0 => 'format',
      ],
    ],
    'locked' => 0,
    'module' => 'text',
    'settings' => [
      'max_length' => 255,
    ],
    'translatable' => 0,
    'type' => 'text',
  ];

  $field_bases['conference_timeslot'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'conference_timeslot',
    'field_permissions' => [
      'type' => 0,
    ],
    'indexes' => [],
    'locked' => 0,
    'module' => 'date',
    'settings' => [
      'cache_count' => 4,
      'cache_enabled' => 0,
      'granularity' => [
        'day' => 'day',
        'hour' => 'hour',
        'minute' => 'minute',
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ],
      'timezone_db' => '',
      'todate' => 'required',
      'tz_handling' => 'date',
    ],
    'translatable' => 0,
    'type' => 'datestamp',
  ];

  $field_bases['conference_timeslot_picker'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'conference_timeslot_picker',
    'field_permissions' => [
      'type' => 2,
    ],
    'indexes' => [
      'value' => [
        0 => 'value',
      ],
    ],
    'locked' => 0,
    'module' => 'list',
    'settings' => [
      'allowed_values' => [],
      'allowed_values_function' => '_conference_schedule_conference_timeslot_picker_values',
      'allowed_values_php' => '',
    ],
    'translatable' => 0,
    'type' => 'list_text',
  ];

  $field_bases['conference_weight'] = [
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => [],
    'field_name' => 'conference_weight',
    'field_permissions' => [
      'type' => 2,
    ],
    'indexes' => [],
    'locked' => 0,
    'module' => 'number',
    'settings' => [],
    'translatable' => 0,
    'type' => 'number_integer',
  ];

  return $field_bases;
}

function _conference_schedule_default_field_instances() {
  $field_instances['node-conference_room-conference_reference'] = [
    'bundle' => 'conference_room',
    'default_value' => NULL,
    'default_value_function' => 'conference_field_default_value',
    'deleted' => 0,
    'description' => '',
    'display' => [
      'default' => [
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => [
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 1,
      ],
      'teaser' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_reference',
    'label' => 'Conference',
    'required' => 0,
    'settings' => [
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'options',
      'settings' => [],
      'type' => 'options_select',
      'weight' => 5,
    ],
  ];

  $field_instances['node-conference_room-conference_weight'] = [
    'bundle' => 'conference_room',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => [
      'default' => [
        'label' => 'above',
        'module' => 'number',
        'settings' => [
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ],
        'type' => 'number_integer',
        'weight' => 2,
      ],
      'teaser' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_weight',
    'label' => 'Weight',
    'required' => 0,
    'settings' => [
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 0,
      'module' => 'html5_tools',
      'settings' => [],
      'type' => 'numberfield',
      'weight' => 6,
    ],
  ];

  $field_instances['node-conference_timeslot-conference_reference'] = [
    'bundle' => 'conference_timeslot',
    'default_value' => NULL,
    'default_value_function' => 'conference_field_default_value',
    'deleted' => 0,
    'description' => '',
    'display' => [
      'default' => [
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => [
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 2,
      ],
      'teaser' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
      'token' => [
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => [
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 2,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_reference',
    'label' => 'Conference',
    'required' => 0,
    'settings' => [
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'options',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ],
      'type' => 'options_select',
      'weight' => 6,
    ],
  ];

  $field_instances['node-conference_timeslot-conference_timeslot'] = [
    'bundle' => 'conference_timeslot',
    'deleted' => 0,
    'description' => '',
    'display' => [
      'default' => [
        'label' => 'above',
        'module' => 'date',
        'settings' => [
          'format_type' => 'long_no_year_12_hour',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ],
        'type' => 'date_default',
        'weight' => 1,
      ],
      'teaser' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
      'token' => [
        'label' => 'above',
        'module' => 'date',
        'settings' => [
          'format_type' => 'html5_tools_iso8601',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
        ],
        'type' => 'date_default',
        'weight' => 1,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_timeslot',
    'label' => 'Time slot',
    'required' => 0,
    'settings' => [
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'date',
      'settings' => [
        'increment' => 15,
        'input_format' => 'm/d/Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'text_parts' => [],
        'year_range' => '-3:+3',
      ],
      'type' => 'date_popup',
      'weight' => 1,
    ],
  ];

  $field_instances['node-session-conference_room_reference'] = [
    'bundle' => 'session',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => [
      'default' => [
        'label' => 'hidden',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 13,
      ],
      'ical' => [
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => [
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 13,
      ],
      'markdown' => [
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => [
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 13,
      ],
      'printable' => [
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => [
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 13,
      ],
      'teaser' => [
        'label' => 'hidden',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 13,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_room_reference',
    'label' => 'Conference room',
    'required' => 0,
    'settings' => [
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'entityreference',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ],
      'type' => 'entityreference_autocomplete',
      'weight' => 18,
    ],
  ];

  $field_instances['node-session-conference_session_number'] = [
    'bundle' => 'session',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => [
      'default' => [
        'label' => 'hidden',
        'module' => 'text',
        'settings' => [],
        'type' => 'text_default',
        'weight' => 21,
      ],
      'ical' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 0,
      ],
      'markdown' => [
        'label' => 'hidden',
        'module' => 'text',
        'settings' => [],
        'type' => 'text_default',
        'weight' => 21,
      ],
      'printable' => [
        'label' => 'hidden',
        'module' => 'text',
        'settings' => [],
        'type' => 'text_default',
        'weight' => 21,
      ],
      'teaser' => [
        'label' => 'hidden',
        'module' => 'text',
        'settings' => [],
        'type' => 'text_default',
        'weight' => 6,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_session_number',
    'label' => 'Session number',
    'required' => 0,
    'settings' => [
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'text',
      'settings' => [
        'maxlength_js' => 0,
        'size' => 60,
      ],
      'type' => 'text_textfield',
      'weight' => 20,
    ],
  ];

  $field_instances['node-session-conference_timeslot_reference'] = [
    'bundle' => 'session',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => [
      'default' => [
        'label' => 'hidden',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 12,
      ],
      'ical' => [
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => [
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 12,
      ],
      'markdown' => [
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => [
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 12,
      ],
      'printable' => [
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => [
          'link' => TRUE,
          'skip_access_check' => 0,
        ],
        'type' => 'entityreference_label',
        'weight' => 12,
      ],
      'teaser' => [
        'label' => 'hidden',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 12,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_timeslot_reference',
    'label' => 'Conference time slot',
    'required' => 0,
    'settings' => [
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'entityreference',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ],
      'type' => 'entityreference_autocomplete',
      'weight' => 17,
    ],
  ];

  $field_instances['node-session-conference_timeslot_picker'] = [
    'bundle' => 'session',
    'default_value' => NULL,
    'default_value_function' => '',
    'default_value_php' => '',
    'deleted' => 0,
    'description' => '',
    'display' => [
      'default' => [
        'label' => 'hidden',
        'module' => 'list',
        'settings' => [],
        'type' => 'list_default',
        'weight' => 3,
      ],
      'ical' => [
        'label' => 'hidden',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 4,
      ],
      'markdown' => [
        'label' => 'hidden',
        'module' => 'list',
        'settings' => [],
        'type' => 'list_default',
        'weight' => 3,
      ],
      'printable' => [
        'label' => 'hidden',
        'module' => 'list',
        'settings' => [],
        'type' => 'list_default',
        'weight' => 3,
      ],
      'teaser' => [
        'label' => 'hidden',
        'module' => 'list',
        'settings' => [],
        'type' => 'list_default',
        'weight' => 1,
      ],
    ],
    'entity_type' => 'node',
    'field_label_plurals_singular' => '',
    'field_name' => 'conference_timeslot_picker',
    'label' => 'Time-slot picker',
    'required' => 0,
    'settings' => [
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 1,
      'module' => 'options',
      'settings' => [],
      'type' => 'options_buttons',
      'weight' => 0,
    ],
  ];

  $field_instances['node-session-conference_weight'] = [
    'bundle' => 'session',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => [
      'default' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 17,
      ],
      'ical' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 16,
      ],
      'markdown' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 17,
      ],
      'printable' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 17,
      ],
      'teaser' => [
        'label' => 'above',
        'settings' => [],
        'type' => 'hidden',
        'weight' => 15,
      ],
    ],
    'entity_type' => 'node',
    'field_name' => 'conference_weight',
    'label' => 'Weight',
    'required' => 0,
    'settings' => [
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ],
    'widget' => [
      'active' => 0,
      'module' => 'html5_tools',
      'settings' => [],
      'type' => 'numberfield',
      'weight' => 21,
    ],
  ];

  return $field_instances;
}

/**
 * Convert conference_timeslot field to use own time zone.
 */
function conference_schedule_update_7200(&$sandbox) {
  module_load_include('inc', 'conference_date', 'conference_date.convert');
  conference_date_convert_timezone('conference_timeslot');
}

/**
 * Set proper default_value_function for conference_reference instances.
 */
function conference_schedule_update_7201(&$sandbox) {
  foreach ( _conference_schedule_default_field_instances() as $instance ) {
    field_update_instance($instance);
  }
}

/**
 * Reset variable timeslot_title_date_pattern.
 */
function conference_schedule_update_7202(&$sandbox) {
  cvdel('timeslot_title_date_pattern');
}

/**
 * Retrofit scheduling data for all conferences.
 */
function conference_schedule_update_7203(&$sandbox) {
  foreach (array_keys(Conference::optionsList()) as $nid) {
    conference_schedule_scheduling_info_refresh($nid);
  }
}

/**
 * Clear conference_schedule_get_timeslots cache.
 */
function conference_schedule_update_7204(&$sandbox) {
  cache_clear_all('conference_schedule_get_timeslots:', 'cache', TRUE);
}

/**
 * Remove obsolete configuration variables conference_rooms and conference_timeslots.
 */
function conference_schedule_update_7205(&$sandbox) {
  db_query("DELETE FROM {configure} WHERE `name` LIKE 'conference_rooms:%';")->execute();
  db_query("DELETE FROM {configure} WHERE `name` LIKE 'conference_timeslots:%';")->execute();
}

/**
 * Clear conference_schedule_get_timeslots cache.
 */
function conference_schedule_update_7206(&$sandbox) {
  cache_clear_all('conference_schedule_get_timeslots:', 'cache', TRUE);
}

/**
 * Convert variable name from conference_schedule_timeslot_simple_default_timeslots
 * to conference_schedule_timeslots_default_timeslots.
 */
function conference_schedule_update_7300(&$sandbox) {
  $val = cvget('conference_schedule_timeslot_simple_default_timeslots');
  cvset('conference_schedule_timeslots_default_timeslots', $val);
  cvdel('conference_schedule_timeslot_simple_default_timeslots');
}
