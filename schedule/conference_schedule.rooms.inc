<?php
/**
 * @file
 * Bulk creation of rooms.
 */

/**
 * Form constructor: bulk creation of rooms.
 */
function conference_schedule_rooms_bulk_form($form, &$form_state) {
  drupal_set_title(t('Add rooms in bulk'));

  $form['rooms'] = [
    '#type' => 'textarea',
    '#title' => t('Room names and capacity'),
    '#description' => t('Enter one room per line, in the format <code>Room name|XX</code>, where “XX” is the room’s capacity.'),
    '#rows' => 25,
    '#required' => TRUE,
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Create rooms'),
  ];

  return $form;
}

/**
 * Validation handler for conference_schedule_rooms_bulk_form().
 */
function conference_schedule_rooms_bulk_form_validate($form, &$form_state) {
  $rooms = array_filter(explode("\n", $form_state['values']['rooms']));
  $wrong = [];

  array_walk($rooms, function(&$room) use (&$wrong) {
    $room = array_filter(explode('|', preg_replace("/\s+\|\s+/", '|', trim($room))));

    if (count($room) <> 2) {
      $wrong[] = implode('|', $room);
    }
  });

  if (!empty($wrong)) {
    form_set_error('rooms', format_plural(count($wrong), 'Entry %rooms does not follow the required format.', '@count entries do not follow the required format: %rooms', ['%rooms' => implode(', ', $wrong)]));
  }

  $form_state['values']['rooms'] = $rooms;
}

/**
 * Submit handler for conference_schedule_rooms_bulk_form().
 */
function conference_schedule_rooms_bulk_form_submit($form, &$form_state) {
  global $user;
  $weight = 0;

  foreach ($form_state['values']['rooms'] as $room) {
    list($name, $capacity) = $room;

    $room = entity_create('node', [
      'title' => $name,
      'type' => 'conference_room',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
      'uid' => $user->uid,
    ]);

    $wroom = entity_metadata_wrapper('node', $room);
    $wroom->conference_room_capacity = $capacity;
    $wroom->conference_reference = Conference::current();
    $wroom->conference_weight = $weight++;
    $wroom->save();
  }

  $form_state['redirect'] = 'admin/conference/settings/schedule/rooms';
}
