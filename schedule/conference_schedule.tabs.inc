<?php
/**
 * @file
 * Schedule tabs for session and area nodes.
 */

/**
 * Menu callback: schedule tab.
 */
function conference_schedule_tab_page($node = NULL) {
  if (!empty($node)) {
    switch ($node->type) {
      case 'area' :
        return drupal_get_form('conference_schedule_tab_settings_form', $node);

      case 'session' :
        return drupal_get_form('conference_schedule_timeslot_edit', $node);
    }
  }

  drupal_set_title(t('Schedule defaults'));
  $form['intro'] = [
    '#markup' => '<p>' . t('On this page you can define scheduling settings for all areas whose settings haven’t been defined individually.') . '</p>',
  ];
  $form += drupal_get_form('conference_schedule_tab_settings_form');
  return $form;
}

/**
 * Form constructor: schedule tab for session nodes.
 */
function conference_schedule_timeslot_edit($form, &$form_state) {
  $node = $form_state['build_info']['args'][0];
  $type_name = node_type_get_name($node);
  drupal_set_title(t('<em>Schedule @type</em> @title', ['@type' => $type_name, '@title' => $node->title]), PASS_THROUGH);

  module_load_include('inc', 'node', 'node.pages');
  $form = drupal_get_form($node->type . '_node_form', $node);
  $display = field_view_field('node', $node, 'conference_timeslot_picker', ['label' => 'hidden']);
  $form = [
    'conference_timeslot_display' => [
      '#markup' => drupal_render($display),
    ],
    'conference_timeslot_picker' => $form['conference_timeslot_picker']['#original'],
  ];
  $form['conference_timeslot_picker'][LANGUAGE_NONE]['#type'] = 'radiomatrix';

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save schedule'),
  ];

  return $form;
}

/**
 * Submit handler for conference_schedule_timeslot_edit().
 */
function conference_schedule_timeslot_edit_submit($form, &$form_state) {
  $node = $form_state['build_info']['args'][0];
  $node->{'conference_timeslot_picker'} = $form_state['values']['conference_timeslot_picker'];
  $auto = flag_get_flag('conference_schedule_auto');
  $auto->flag('unflag', $node->nid, NULL, TRUE);
  $form_state['redirect'] = 'node/' . $node->nid;
  node_save($node);
}

/**
 * Form constructor: schedule-settings tab for area nodes.
 */
function conference_schedule_tab_settings_form($form, &$form_state) {
  $node = !empty($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : NULL;
  $area_id = !empty($node) ? $node->nid : 'default';
  $settings = cvget('conference_schedule_auto_settings', ['area_id' => $area_id]);

  // If conference is not current, reset everything except room occupancy
  if ($settings['conference'] != Conference::current()) {
    $settings['conference'] = Conference::current();
    $settings['data'] = [
      'room_capacity' => !empty($settings['data']['room_capacity']) ? $settings['data']['room_capacity'] : ['min' => 0, 'max' => 5000],
    ];
  }

  $data = conference_schedule_get_scheduling_data($settings['conference']);
  $form_state['conference_schedule_auto_settings'] = $settings;
  $rooms = !empty($data['conference_rooms']) ? $data['conference_rooms'] : [];

  if (empty($rooms)) {
    drupal_set_message(t('No rooms are available; !create_rooms or !refresh_settings.', ['!create_rooms' => l(t('create rooms'), 'admin/conference/settings/schedule/rooms'), '!refresh_settings' => l(t('refresh scheduling data'), 'admin/conference/settings/schedule/refresh')]), 'warning');
  }

  // Restrict by capacity and produce room labels
  foreach ($rooms as $rnid => &$room) {
    if ($room['capacity'] >= $settings['data']['room_capacity']['min'] && $room['capacity'] <= $settings['data']['room_capacity']['max']) {
      $room = $room['title'] . ' (' . $room['capacity'] . ')';
    }
    else {
      unset($rooms[$rnid]);
    }
  }

  // Checkboxes of rooms filterable by capacity
  $wrapper_id = 'conference-schedule-settings-rooms-wrapper';
  $form['rooms'] = [
    '#type' => 'checkboxes',
    '#title' => t('Rooms'),
    '#options' => $rooms,
    '#default_value' => !empty($settings['data']['rooms']) ? $settings['data']['rooms'] : array_combine(array_keys($rooms), array_keys($rooms)),
    '#prefix' => '<div id="' . $wrapper_id . '">',
    '#suffix' => '</div>',
    '#description' => t('Rooms with capacity between !min and !max people.', ['!min' => $settings['data']['room_capacity']['min'], '!max' => $settings['data']['room_capacity']['max']]),
    '#checkall' => TRUE, // Works only if checkall module is installed
  ];

  // Refresh rooms by capacity
  $form['room_capacity'] = [
    '#title' => t('Room capacity'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['room_capacity']['min'] = [
    '#type' => 'numberfield',
    '#title' => t('Min'),
    '#default_value' => !empty($settings['data']['room_capacity']['min']) ? $settings['data']['room_capacity']['min'] : 0,
    '#step' => 10,
    '#min' => 0,
    '#max' => $settings['data']['room_capacity']['max'],
  ];

  $form['room_capacity']['max'] = [
    '#type' => 'numberfield',
    '#title' => t('Max'),
    '#default_value' => !empty($settings['data']['room_capacity']['max']) ? $settings['data']['room_capacity']['max'] : 0,
    '#step' => 10,
    '#min' => $settings['data']['room_capacity']['min'],
  ];

  $form['room_capacity']['actions'] = [
    '#type' => 'actions',
  ];

  $form['room_capacity']['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Refresh rooms'),
    '#submit' => ['conference_schedule_tab_settings_room_capacity_submit'],
    '#ajax' => [
      'callback' => 'conference_schedule_tab_settings_room_capacity_js',
      'wrapper' => $wrapper_id,
      'effect' => 'fade',
    ],
    '#name' => 'rooms_refresh',
  ];

  $form['actions'] = [
    '#type' => 'actions',
  ];

  $form['room_options'] = [
    '#type' => 'fieldset',
    '#title' => t('Room options'),
    '#tree' => TRUE,
  ];

  $form['room_options']['split'] = [
    '#type' => 'checkbox',
    '#title' => t('Allow splitting areas into separate rooms'),
    '#description' => t('If checked, it allows an area’s schedule to be continued in a different room after all time slots in a previous room have been exhausted. It does not schedule sessions in multiple rooms at the same time.'),
    '#default_value' => isset($settings['data']['room_options']['split']) ? $settings['data']['room_options']['split'] : 0,
  ];

  // Time slots
  $days = Conference::days();
  $timeslots = $data['conference_timeslots'];

  foreach ($days as $wday => &$data) {
    $data['timeslots'] = array_filter($timeslots, function(&$timeslot) use ($data) {
      return $timeslot['timeslot']['value'] >= $data['day'] && $timeslot['timeslot']['value'] < strtotime('+1 day', $data['day']);
    });
  }

  $options = [];
  $conference = $settings['conference'];

  array_walk($timeslots, function(&$timeslot) use (&$options, $conference) {
    $day = conference_date_timestamp($timeslot['timeslot']['value'], $conference);
    // No need to account for the offset, because time slots are already in the proper time zone
    $day->setTime('00', '00', '00');
    $day = $day->getTimestamp();
    $options[$day][$timeslot['nid']] = $timeslot['timeslot_time'];
  });

  $form['timeslots'] = [
    '#title' => t('Time slots'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#raw_data' => $days,
  ];

  array_walk($days, function ($data) use (&$form, $options, $settings) {
    $form['timeslots'][$data['day']] = [
      '#title' => $data['date'],
      '#type' => 'checkboxes',
      '#options' => $options[$data['day']],
      '#default_value' => isset($settings['data']['timeslots'][$data['day']]) ? $settings['data']['timeslots'][$data['day']] : array_combine(array_keys($options[$data['day']]), array_keys($options[$data['day']])),
      '#checkall' => TRUE, // Works only if checkall module is installed
    ];
  });

  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save settings without scheduling'),
    '#name' => 'save',
  ];

  return $form;
}

/**
 * Submit handler for conference_schedule_tab_settings_form().
 */
function conference_schedule_tab_settings_form_submit($form, &$form_state) {
  _conference_schedule_tab_settings_save($form, $form_state);
  drupal_set_message(t('Settings saved.'));
}

/**
 * Submit handler for the rooms_refresh button in conference_schedule_tab_settings_form().
 */
function conference_schedule_tab_settings_room_capacity_submit($form, &$form_state) {
  _conference_schedule_tab_settings_save($form, $form_state);
  $form_state['rebuild'] = TRUE;
}

/**
 * AJAX callback for the rooms_refresh button in conference_schedule_tab_settings_form().
 */
function conference_schedule_tab_settings_room_capacity_js($form, $form_state) {
  return $form['rooms'];
}

/**
 * Saves area's schedule settings to a configuration variable.
 */
function _conference_schedule_tab_settings_save($form, &$form_state) {
  $node = !empty($form_state['build_info']['args'][0]) ? $form_state['build_info']['args'][0] : NULL;
  $settings = &$form_state['conference_schedule_auto_settings'];
  form_state_values_clean($form_state);
  $settings['data'] = array_merge($settings['data'], $form_state['values']);
  _conference_schedule_tab_settings_array_filter($settings['data']);
  unset($settings['data']['save'], $settings['data']['rooms_refresh']);

  if (!empty($node)) {
    cvset('conference_schedule_auto_settings', $settings, ['area_id' => $node->nid]);
  }
  else {
    switch ($form_state['triggering_element']['#name']) {
      case 'save' :
        $nodes = Conference::getAreas();
        foreach ($nodes as $nid => $node) {
          $existing = cvget('conference_schedule_auto_settings', ['area_id' => $nid]);

          if (empty($existing['conference']) || $existing['conference'] != Conference::current()) {
            cvset('conference_schedule_auto_settings', $settings, ['area_id' => $node->nid]);
          }
        }

      default :
        cvset('conference_schedule_auto_settings', $settings, ['area_id' => 'default']);
        break;
    }
  }
}

function _conference_schedule_tab_settings_array_filter(&$variable) {
  if (is_array($variable)) {
    $variable = array_filter($variable);
    array_walk($variable, '_conference_schedule_tab_settings_array_filter');
  }
}

/**
 * Outputs HTML for the radiomatrix element.
 */
function theme_radiomatrix($variables) {
  $element = $variables['element'];
  $entity = $element['#entity'];
  $areas = Conference::getAreas();
  static $node_areas = [];

  // The matrix will be built only if the session is in the current conference
  $conference = Conference::getEntityConference($entity);

  if (!empty($conference) && $conference->nid == Conference::current()) {
    if (isset($element['#options'])) {
      $data = conference_schedule_get_scheduling_data($conference);
      $days = Conference::days($conference);
      $room_count = count($data['conference_rooms']);
      $room_count = $room_count + round($room_count / 10);

      // Group time slots based by day
      $timeslots = [];

      if (!empty($data['conference_timeslots'])) {
        array_walk($data['conference_timeslots'], function(&$timeslot) use ($days, &$timeslots) {
          foreach ($days as $weekday => $day) {
            if ($timeslot['timeslot']['value'] >= $day['day'] && $timeslot['timeslot']['value'] < strtotime('+1 day', $day['day'])) {
              $timeslots[$weekday][$timeslot['nid']] = [
                'data' => $timeslot['timeslot_time'],
                'class' => ['timeslot'],
              ];
            }
          }
        });
      }

      if (count($data)) {
        $conference_scheduled_sessions = [];

        foreach ($data['conference_scheduled_sessions'] as $nid => $node) {
          $wsession = entity_metadata_wrapper('node', $node);
          $nareas = array_flip($wsession->conference_areas->raw());
          $nareas_key = serialize($nareas);
          if (empty($node_areas[$nareas_key])) {
            $nareas_nodes = array_intersect_key($areas, $nareas);
            array_walk($nareas_nodes, function(&$area) {
              $area = $area->title;
            });
            $node_areas[$nareas_key] = implode('; ', $nareas_nodes);
          }
          $picker = $wsession->conference_timeslot_picker->value();
          $conference_scheduled_sessions[$picker]['title'] = $node->title . " ({$node_areas[$nareas_key]})";
          $conference_scheduled_sessions[$picker]['status'] = $entity->nid == $nid ? 'current' : 'scheduled';
        }

        $table = ['header' => ['timeslot' => ''], 'rows' => []];

        $i = 0;
        foreach ($data['conference_rooms'] as $rnid => $room) {
          $table['header'][$rnid] = $room['title'];
          if ($room['capacity']) {
            $table['header'][$rnid] .= " ({$room['capacity']})";
          }

          if ($i % 10 == 9) {
            $j = $i + 1;
            $table['header']["timeslot_{$j}"] = $table['header']['timeslot'];
          }

          $i++;
        }

        foreach ($days as $weekday => $day) {
          $table['rows'][$weekday]['timeslot'] = [
            'data' => $day['date'],
            'class' => ['weekday'],
            'colspan' => $room_count <= 11 ? $room_count : 11,
          ];

          for ($i = 1; $i <= $room_count; $i++) {
            $remaining = $room_count - $i;
            if ($remaining > 1) {
              if ($i % 11 == 0) {
                $table['rows'][$weekday]["timeslot_{$i}"] = $table['rows'][$weekday]['timeslot'];
                if ($remaining === 0) {
                  unset($table['rows'][$weekday]["timeslot_{$i}"]['colspan']);
                }
                else {
                  $table['rows'][$weekday]["timeslot_{$i}"]['colspan'] = $remaining <= 11 ? $remaining + 1 : 11;
                }
              }
            }
          }

          foreach ($timeslots[$weekday] as $tnid => $timeslot) {
            $table['rows'][$tnid]['timeslot'] = $timeslot;

            $i = 0;
            foreach ($data['conference_rooms'] as $rnid => $room) {
              $matrix_key = "{$tnid}|{$rnid}";
              $table['rows'][$tnid][$rnid] = [
                'data' => $element[$matrix_key]['#children'],
                'title' => $element[$matrix_key]['#title'],
              ];

              if (isset($conference_scheduled_sessions[$matrix_key])) {
                $table['rows'][$tnid][$rnid]['class'] = $conference_scheduled_sessions[$matrix_key]['status'];

                if ($conference_scheduled_sessions[$matrix_key]['status'] == 'scheduled') {
                  $table['rows'][$tnid][$rnid]['title'] .= ' – ' . $conference_scheduled_sessions[$matrix_key]['title'];
                }
              }

              if ($i % 10 == 9) {
                $j = $i + 1;
                $table['rows'][$tnid]["timeslot_{$j}"] = $table['rows'][$tnid]['timeslot'];
              }

              $i++;
            }
          }
        }

        return $element['_none']['#children'] . theme('table', $table);
      }
      else {
        return t('No scheduling options available.');
      }
    }
  }
  else {
    return t('This session is not in the current conference, and cannot be scheduled.');
  }
}
