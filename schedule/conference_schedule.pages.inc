<?php
/**
 * @file
 */

/**
 * Produces a conference-schedule page, which in turn calls a view.
 * The result is similar to using a contextual filter with a summary,
 * but more easily customized.
 */
function conference_schedule_schedule_page($snapshot = NULL, $year = NULL) {
  $conference = empty($year) ? Conference::getEntityConference() : Conference::getByYear($year);
  $cid = $conference->nid;
  $year = is_null($year) ? Conference::year($cid) : $year;
  $data = conference_schedule_get_scheduling_data($cid);

  // By day
  $output['day'] = conference_schedule_schedule_day($snapshot, $year, 'none', $conference, $data);
  // By area
  $output['area'] = drupal_get_form('conference_schedule_schedule_area_form', [$data, $conference]);
  // Search presentation titles
  $output['presentation'] = drupal_get_form('conference_schedule_schedule_presentation_form', [$cid]);

  $variables = [
    'conference' => $conference,
    'year' => $year,
    'data' => $data,
  ];
  drupal_alter('conference_schedule_schedule_page', $output, $variables);

  return empty($output) ? [] : $output;
}

/**
 * Displays the by-day conference schedule.
 */
function conference_schedule_schedule_day($snapshot, $year, $day = 'none', $conference = NULL, $data = NULL) {
  if (empty($day)) {
    drupal_goto('conference/schedule');
  }

  // 'none' is a trick to distinguish from NULL, and prevent infinite loops
  $day = $day == 'none' ? NULL : $day;

  // We need a full conference node
  $conference = empty($conference) ? Conference::getEntityConference() : Conference::getByYear($year);
  $cid = $conference->nid;
  $data = empty($data) ? conference_schedule_get_scheduling_data($cid) : $data;
  $snapshot = conference_schedule_get_snapshot($cid, $snapshot);

  $key = __FUNCTION__ . ":$cid";
  $schedules = &drupal_static($key, []);
  // Check cache first
  if (empty($schedules) && $cache = cache_get($key)) {
    $schedules = $cache->data;
  }

  if (empty($schedules[$year][$snapshot])) {
    if (!empty($conference)) {
      $schedules[$year][$snapshot] = Conference::days($conference);
      $list = [
        'type' => 'ul',
      ];

      foreach ($schedules[$year][$snapshot] as $conference_weekday => $conference_date) {
        $list['items'][] = l($conference_date['date'], 'conference/schedule/' . $year . '/day/' . $conference_weekday);
      }
      // Cache list HTML as well, so there's one less function to call
      $schedules[$year][$snapshot]['list'] = [
        '#theme' => 'item_list',
        '#type' => $list['type'],
        '#items' => $list['items'],
      ];
      // Data has changed, so save to cache
      cache_set($key, $schedules, 'cache', CACHE_TEMPORARY);
    }
  }

  // Produce links to daily schedule
  if (empty($day)) {
    drupal_set_title(t('!year conference schedule', ['!year' => $year]));
    return $schedules[$year][$snapshot]['list'];
  }
  else {
    if (!empty($schedules[$year][$snapshot][$day])) {
      $conference_date = $schedules[$year][$snapshot][$day];
      // Passing the date as argument makes the query not respect the time zone
      // Let's pass the NIDs of the day's time slots instead
      $timeslots = conference_schedule_get_timeslots($conference, $day);
      $timeslots = implode('+', array_keys($timeslots));
      drupal_set_title(t('Schedule for !date', ['!date' => $conference_date['date']]));
      $view_setting = explode(':', $data['conference_schedule_schedule_by_day_view']);
      $view = views_get_view($view_setting[0]);
      $output = $view->preview($view_setting[1], [$timeslots]);
      return empty($output) ? t('No schedule available.') : $output;
    }
  }
}

/**
 * Form constructor: area selection on schedule landing.
 *
 * @see conference_schedule_schedule_area_form_submit()
 */
function conference_schedule_schedule_area_form($form, &$form_state, $arguments) {
  list($data, $conference) = $arguments;
  $areas = _conference_get_sessions_areas($data['conference_scheduled_sessions'], $conference->nid);

  array_walk($areas, function(&$area) {
    $area = html_entity_decode($area->title);
  });

  $form['area'] = [
    '#type' => 'select',
    '#title' => t('View schedule by area'),
    '#options' => $areas,
    '#required' => TRUE,
  ];

  $form['conference'] = [
    '#type' => 'value',
    '#value' => $conference,
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('View schedule'),
  ];

  return $form;
}

/**
 * Submit handler for conference_schedule_schedule_area_form().
 *
 * @see conference_schedule_schedule_area_form()
 */
function conference_schedule_schedule_area_form_submit($form, &$form_state) {
  $area = $form_state['values']['area'];
  $year = Conference::year($form_state['values']['conference']);
  $form_state['redirect'] = "conference/schedule/$year/areas/$area";
}

/**
 * Menu callback for conference/schedule/areas.
 */
function conference_schedule_schedule_areas_page($area_nid = NULL, $year = NULL) {
  $conference = empty($year) ? Conference::getEntityConference() : Conference::getByYear($year);
  $data = conference_schedule_get_scheduling_data($conference->nid);
  $year = empty($year) ? Conference::year($conference) : $year;

  if (empty($area_nid)) {
    $areas = _conference_get_sessions_areas($data['conference_scheduled_sessions'], $conference->nid);

    if (empty($areas)) {
      return '<p>' . t('No areas have been scheduled yet.') . '</p>';
    }

    $items = [];
    array_walk($areas, function($area) use (&$items, $year) {
      $items[$area->nid] = l($area->title, "conference/schedule/$year/areas/{$area->nid}", ['html' => TRUE]);
    });

    return [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
  }

  $area = node_load($area_nid);
  drupal_set_title(t('Schedule by area: !area', ['!area' => html_entity_decode($area->title)]));
  $view_setting = explode(':', $data['conference_schedule_schedule_by_area_view']);
  $view = views_get_view($view_setting[0]);
  return $view->preview($view_setting[1], [$area_nid, $conference->nid]);
}

/**
 * Helper: extracts an array of areas from an array of sessions.
 */
function _conference_get_sessions_areas($sessions, $conference) {
  $key = "conference_schedule_sessions_areas:$conference";
  $cache = cache_get($key);
  $areas = !empty($cache->data) ? $cache->data : [];

  if (empty($areas)) {
    array_walk($sessions, function($session) use (&$areas) {
      $w = entity_metadata_wrapper('node', $session);
      $session_areas = $w->conference_areas->raw();
      $areas = array_merge($areas, $session_areas);
    });

    $areas = array_keys(array_flip($areas));

    if (empty($areas)) {
      return [];
    }

    // To sort nodes by title, we can't use node_load_multiple() right away
    $areas = (new EntityFieldQuery)
      ->entityCondition('entity_type', 'node')
      ->propertyCondition('nid', $areas)
      ->propertyOrderBy('title', 'ASC')
      ->execute();

    $areas = node_load_multiple(array_keys($areas['node']));
    cache_set($key, $areas, 'cache', CACHE_TEMPORARY);
  }

  return $areas;
}

/**
 * Form constructor: search presentations on schedule landing.
 *
 * @see conference_schedule_schedule_presentation_form_validate()
 * @see conference_schedule_schedule_presentation_form_submit()
 */
function conference_schedule_schedule_presentation_form($form, &$form_state, $arguments) {
  list($conference) = $arguments;

  $form['search'] = [
    '#type' => 'textfield',
    '#title' => t('Search presentation titles'),
    '#description' => t('Enter up to three search terms, separated by spaces. The search will match the terms in any order. Terms must be at least two characters long.'),
  ];

  $form['conference'] = [
    '#type' => 'value',
    '#value' => $conference,
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Search presentations'),
  ];

  return $form;
}

/**
 * Validation handler for conference_schedule_schedule_presentation_form().
 *
 * @see conference_schedule_schedule_presentation_form()
 * @see conference_schedule_schedule_presentation_form_submit()
 */
function conference_schedule_schedule_presentation_form_validate($form, &$form_state) {
  $search = explode(' ', preg_replace("/\s+/", ' ', trim(strtolower(filter_xss($form_state['values']['search'])))));

  $invalid = array_filter($search, function($term) {
    return strlen($term) < 2;
  });

  if (!empty($invalid)) {
    form_set_error('search', t('Each of your search terms must be at least two characters long.'));
    return;
  }

  if (count($search) > 3) {
    $search = array_slice($search, 0, 3);
    drupal_set_message(t('The is based only on the first three terms: %terms.', ['%terms' => implode(' ', $search)]), 'warning');
  }

  $form_state['conference_schedule_schedule_presentation_search'] = $search;
}

/**
 * Submit handler for conference_schedule_schedule_presentation_form().
 *
 * @see conference_schedule_schedule_presentation_form()
 * @see conference_schedule_schedule_presentation_form_validate()
 */
function conference_schedule_schedule_presentation_form_submit($form, &$form_state) {
  $search = $form_state['conference_schedule_schedule_presentation_search'];

  // This leaves the terms in the URL in the order they were entered
  $url_component = implode('-', $form_state['conference_schedule_schedule_presentation_search']);
  $year = Conference::year($form_state['values']['conference']);
  $form_state['redirect'] = "conference/schedule/$year/presentations/$url_component";
}

/**
 * Menu callback for conference/schedule/presentations.
 *
 * Displays search results by presentation title.
 */
function conference_schedule_schedule_presentations_page($terms = NULL, $year = NULL) {
  if (empty($terms)) {
    drupal_goto('conference/schedule');
    exit;
  }

  $conference = empty($year) ? node_load(Conference::current()) : Conference::getByYear($year);
  $data = conference_schedule_get_scheduling_data($conference->nid);
  $nids = _conference_schedule_search_by_title(explode('-', $terms), $conference->nid);
  $nids = !empty($nids) ? implode('+', $nids) : 0;

  drupal_set_title(t('Search presentation titles: %terms', ['%terms' => str_replace('-', ' ', $terms)]), PASS_THROUGH);
  $view_setting = explode(':', $data['conference_schedule_presentation_search_view']);
  $view = views_get_view($view_setting[0]);
  return $view->preview($view_setting[1], [$nids]);
}

/**
 * Helper: searches and caches NIDs based on terms.
 */
function _conference_schedule_search_by_title(array $search, $conference) {
  sort($search);
  $hash = sha1(serialize($search));
  // Needs new view (and config)
  // Should return a list of sessions (similar to by-day)

  // Check cache
  $key = "conference_schedule_schedule_presentation_search:$hash";
  $cache = cache_get($key);

  if (empty($cache->data)) {
    $presentations = (new EntityFieldQuery)
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'presentation')
      ->fieldCondition('conference_reference', 'target_id', $conference);

    foreach ($search as $term) {
      $presentations->propertyCondition('title', "%{$term}%", 'LIKE');
    }

    $presentations = $presentations->execute();

    if (!empty($presentations)) {
      $nids = array_keys($presentations['node']);
      cache_set($key, $nids, 'cache', CACHE_TEMPORARY);
    }
  }
  else {
    $nids = $cache->data;
  }

  return !empty($nids) ? $nids : [];
}
