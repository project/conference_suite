<?php
/**
 * @file
 * Bulk creation of time slots (simplified form).
 */

/**
 * Form constructor: bulk creation of time slots.
 */
function conference_schedule_timeslots_bulk_form($form, &$form_state) {
  drupal_set_title(t('Add time slots in bulk'));

  $default = cvget('conference_schedule_timeslots_default_timeslots');

  $form['timeslots'] = [
    '#type' => 'textarea',
    '#title' => t('Time slots'),
    '#description' => t('Enter one time slot per line, in the format <code>HH:mm-HH:mm</code>, where “HH:mm” is the time in 24-hour format. The provided time slots will be created for each day of the conference.'),
    '#rows' => 25,
    '#required' => TRUE,
    '#default_value' => $default,
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Create time slots'),
  ];

  return $form;
}

/**
 * Validation handler for conference_schedule_timeslots_bulk_form().
 */
function conference_schedule_timeslots_bulk_form_validate($form, &$form_state) {

}

/**
 * Submit handler for conference_schedule_timeslots_bulk_form().
 *
 * @todo Make lines 61–72 the default handling of conference days in Conference::days().
 */
function conference_schedule_timeslots_bulk_form_submit($form, &$form_state) {
  $timeslots = array_filter(explode("\n", $form_state['values']['timeslots']));
  $data = conference_schedule_get_scheduling_data();
  $existing_timeslots = $data['conference_timeslots'];
  array_walk($existing_timeslots, function(&$timeslot) {
    $timeslot = "{$timeslot['timeslot']['value']}-{$timeslot['timeslot']['value2']}";
  });
  $keys = ['start', 'end'];

  array_walk($timeslots, function(&$timeslot) use (&$keys) {
    $timeslot = array_combine($keys, array_filter(explode('-', trim($timeslot))));
  });

  $site_timezone = date_default_timezone_get();
  $timezone = conference_date_timezone();

  cvset('conference_schedule_timeslots_default_timeslots', $form_state['values']['timeslots']);

  $days = Conference::days();

  array_walk($days, function (&$day) use ($timezone, $site_timezone) {
    $date = new DateTime();
    $date->setTimestamp($day['day']);
    $utc = $day['day'] + $date->getOffset();

    date_default_timezone_set($timezone);

    $date = new DateTime();
    $date->setTimestamp($utc);
    $day = strtotime('midnight', $utc - $date->getOffset());
    date_default_timezone_set($site_timezone);
  });

  if ($timeslots) {
    $batch = [
      'title' => t('Creating time slots…'),
      'operations' => [
        [
          'conference_schedule_timeslots_bulk_create',
          [
            $timeslots,
            $existing_timeslots,
            $days,
            $timezone,
            $site_timezone,
          ],
        ],
      ],
      'init_message' => t('Starting time-slot creation'),
      'error_message' => t('An error occurred during processing'),
      'finished' => 'conference_schedule_timeslots_bulk_create_finished',
      'file' => drupal_get_path('module', 'conference_schedule') . '/conference_schedule.timeslots.inc',
    ];

    date_default_timezone_set($timezone);
    batch_set($batch);
  }

  $form_state['redirect'] = 'admin/conference/settings/schedule/timeslots';
}

/**
 * Batch-operation callback for creating time slots.
 *
 * @param $timeslots
 * @param $existing_timeslots
 * @param $days
 * @param $timezone
 * @param $site_timezone
 * @param $context
 */
function conference_schedule_timeslots_bulk_create($timeslots, $existing_timeslots, $days, $timezone, $site_timezone, &$context) {
  if (!isset($context['sandbox']['progress'])) :
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($timeslots);
    $context['sandbox']['timeslots'] = $timeslots;
    $context['sandbox']['existing_timeslots'] = $existing_timeslots;
    $context['results']['site_timezone'] = $site_timezone;
  endif;

  global $user;
  $timeslot = array_shift($context['sandbox']['timeslots']);

  foreach ($days as $day) {
    $start = strtotime($timeslot['start'], $day);
    $end = strtotime($timeslot['end'], $day);

    if (!in_array("{$start}-{$end}", $context['sandbox']['existing_timeslots'])) {
      $node = entity_create('node', [
        'type' => 'conference_timeslot',
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
        'uid' => $user->uid,
      ]);
      $w = entity_metadata_wrapper('node', $node);
      $w->conference_reference = Conference::current();
      $w->conference_timeslot = [
        'value' => $start,
        'value2' => $end,
        'timezone' => $timezone,
      ];
      $w->save();
      $context['sandbox']['existing_timeslots']["{$start}-{$end}"] = $w;
    }
  }

  $context['sandbox']['progress']++;
  $current = $context['sandbox']['progress'] * count($days);
  $total = $context['sandbox']['max'] * count($days);
  $context['message'] = t('Creating time slot !current of !count', ['!current' => $current, '!count' => $total]);
  $context['results']['processed'] = $current;

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) :
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  endif;
}

/**
 * Wraps up the bulk creation of time slots.
 *
 * @param $success
 * @param $results
 * @param $operations
 */
function conference_schedule_timeslots_bulk_create_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(format_plural($results['processed'], 'One time slot has been created.', '@count time slots have been created.'), 'status', FALSE);
  }
  date_default_timezone_set($results['site_timezone']);
}
