<?php
/**
 * @file
 * conference_schedule.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function conference_schedule_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'conference_room_capacity'.
  $field_bases['conference_room_capacity'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'conference_room_capacity',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  return $field_bases;
}
