# Conference Scheduling

Using results from the views `conference_timeslots` and `conference_rooms`, it provides a matrix of radio buttons used as the display for the field `conference_timeslot_picker` in the session content type. The value entered in this field is used to populate (in the background) two separate node-reference fields, `conference_timeslot_reference` and `conference_room_reference`.

The matrix is provided by adding to the system a new field type, called `radiomatrix`, whose process function replaces `form_process_radio`, and whose theme function is in charge of producing the table display for the field.

It's system-specific because the names of views and fields involved are hard-coded.

Results from the view are stored using the Config module.

Another settings page ("Assign session numbers") generates session numbers and saves them as strings in `conference_session_number` in session nodes. Currently, session numbers are built as follows:

- one digit for the conference day
- one letter for the time slot within that day
- two digits for the room.