<?php
/**
 * @file
 * Definition of conference_schedule_handler_field_node_link_schedule.
 */

/**
 * Field handler to present a link node edit.
 *
 * @ingroup views_field_handlers
 */
class conference_schedule_handler_field_node_link_schedule extends views_handler_field_node_link {
  /**
   * Renders the link.
   */
  function render_link($node, $values) {
    // Ensure user has access to edit this node.
    if (!conference_schedule_tab_access($node)) {
      return;
    }

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['path'] = "node/$node->nid/schedule";
    $this->options['alter']['query'] = drupal_get_destination();

    $text = !empty($this->options['text']) ? $this->options['text'] : t('schedule');
    return $text;
  }
}
