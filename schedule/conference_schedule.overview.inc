<?php
/**
 * @file
 * Schedule-overview table.
 */

/**
 * Menu callback: schedule overview.
 */
function conference_schedule_overview_page($area_nid = NULL) {
  drupal_set_title(t('Schedule overview'));

  $data = conference_schedule_get_scheduling_data();

  $header = ['timeslot' => ''];
  $rows = [];
  $empty = '';

  if (!empty($data['conference_rooms'])) {
    // Headers are keyed by room NID
    $i = 0;
    foreach ($data['conference_rooms'] as $rnid => $room) {
      $header[$rnid] = $room['title'];
      if ($room['capacity']) {
        $header[$rnid] .= " ({$room['capacity']})";
      };

      if ($i % 10 == 9) {
        $j = $i + 1;
        $header["timeslot_{$j}"] = $header['timeslot'];
      }

      $i++;
    }

    $days = Conference::days();
    $room_count = count($data['conference_rooms']);
    $room_count = $room_count + round($room_count / 10);
  }
  else {
    $empty .= '<p>' . t('No conference rooms have been defined. !manage_rooms.', ['!manage_rooms' => l(t('Manage rooms'), 'admin/conference/settings/schedule/rooms')]) . '</p>';
  }

  // Group time slots based by day
  $timeslots = [];

  if (!empty($data['conference_timeslots'])) {
    array_walk($data['conference_timeslots'], function(&$timeslot) use ($days, &$timeslots) {
      foreach ($days as $weekday => $day) {
        if ($timeslot['timeslot']['value'] >= $day['day'] && $timeslot['timeslot']['value'] < strtotime('+1 day', $day['day'])) {
          $timeslots[$weekday][$timeslot['nid']] = [
            'data' => $timeslot['timeslot_time'],
            'class' => ['timeslot'],
          ];
        }
      }
    });
  }
  else {
    $empty .= '<p>' . t('No time slots have been defined. !manage_timeslots.', ['!manage_timeslots' => l(t('Manage time slots'), 'admin/conference/settings/schedule/timeslots')]) . '</p>';
  }

  if (!empty($data['conference_rooms']) && !empty($data['conference_timeslots'])) {
    foreach ($days as $weekday => $day) {
      $rows[$weekday]['timeslot'] = [
        'data' => $day['date'],
        'class' => ['weekday'],
        'colspan' => $room_count <= 11 ? $room_count : 11,
      ];

      for ($i = 1; $i <= $room_count; $i++) {
        $remaining = $room_count - $i;
        if ($remaining > 1) {
          if ($i % 11 == 0) {
            $rows[$weekday]["timeslot_{$i}"] = $rows[$weekday]['timeslot'];
            if ($remaining === 0) {
              unset($rows[$weekday]["timeslot_{$i}"]['colspan']);
            }
            else {
              $rows[$weekday]["timeslot_{$i}"]['colspan'] = $remaining <= 11 ? $remaining + 1 : 11;
            }
          }
        }
      }

      foreach ($timeslots[$weekday] as $tnid => $timeslot) {
        $rows[$tnid]['timeslot'] = $timeslot;

        // Rows are keyed by time-slot NID
        $i = 0;
        foreach ($data['conference_rooms'] as $rnid => $room) {
          // Default: time slot is empty
          $rows[$tnid][$rnid] = [
            'data' => '<span class="element-invisible">' . t('Empty time slot') . '</span>',
            'class' => ['empty'],
            'title' => "{$day['date']} – {$timeslot['data']} ({$room['title']})",
          ];

          if ($i % 10 == 9) {
            $j = $i + 1;
            $rows[$tnid]["timeslot_{$j}"] = $rows[$tnid]['timeslot'];
          }

          $i++;
        }
      }
    }

    $areas = Conference::getAreas();

    // Fill in sessions
    foreach ($data['conference_scheduled_sessions'] as $session) {
      $wsession = entity_metadata_wrapper('node', $session);
      // Use reference fields for no extra processing
      $timeslot = $wsession->conference_timeslot_reference->raw();
      $room = $wsession->conference_room_reference->raw();

      $session_areas = array_intersect_key($areas, array_flip($wsession->conference_areas->raw()));

      if (in_array($area_nid, array_keys($session_areas))) {
        $class = ['highlight'];
      }
      else {
        $class = !empty($area_nid) ? ['dim'] : NULL;
      }

      array_walk($session_areas, function(&$area) use (&$area_nids) {
        $area = $area->title;
      });

      $session_areas = implode(', ', $session_areas);

      $rows[$timeslot][$room]['data'] = l($session->title, "node/{$session->nid}", ['html' => TRUE, 'attributes' => ['class' => ['session']]]) . "<br />\n<span class=\"area\">$session_areas</span>";
      if (!empty($class)) {
        $rows[$timeslot][$room]['class'] = $class;
      }
      else {
        unset($rows[$timeslot][$room]['class']);
      }
    }
  }

  return [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => [
      'class' => ['schedule-overview'],
    ],
    '#attached' => [
      'css' => [drupal_get_path('module', 'conference_schedule') . '/css/conference_schedule.css'],
    ],
    '#empty' => $empty,
  ];
}
