<?php
/**
 * @file
 * conference_schedule.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function conference_schedule_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-conference_room-conference_room_capacity'.
  $field_instances['node-conference_room-conference_room_capacity'] = array(
    'bundle' => 'conference_room',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'conference_room_capacity',
    'label' => 'Room capacity',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'html5_tools',
      'settings' => array(),
      'type' => 'numberfield',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Room capacity');

  return $field_instances;
}
