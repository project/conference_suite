<?php
/**
 * @file
 * Automatic scheduling of sessions.
 */

/**
 * Form constructor: area-scheduling overview.
 */
function conference_schedule_auto_form($form, &$form_state) {
  drupal_set_title(t('Automatic scheduling'));
  $data = conference_schedule_get_scheduling_data();

  $form['options'] = ['#type' => 'item'];
  $form['options']['operation'] = [
    '#type' => 'select',
    '#title' => t('Operation'),
    '#description' => t('The chosen operation will be performed on all areas selected in the table below.'),
    '#options' => [
      '_none' => t('- Select operation -'),
      t('Schedule') => [
        'schedule' => t('Schedule sessions'),
        'unschedule' => t('Unschedule sessions'),
        'direct' => t('Schedule sessions (non-batch)'),
        'unschedule_direct' => t('Unschedule sessions (non-batch)'),
      ],
      t('Settings') => [
        'defaults' => t('Set scheduling defaults'),
        'defaults_delete' => t('Delete scheduling defaults'),
      ],
      t('Test') => [
        'test' => t('Test-schedule sessions'),
        'direct_test' => t('Test-schedule sessions (non-batch)'),
      ],
    ],
  ];

  $form['options']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Run operation'),
  ];

  $areas = Conference::getAreas();
  $settings = cvget('conference_schedule_auto_settings', ['area_id' => '%']);
  $settings = !empty($settings['multiple']) ? $settings['multiple'] : [];
  $rows = [];
  $destination = current_path();
  $scheduled = $data['conference_scheduled_sessions'];
  array_walk($scheduled, function(&$node) {
    $wnode = entity_metadata_wrapper('node', $node);
    $areas = !empty($node->conference_areas) ? $wnode->conference_areas->raw() : [];
    $node = reset($areas); // Use first area as dominant
  });
  $scheduled = array_filter($scheduled);
  
  $unscheduled = $data['conference_unscheduled_sessions'];
  array_walk($unscheduled, function(&$node) {
    $wnode = entity_metadata_wrapper('node', $node);
    $areas = !empty($node->conference_areas) ? $wnode->conference_areas->raw() : [];
    $node = reset($areas); // Use first area as dominant
  });
  $unscheduled = array_filter($unscheduled);

  array_walk($areas, function(&$area) use ($settings, &$rows, $destination, $scheduled, $unscheduled) {
    $scheduled_count = count(preg_grep("/^{$area->nid}$/", $scheduled));
    $unscheduled_count = count(preg_grep("/^{$area->nid}$/", $unscheduled));
    
    if ($scheduled_count + $unscheduled_count != 0) { // Hide areas without any sessions
      if (!empty($settings[$area->nid]['data']['timeslots'])) {
        // Flatten time slots
        $timeslots = [];
        array_walk($settings[$area->nid]['data']['timeslots'], function ($day) use (&$timeslots) {
          $timeslots += $day;
        });
        $settings[$area->nid]['data']['timeslots'] = array_filter($timeslots);
      }
  
      $area->conference_schedule_auto_settings = !empty($settings[$area->nid]) ? $settings[$area->nid]['data'] : [];
      $rows[$area->nid] = [
        'area' => l($area->title, 'admin/conference/areas/sessions/', [
          'html' => TRUE,
          'query' => ['area' => $area->nid],
          'attributes' => ['target' => 'sessions']
        ]),
      ];
  
      if (!empty($settings[$area->nid]['data'])) {
        $rows[$area->nid] += [
          'room_capacity' => sprintf("%s – %s", $settings[$area->nid]['data']['room_capacity']['min'], $settings[$area->nid]['data']['room_capacity']['max']),
          'rooms' => count($settings[$area->nid]['data']['rooms']) . (!empty($settings[$area->nid]['data']['room_options']['split']) ? ' (' . t('Allow splitting') . ')' : ''),
          'timeslots' => count($settings[$area->nid]['data']['timeslots']),
        ];
      }
      else {
        $rows[$area->nid] += [
          'room_capacity' => [
            'class' => ['empty'],
          ],
          'rooms' => [],
          'timeslots' => [],
        ];
      }
  
      $rows[$area->nid] += [
        'scheduled' => $scheduled_count,
        'unscheduled' => $unscheduled_count,
        'operations' => [
          'data' => theme('item_list', [
            'items' => [
              l(t('settings'), "node/{$area->nid}/schedule", ['query' => ['destination' => $destination]]),
              l(t('overview'), "admin/conference/dashboard/schedule/{$area->nid}", ['attributes' => ['target' => 'overview']]),
            ],
          ]),
          'class' => 'operations',
        ],
      ];
    }
  });

  // Areas with settings will float to the top of the table
  $areas_set = array_intersect_key($rows, array_filter($areas, function(&$area) {
    return !empty($area->conference_schedule_auto_settings);
  }));
  $rows = $areas_set + array_diff_key($rows, $areas_set);

  // Add class to the last preset area
  end($areas_set);
  $last_set = key($areas_set);
  if (!empty($last_set)) {
    $rows[$last_set]['#attributes']['class'] = ['last-set'];
  }

  $form['areas'] = [
    '#type' => 'tableselect',
    '#empty' => t('No areas available.'),
    '#header' => [
      'area' => t('Area'),
      'room_capacity' => t('Room capacity (people)'),
      'rooms' => t('Rooms'),
      'timeslots' => t('Time slots'),
      'scheduled' => t('Scheduled sessions'),
      'unscheduled' => t('Unscheduled sessions'),
      'operations' => '',
    ],
    '#options' => $rows,
    '#attributes' => [
      'class' => ['conference-schedule-auto']
    ],
    '#attached' => [
      'css' => [drupal_get_path('module', 'conference_schedule') . '/css/conference_schedule.css'],
    ],
  ];

  $form['areas_data'] = [
    '#type' => 'value',
    '#value' => array_filter($areas, function(&$area) {
      return !empty($area->conference_schedule_auto_settings);
    }),
  ];

  $form['scheduling_data'] = [
    '#type' => 'value',
    '#value' => $data,
  ];

  return $form;
}

/**
 * Validation handler for conference_schedule_auto_form().
 */
function conference_schedule_auto_form_validate($form, &$form_state) {
  $areas = array_filter($form_state['values']['areas']);
  if (empty($areas)) {
    form_set_error('areas', t('You must select at least one area.'));
  }
  else {
    switch ($form_state['values']['operation']) {
      case 'schedule' :
      case 'test' :
      case 'direct' :
      case 'direct_test' :
      case 'unschedule' :
      case 'unschedule_direct' :
        $count = count($areas);
        $areas = array_intersect_key($form_state['values']['areas_data'], $areas);

        if (empty($areas)) {
          form_set_error('areas', format_plural($count, 'The area you selected cannot be scheduled.', 'The areas you selected cannot be scheduled.'));
        }
        else {
          // Replace array of full objects
          $form_state['values']['areas'] = $areas;
        }
      break;

      case 'defaults' :
      case 'defaults_delete' :
        // Nothing to do here
      break;
    }
  }
}

/**
 * Submit handler for conference_schedule_auto_form().
 */
function conference_schedule_auto_form_submit($form, &$form_state) {
  switch ($form_state['values']['operation']) {
    case 'schedule' :
    case 'test' :
    case 'direct' :
    case 'direct_test' :
    case 'unschedule' :
    case 'unschedule_direct' :
      $batch = !in_array($form_state['values']['operation'], ['direct', 'direct_test', 'unschedule_direct']) ? TRUE : FALSE;
      $test = in_array($form_state['values']['operation'], ['test', 'direct_test']) ? TRUE : FALSE;
      $operation = stristr($form_state['values']['operation'], 'unschedule') ? 'unschedule' : 'schedule';

      $conference = Conference::current();
      $areas = $form_state['values']['areas'];

      // Get scheduled sessions
      $data = $form_state['values']['scheduling_data'];
      // Get scheduled time slots
      $taken = [];
      array_walk($data['conference_scheduled_sessions'], function(&$session) use (&$taken) {
        $w = entity_metadata_wrapper('node', $session);
        $taken[$w->conference_timeslot_picker->value()] = $session->nid;
      });

      if ($batch) {
        if (!empty($areas)) {
          $operation_label = $operation == 'schedule' ? t('Scheduling') : t('Unscheduling');
          $batch = [
            'title' => t('!operation_label sessions…', ['!operation_label' => $operation_label]),
            'operations' => [
              [  'conference_schedule_auto_batch',
                [
                  $areas,
                  $taken,
                  $data,
                  $conference,
                  $test,
                  $operation,
                ],
              ],
            ],
            'init_message' => t('Starting auto-scheduling'),
            'error_message' => t('An error occurred during processing'),
            'finished' => 'conference_schedule_auto_batch_finished',
            'file' => drupal_get_path('module', 'conference_schedule') . '/conference_schedule.auto.inc',
          ];

          batch_set($batch);
        }
      }
      else {
        foreach ($areas as $area) {
          conference_schedule_auto_schedule($area, $taken, $data, $conference, $test, $operation);
        }
      }
    break;

    case 'defaults' :
      $defaults = $settings = cvget('conference_schedule_auto_settings', ['area_id' => 'default']);

      foreach ($form_state['values']['areas'] as $nid) {
        $existing = cvget('conference_schedule_auto_settings', ['area_id' => $nid]);

        if (empty($existing['conference']) || $existing['conference'] != Conference::current()) {
          cvset('conference_schedule_auto_settings', $settings, ['area_id' => $nid]);
        }
      }
    break;

    case 'defaults_delete' :
      foreach ($form_state['values']['areas'] as $nid) {
        cvdel('conference_schedule_auto_settings', ['area_id' => $nid]);
      }
    break;
  }
}

/**
 * Schedules sessions as batch.
 */
function conference_schedule_auto_batch($areas, $taken, $data, $conference, $test, $operation = 'schedule', &$context) {
  if (!isset($context['sandbox']['progress'])) :
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($areas);
    $context['sandbox']['areas'] = $areas;
    $context['sandbox']['taken'] = $taken;
  endif;

  // Process one area at a time
  $area = array_shift($context['sandbox']['areas']);

  // Keying results by area name for simplicity. @see conference_schedule_auto_batch_finished()
  $context['results'][$area->title] = conference_schedule_auto_schedule($area, $context['sandbox']['taken'], $data, $conference, $test, $operation);
  $context['sandbox']['progress']++;
  $operation_label = $operation == 'schedule' ? t('Scheduling') : t('Unscheduling');
  $context['message'] = t('!operation_label %record area', ['!operation_label' => $operation_label, '%record' => $area->title]);

  // Batch progress counter
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) :
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  endif;
}

/**
 * Performs the scheduling or unscheduling of sessions.
 *
 * @param $area
 * @param $taken
 * @param $data
 * @param $conference
 * @param $test
 * @param string $operation
 */
function conference_schedule_auto_schedule($area, &$taken, $data, $conference, $test, $operation = 'schedule') {
  // All area sessions
  $sessions = (new EntityFieldQuery)
    ->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'session')
    ->fieldCondition('conference_reference', 'target_id', $conference)
    ->fieldCondition('conference_areas', 'target_id', $area->nid)
    ->propertyOrderBy('nid', 'ASC')
    ->execute();

  if (!empty($sessions)) {
    $sessions = node_load_multiple(array_keys($sessions['node']));
  }

  // Auto-scheduling flag
  $auto = flag_get_flag('conference_schedule_auto');

  $saved = 0;
  switch ($operation) {
    case 'schedule' :
      $possible = [];
      $timeslots = $area->conference_schedule_auto_settings['timeslots'];

      // Find possible time slots per available rooms
      array_walk($area->conference_schedule_auto_settings['rooms'], function(&$room) use (&$possible, $timeslots, $taken) {
        foreach ($timeslots as $timeslot) {
          $slot = "$timeslot|$room";
          if (!isset($taken[$slot])) {
            $possible[$room][$slot] = $slot;
          }
        }
      });

      // Sorting sessions by weight using EntityFieldQuery doesn't work
      // if the weight field is empty, so we need more processing
      $sort = [];
      $i = 0;
      array_walk($sessions, function (&$session) use (&$sort, &$i) {
        $w = entity_metadata_wrapper('node', $session);
        $sort[$session->nid] = [
          'weight' => !empty($w->conference_weight->value()) ? $w->conference_weight->value() : $i++,
        ];
      });

      uasort($sort, 'drupal_sort_weight');

      $sorted = [];
      foreach ($sort as $nid => $item) {
        $sorted[$nid] = $sessions[$nid];
      }

      // Calculate only unscheduled sessions
      $sessions = array_diff_key($sorted, $data['conference_scheduled_sessions']);
      $session_count = count($sessions);
      $selected_slots = [];
      foreach ($possible as $rnid => $rtimeslots) {
        if (empty($area->conference_schedule_auto_settings['room_options']['split'])) {
          // If splitting is not allowed, pick one room with available slots
          if (count($rtimeslots) >= $session_count) {
            // Select timeslots
            $selected_slots = array_slice($rtimeslots, 0, $session_count, TRUE);
            break;
          }
          else {
            $rwrapper = entity_metadata_wrapper('node', $rnid);
            drupal_set_message(t('No available time slots for %area in %room.', ['%area' => $area->title, '%room' => $rwrapper->title->value()]), 'warning');
          }
        }
        else {
          // Splitting is allowed, so pick the appropriate time slots
          // For each room, find the last selected time slot,
          // and remove it and all previous ones from the following room
          if (!empty($previous_latest)) {
            // Check against all possible time slots
            $all_timeslots = array_keys($data['conference_timeslots']);
            $previous_position = array_search($previous_latest, $all_timeslots);
            $remaining_timeslots = array_slice($all_timeslots, $previous_position + 1);
            array_walk($remaining_timeslots, function(&$index) use ($rnid) {
              $index = "$index|$rnid";
            });
            $rtimeslots = array_intersect_key($rtimeslots, array_flip($remaining_timeslots));
          }

          if (empty($rtimeslots)) {
            // If there are no (more) time slots available, break the loop
            break;
          }

          $selected_slots += $rtimeslots;
          $rtimeslots = array_values($rtimeslots);
          end($rtimeslots);
          $latest_timeslot = key($rtimeslots);
          preg_match("/(\d+)\|\d+/", $rtimeslots[$latest_timeslot], $match);
          $previous_latest = $match[1];
        }
      }

      if (!empty($selected_slots)) {
        foreach ($sessions as $nid => $session) {
          $wsession = entity_metadata_wrapper('node', $session);
          $slot = array_shift($selected_slots);
          $taken[$slot] = "$nid";
          $wsession->conference_timeslot_picker = $slot;
          if ($test) {
            dpm($slot);
            dpm($session);
            // Collect data and display it in a table or something
          }
          else {
            $wsession->save();
            $saved++;
            $auto->flag('flag', $session->nid, NULL, TRUE);
          }

        }
      }
      else {
        drupal_set_message(t('No available time slots for %area with the current settings.', ['%area' => $area->title]), 'warning');
      }

    break;

    case 'unschedule' :
      foreach ($sessions as $session) {
        if ($auto->is_flagged($session->nid)) {
          // Unschedule only if auto-scheduled
          actions_do('conference_schedule_unschedule_session', $session);
          $auto->flag('unflag', $session->nid, NULL, TRUE);
          $saved++;
        }
      }
    break;
  }

  return $saved;
}

/**
 * Displays information at the end of the batch scheduling.
 */
function conference_schedule_auto_batch_finished($success, $results, $operations) {
  $results = array_filter($results);
  if (!empty($results)) {
    $areas = implode(', ', array_keys($results));
    drupal_set_message(t('Sessions have been scheduled for %areas.', ['%areas' => $areas]), 'status', FALSE);
  }
}