<?php
/**
 * @file
 * Session-numbering forms.
 */

/**
 * Menu callback for admin/conference/settings/schedule/numbers.
 *
 * Displays session-numbering settings and action form.
 */
function conference_schedule_session_numbers_page() {
  drupal_set_title(t('Assign session numbers'));
  $output['settings'] = drupal_get_form('conference_schedule_session_numbers_settings_form');
  $output['session_numbers_form'] = drupal_get_form('conference_schedule_session_numbers_form');
  return $output;
}

/**
 * Form constructor: session-numbering settings.
 *
 * @see conference_schedule_session_numbers_settings_form_submit()
 */
function conference_schedule_session_numbers_settings_form($form, &$form_state) {
  $settings = cvget('conference_schedule_session_numbers_settings');

  $options = [
    'numbers' => t('Numbers only'),
    'letters' => t('Letters only'),
    'numbers_letters' => t('Numbers, then letters'),
  ];

  $form['fieldset'] = [
    '#type' => 'fieldset',
    '#title' => t('Session-numbering settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['fieldset']['settings'] = ['#tree' => TRUE];

  $form['fieldset']['settings']['day'] = [
    '#type' => 'select',
    '#title' => t('Day'),
    '#options' => $options,
    '#default_value' => $settings['day'],
    '#required' => TRUE,
  ];

  $form['fieldset']['settings']['timeslot'] = [
    '#type' => 'select',
    '#title' => t('Time slot'),
    '#options' => $options,
    '#default_value' => $settings['timeslot'],
    '#required' => TRUE,
  ];

  $form['fieldset']['settings']['room'] = [
    '#type' => 'select',
    '#title' => t('Room'),
    '#options' => $options,
    '#default_value' => $settings['room'],
    '#required' => TRUE,
  ];

  $form['fieldset']['actions'] = ['#type' => 'actions'];
  $form['fieldset']['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save settings'),
  ];

  return $form;
}

/**
 * Submit handler for conference_schedule_session_numbers_settings_form().
 *
 * @see conference_schedule_session_numbers_settings_form()
 */
function conference_schedule_session_numbers_settings_form_submit($form, &$form_state) {
  $settings = $form_state['values']['settings'];
  cvset('conference_schedule_session_numbers_settings', $settings);
  drupal_set_message('The session-numbering settings have been saved.');
}

/**
 * Form constructor: assigns session numbers.
 *
 * @see conference_schedule_session_numbers_form_submit()
 */
function conference_schedule_session_numbers_form($form, &$form_state) {
  $data = conference_schedule_get_scheduling_data();
  $sessions = $data['conference_scheduled_sessions'];

  $markup = '<h3>' . t('Assign session numbers') . '</h3>';

  if (empty($sessions)) {
    $markup .= t('<p>There are no scheduled sessions.</p>');
  }
  else {
    $markup .= t('<p>Sessions are ready to be numbered. Only scheduled sessions will be numbered.</p>');

    $form['general']['description'] = [
      '#type' => 'markup',
      '#markup' => $markup,
    ];

    $form['data'] = [
      '#type' => 'value',
      '#value' => $data,
    ];

    $form['general']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Assign session numbers'),
    ];
  }

  return $form;
}

/**
 * Submit handler for conference_schedule_session_numbers_form().
 *
 * @see conference_schedule_session_numbers_form()
 */
function conference_schedule_session_numbers_form_submit($form, &$form_state) {
  // $current_conference = Conference::current();
  $data = $form_state['values']['data'];
  $settings = $data['conference_schedule_session_numbers_settings'];
  $sessions = $data['conference_scheduled_sessions'];
  $days = Conference::days();

  foreach ($days as $weekday => &$day) {
    $counter = isset($counter) ? ++$counter : (
        $settings['day'] == 'letter' ? 'A' : 1
      );
    $day['counter'] = $counter;
    $timeslots = (new EntityFieldQuery())
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'conference_timeslot')
      ->fieldCondition('conference_timeslot', 'value', $day['day'], '>=')
      ->fieldCondition('conference_timeslot', 'value2', strtotime("+1 day", $day['day']), '<')
      ->fieldOrderBy('conference_timeslot', 'value', 'ASC')
      ->fieldOrderBy('conference_timeslot', 'value2', 'ASC')
      ->execute();

    $timeslots = !empty($timeslots) ? array_keys($timeslots['node']) : [];
    $timeslot_counter = [];
    unset($tcounter);
    foreach ($timeslots as $nid) {
      $w = entity_metadata_wrapper('node', $nid);
      if (isset($timeslot_counter[$w->conference_timeslot->value()['value']])) {
        $tcounter = $timeslot_counter[$w->conference_timeslot->value()['value']];
      }
      else {
        $tcounter = isset($tcounter) ?
          (($tcounter == 9 && $settings['timeslot'] == 'numbers_letters') ? 'A' : ++$tcounter) :
          ($settings['timeslot'] == 'letter' ? 'A' : 0);
        $timeslot_counter[$w->conference_timeslot->value()['value']] = $tcounter;
      }

      $timeslot_codes[$nid] = $day['counter'] . $tcounter;
    }
  }

  $room_codes = [];
  foreach ($data['conference_rooms'] as $nid => $room) {
    $rcounter = isset($rcounter) ? ++$rcounter : (
        $settings['room'] == 'letter' ? 'A' : 1
      );
    $room_codes[$nid] = $settings['room'] != 'letter' ? str_pad($rcounter, 2, '0', STR_PAD_LEFT) : $rcounter;
  }

  if (!empty($timeslot_codes) && !empty($room_codes)) {
    $batch = [
      'title' => t('Numbering sessions…'),
      'operations' => [],
      'init_message' => t('Getting started'),
      'progress_message' => NULL,
      'error_message' => t('An error occurred during processing'),
      'finished' => 'conference_schedule_session_numbers_finished',
      'file' => drupal_get_path('module', 'conference_schedule') . '/conference_schedule.session_numbers.inc',
    ];

    // Reduce duplicates
    $batch['operations'][] = [
      'conference_schedule_session_numbers_batch',
      [
        $data['conference_scheduled_sessions'],
        $timeslot_codes,
        $room_codes,
      ],
    ];

    batch_set($batch);
    batch_process();
  }
}

/**
 * Batch operation callback: perform session numbering.
 */
function conference_schedule_session_numbers_batch($sessions, $timeslots, $rooms, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_record'] = 0;
    $context['sandbox']['max'] = count($sessions);
  }

  $chunk = array_slice($sessions, $context['sandbox']['current_record'], 50, TRUE);

  foreach ($chunk as $nid => $node) {
    $w = entity_metadata_wrapper('node', $node);
    $picker = explode('|', $w->conference_timeslot_picker->value());
    $number = $timeslots[$picker[0]] . $rooms[$picker[1]];
    $w->conference_session_number = $number;
    $w->save();

    $context['results'][$nid] = $number;
    $context['sandbox']['progress']++;
    $context['sandbox']['current_record']++;
    $context['message'] = format_plural($context['sandbox']['current_record'], '1 session numbered', '@count sessions numbered');
  }

  // Batch progress counter
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Performs post-processing for conference_schedule_session_numbers_form_submit().
 */
function conference_schedule_session_numbers_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(format_plural(count($results), 'One session has been numbered.', '@count sessions have been numbered.'));
  }
}