<?php
/**
 * @file
 * API for conference_schedule.
 */

/**
 * Allows modules to alter the output of conference_schedule_schedule_page().
 */
function hook_conference_schedule_schedule_page_alter(&$output, &$variables) {

}
