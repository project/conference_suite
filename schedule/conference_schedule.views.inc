<?php
/**
 * @file
 * Views support for Conference Schedule.
 */
/**
 * Implements hook_views_data_alter().
 */
function conference_schedule_views_data_alter(&$data) {
  $data['views_entity_node']['schedule_node'] = [
    'field' => [
      'title' => t('Schedule link'),
      'help' => t('Provide a simple link to schedule the content. It applies only to session nodes.'),
      'handler' => 'conference_schedule_handler_field_node_link_schedule',
    ],
  ];
}
