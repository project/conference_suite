<?php
/**
 * @file
 * Defines scheduling-related variables.
 */

/**
 * Implements hook_configure_variable_info().
 */
function conference_schedule_configure_variable_info() {
  $variables = [
    'conference_schedule_snapshots:conference_id' => [
      'type' => 'meta',
      'title' => t('Schedule snapshots'),
    ],
    'timeslot_title_date_pattern' => [
      'group' => 'misc',
      'main module' => 'conference',
      'title' => t('Time-slot title date pattern'),
      'options callback' => 'date_format_type_options',
      'default' => 'long',
      'element' => [
        '#type' => 'select',
        '#options' => [],
      ],
    ],
    'conference_schedule_timeslot_day_pattern' => [
      'group' => 'misc',
      'main module' => 'conference',
      'title' => t('Time-slot day pattern'),
      'options callback' => 'date_format_type_options',
      'default' => 'long',
      'element' => [
        '#type' => 'select',
        '#options' => [],
      ],
    ],
    'conference_schedule_timeslot_hour_pattern' => [
      'group' => 'misc',
      'main module' => 'conference',
      'title' => t('Time-slot hour pattern'),
      'options callback' => 'date_format_type_options',
      'default' => 'short',
      'element' => [
        '#type' => 'select',
        '#options' => [],
      ],
    ],
    'conference_schedule_timeslots_default_timeslots' => [
      'title' => t('Default time slots'),
    ],
    'conference_schedule_auto_settings:area_id' => [
      'type' => 'meta',
      'title' => t('Auto-scheduling settings'),
    ],
    'conference_schedule_default_timeslots' => [
      'title' => t('Default time slots'),
    ],
    'conference_schedule_schedule_by_day_view' => [
      'group' => 'misc',
      'main module' => 'conference',
      'title' => t('View used for schedule by day'),
      'options callback' => '_conference_get_views',
      'element' => [
        '#type' => 'select',
        '#options' => [t('- Select -')],
        '#required' => TRUE,
      ],
    ],
    'conference_schedule_schedule_by_area_view' => [
      'group' => 'misc',
      'main module' => 'conference',
      'title' => t('View used for schedule by area'),
      'options callback' => '_conference_get_views',
      'element' => [
        '#type' => 'select',
        '#options' => [t('- Select -')],
        '#required' => TRUE,
      ],
    ],
    'conference_schedule_presentation_search_view' => [
      'group' => 'misc',
      'main module' => 'conference',
      'title' => t('View used for presentation search'),
      'options callback' => '_conference_get_views',
      'element' => [
        '#type' => 'select',
        '#options' => [t('- Select -')],
        '#required' => TRUE,
      ],
    ],
    'conference_schedule_session_numbers_settings' => [
      'title' => t('Session-numbering settings'),
    ],
  ];

  return $variables;
}

/**
 * Implements hook_configure_variable_defaults().
 */
function conference_schedule_configure_variable_defaults() {
  return [
    'conference_schedule_auto_settings' => [
      'conference' => '',
      'data' => [],
    ],
    'conference_schedule_schedule_by_day_view' => 'conference_schedule_by_day:default',
    'conference_schedule_schedule_by_area_view' => 'conference_schedule_by_area:default',
    'conference_schedule_presentation_search_view' => 'conference_schedule_presentation_search:default',
    'conference_schedule_session_numbers_settings' => [
      'day' => 'numbers',
      'timeslot' => 'numbers',
      'room' => 'numbers',
    ],
  ];
}

/**
 * Implements hook_configure_variable_set().
 */
function conference_schedule_configure_variable_set($variables) {
  if ( in_array($variables['name'], ['conference_timeslots', 'conference_rooms']) ) {
    drupal_static_reset('conference_schedule_variables_get');
  }
}

/**
 * Implements hook_configure_variable_identifier().
 */
function conference_schedule_configure_variable_identifier($variable, $identifier) {
  switch ( $variable['identifier'] ) {
    case 'conference_id' :
      $node = node_load($identifier);

      return [
        'title' => $node->title,
        'value' => $node,
      ];
    break;
  }
}
