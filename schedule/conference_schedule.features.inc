<?php
/**
 * @file
 * conference_schedule.features.inc
 */

/**
 * Implements hook_views_api().
 */
function conference_schedule_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function conference_schedule_flag_default_flags() {
  $flags = array();
  // Exported flag: "Conference schedule auto".
  $flags['conference_schedule_auto'] = array(
    'entity_type' => 'node',
    'title' => 'Conference schedule auto',
    'global' => 1,
    'types' => array(
      0 => 'session',
    ),
    'flag_short' => 'Mark as scheduled automatically',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unmark as scheduled automatically',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'conference_schedule' => 0,
      'conference_schedule_xml' => 0,
      'conference_submissions_tabular' => 0,
      'ical' => 0,
      'diff_standard' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => FALSE,
    'i18n' => 0,
    'module' => 'conference_schedule',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Personal schedule".
  $flags['personal_calendar'] = array(
    'entity_type' => 'node',
    'title' => 'Personal schedule',
    'global' => 0,
    'types' => array(
      0 => 'session',
    ),
    'flag_short' => 'Add to personal schedule',
    'flag_long' => 'Add this session to your personal schedule',
    'flag_message' => '',
    'unflag_short' => 'In personal schedule',
    'unflag_long' => 'Remove this session from your personal schedule',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => -10,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'conference_schedule' => 0,
      'ical' => 0,
      'diff_standard' => 0,
      'easynews_newsletter' => 0,
      'token' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'conference_schedule',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}
