<?php
/**
 * @file
 * Defines variables for Conference Coupon.
 */

/**
 * Implements hook_configure_variable_info().
 *
 * @todo Generalize defaults.
 */
function conference_coupon_configure_variable_info() {
  $variables = [
    'conference_coupon_notify_coupon_creation' => [
      'group' => 'coupon_email',
      'type' => 'group',
      'title' => t('Coupon-creation notification e-mail'),
      'element' => [],
      'items' => [
        'subject' => [
          'title' => t('Subject of coupon-creation notification'),
          'element' => [
            '#type' => 'textfield',
          ],
          'default' => 'You have created a registration coupon [!code]',
        ],
        'body' => [
          'title' => t('Body of coupon-creation notification'),
          'element' => [
            '#type' => 'textarea',
            '#rows' => 10,
          ],
          'default' => "Dear !person,

you have created a registration coupon for **!product** on the [[site:name]][1] website.

Please forward this message to the recipient of your gift.

To use the registration coupon, simply go to this web address, and follow the instructions:

`!code_redemption_link`

The code expires on **!expiration**.

Should the link above not work properly, please go to `[site:url]coupon` and enter the code **!code** in the box.

!salutation

-- The [site:name] Web Genie


[1]: [site:url]"
        ],
      ]
    ],
    'conference_coupon_gift_expiration' => [
      'main module' => 'conference',
      'group' => 'deadlines',
      'default' => '21',
      'element' => [
        '#title' => t('Expiration of registration coupons'),
        '#type' => 'numberfield',
        '#description' => t('Number of days before the start of the conference by which registration coupons must be used.'),
      ],
      'extra' => [
        'deadline' => [
          'unit' => 'days',
          'operation' => 'before',
          'relative' => 'conference_start',
        ],
      ],
    ],
  ];

  return $variables;
}
