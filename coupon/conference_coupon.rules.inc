<?php
/**
 * @file
 * conference_coupon.rules.inc
 */

/**
 * Implements hook_rules_action_info().
 */
function conference_coupon_rules_action_info() {
  return [
    'conference_coupon_rules_create_gift_codes_action' => [
      'label' => t('Create registration coupons from order'),
      'group' => t('Conference Coupon'),
      'parameter' => [
        'user' => [
          'type' => 'commerce_order',
          'label' => t('Commerce order'),
        ],
      ],
    ],
  ];
}

/**
 * Rule action to create a registration coupon.
 *
 * @see conference_coupon_create_gift_codes_action()
 */
function conference_coupon_rules_create_gift_codes_action($commerce_order) {
  conference_coupon_create_gift_codes_action($commerce_order);
}